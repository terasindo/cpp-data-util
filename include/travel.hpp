#pragma once

#include <string>

#include "busStop.hpp"
#include "corridor.hpp"

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

using std::string;

class Travel {
public:
  string busId;
  BusStop* fromBusStop;
  BusStop* toBusStop;
  int durationSec;
  int startUnixTime;

  Travel(string _busId, BusStop* _fromBusStop, BusStop* _toBusStop, int _durationSec, int _startUnixTime) :
      busId(_busId),
      fromBusStop(_fromBusStop),
      toBusStop(_toBusStop),
      durationSec(_durationSec),
      startUnixTime(_startUnixTime)
    {
  };
  ~Travel() {};

  boost::posix_time::ptime getStartTime() {
    return boost::posix_time::ptime(Constant::UNIX_EPOCH_DATE) + boost::posix_time::seconds(startUnixTime);
  }

  boost::posix_time::ptime getStartTime(int utcOffsetHour) {
    return boost::posix_time::ptime(Constant::UNIX_EPOCH_DATE)
        + boost::posix_time::seconds(startUnixTime)
        + boost::posix_time::hours(utcOffsetHour);
  }

  string getReadableStartTime() {
    boost::posix_time::ptime time = getStartTime();
    return boost::posix_time::to_simple_string(time);
  }

  string getReadableStartTime(int utcOffsetHour) {
    boost::posix_time::ptime time = getStartTime(utcOffsetHour);
    return boost::posix_time::to_simple_string(time);
  }
};
