#pragma once

#include <cstdio>
#include <vector>
#include <unordered_map>
#include <string>

#include "position.hpp"

using std::string;
using std::vector;

class Position;

class PositionLogBundle {
private:
  unordered_map<string, vector<Position>> positionLogByBusId;

public:
  PositionLogBundle() {}
  ~PositionLogBundle() {};

  void readFromFile(string filePath) {
    fprintf(stdout, "Reading %s\n", filePath.c_str());
    FILE *file = fopen(filePath.c_str(), "r+");

    char buff[128];

    while (fscanf(file, "%[^\n]\n", buff) == 1) {
      string busId = buff;

      int unixTime;
      double latitude;
      double longitude;
      fscanf(file, "%d %lf %lf %[^\n]\n", &unixTime, &latitude, &longitude, buff);

      // Currently not used
      string claimedCorridor = buff;

      positionLogByBusId[busId].push_back(Position(unixTime, latitude, longitude));
    }

    fprintf(stdout, "Finished reading %s\n", filePath.c_str());
    fclose(file);
  }

  void erase(string busId) {
    positionLogByBusId.erase(busId);
  }

  vector<string> getBusIds() {
    vector<string> vec;
    vec.reserve(positionLogByBusId.size());
    for (auto kv : positionLogByBusId) {
      vec.push_back(kv.first);
    }
    return vec;
  }

  vector<Position> getPositionLog(string busId) {
    return positionLogByBusId[busId];
  }

  void addPositionLogByCloning(string busId, const vector<Position> &positionLog) {
    vector<Position> &positionsRef = positionLogByBusId[busId];

    positionsRef.clear();
    positionsRef.reserve(positionLog.size());

    for (int i = 0; i < positionLog.size(); i++) {
      positionsRef.push_back(positionLog[i]);
    }
  }
};
