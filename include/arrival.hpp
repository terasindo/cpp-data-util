#pragma once

#include "busStop.hpp"
#include "corridor.hpp"

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

class Arrival {
public:
  Corridor* corridor;
  BusStop* busStop;
  BusStop* destinationBusStop;
  int unixTime;
  string busId;

  Arrival(Corridor* _corridor, BusStop* _busStop, BusStop* _destinationBusStop, int _unixTime, string _busId) :
      corridor(_corridor),
      busStop(_busStop),
      destinationBusStop(_destinationBusStop),
      unixTime(_unixTime),
      busId(_busId)
    {
  };
  ~Arrival() {};

  boost::posix_time::ptime getTime() {
    return boost::posix_time::ptime(Constant::UNIX_EPOCH_DATE) + boost::posix_time::seconds(unixTime);
  }

  boost::posix_time::ptime getTime(int utcOffsetHour) {
    return boost::posix_time::ptime(Constant::UNIX_EPOCH_DATE)
        + boost::posix_time::seconds(unixTime)
        + boost::posix_time::hours(utcOffsetHour);
  }

  string getReadableTime() {
    boost::posix_time::ptime time = getTime();
    return boost::posix_time::to_simple_string(time);
  }

  string getReadableTime(int utcOffsetHour) {
    boost::posix_time::ptime time = getTime(utcOffsetHour);
    return boost::posix_time::to_simple_string(time);
  }
};
