#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "constant.hpp"
#include "graph.hpp"
#include "auxDataGenerator/autocompleteEntry.hpp"
using std::set;
using std::pair;
using std::make_pair;
using std::vector;
namespace gregorian = boost::gregorian;
namespace posix = boost::posix_time;

class AutocompleteGenerator {
private:
  Graph *mGraph;

public:
  AutocompleteGenerator(Graph *graph) {
    mGraph = graph;
  }
  ~AutocompleteGenerator() {};

  vector<AutocompleteEntry> generateAutocompleteEntries() {
    vector<AutocompleteEntry> result;

    for (Corridor* corridor : mGraph->corridors) {
      vector<BusStop*> busStops = mGraph->getBusStopsInCorridor(corridor);
      vector<BusStop*> ends = {corridor->startBusStop, corridor->endBusStop};

      for (BusStop* from : busStops) {
        for (BusStop* end : ends) {
          vector<BusStop*> visitedBusStop;
          vector<BusStop*> commonBusStop;

          BusStop* current = from;
          while (current != NULL) {
            visitedBusStop.push_back(current);
            current = current->getNext(corridor, end);
          }

          BusStop* currentCommonBusStop = NULL;
          for (int i = (int)visitedBusStop.size()-1; i >= 1; i--) {
            if (visitedBusStop[i]->isCommon) {
              currentCommonBusStop = visitedBusStop[i];
            }

            result.push_back(AutocompleteEntry(corridor, from, visitedBusStop[i], currentCommonBusStop));
          }
        }
      }
    }

    return result;
  }
};
