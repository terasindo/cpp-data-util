#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "constant.hpp"
#include "graph.hpp"

using std::set;
using std::pair;
using std::make_pair;
using std::vector;
namespace gregorian = boost::gregorian;
namespace posix = boost::posix_time;

class AutocompleteEntry {
public:
  Corridor *corridor;
  BusStop *fromBusStop;
  BusStop *toBusStop;
  BusStop *nextCommonBusStop;

  AutocompleteEntry(Corridor* _corridor, BusStop* _fromBusStop, BusStop* _toBusStop, BusStop* _nextCommonBusStop) :
      corridor(_corridor),
      fromBusStop(_fromBusStop),
      toBusStop(_toBusStop),
      nextCommonBusStop(_nextCommonBusStop)
    {
  }
  ~AutocompleteEntry() {};
};
