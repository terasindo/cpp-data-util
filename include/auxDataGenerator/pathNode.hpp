#pragma once

#include <algorithm>
#include <vector>
#include <bitset>

#include "busStop.hpp"
#include "corridor.hpp"
#include "constant.hpp"

using std::bitset;

class PathNode {
public:
  static const int ACTION_NONE = 0;
  static const int ACTION_GET_ON = 1;
  static const int ACTION_IN_BUS = 2;
  static const int ACTION_GET_OFF = 3;
  static const int ACTION_CROSS_BRIDGE = 4;

  bitset<Constant::MAX_CORRIDOR> activeCorridor;
  bitset<Constant::MAX_CORRIDOR> headingToEnd;
  BusStop *busStop;
  int action;

  PathNode(
      bitset<Constant::MAX_CORRIDOR> _activeCorridor,
      bitset<Constant::MAX_CORRIDOR> _headingToEnd,
      BusStop* _busStop,
      int _action
    ) :
      activeCorridor(_activeCorridor),
      headingToEnd(_headingToEnd),
      busStop(_busStop),
      action(_action)
    {
  };
  ~PathNode() {
  };

  void print() {
    if (action == ACTION_NONE) {
      printf("Start\n");
    } else if (action == ACTION_GET_ON) {
      string corridorChoice = "";
      for (Corridor* c : busStop->corridors) {
        if (activeCorridor.test(c->index) == 1) {
          if (corridorChoice.length() > 0) {
            corridorChoice += ";";
          }
          if (headingToEnd.test(c->index) == 1) {
            corridorChoice += c->name + "(" + c->endBusStop->name + ")";
          } else {
            corridorChoice += c->name + "(" + c->startBusStop->name + ")";
          }
        }
      }
      printf("Get on [%s], using [%s]\n", busStop->name.c_str(), corridorChoice.c_str());
    } else if (action == ACTION_IN_BUS) {
      printf("Travel in bus to [%s]\n", busStop->name.c_str());
    } else if (action == ACTION_GET_OFF) {
      printf("Get off in [%s]\n", busStop->name.c_str());
    } else if (action == ACTION_CROSS_BRIDGE) {
      printf("Walk to bus stop [%s]\n", busStop->name.c_str());
    }
  }

  bool isActiveCorridor(Corridor* c) {
    return activeCorridor.test(c->index) == 1;
  }
  BusStop* getCorridorEnd(Corridor* c) {
    if (headingToEnd.test(c->index) == 1) {
      return c->endBusStop;
    } else {
      return c->startBusStop;
    }
  }
};