#pragma once

#include <algorithm>
#include <vector>

#include "busStop.hpp"
#include "corridor.hpp"
#include "auxDataGenerator/pathNode.hpp"

class PathGroup {
public:
  BusStop *from;
  BusStop *to;
  vector<double> ranks;
  vector<vector<PathNode>> paths;

  PathGroup(BusStop* _from, BusStop* _to) :
      from(_from),
      to(_to)
    {
  };
  ~PathGroup() {
  };
};