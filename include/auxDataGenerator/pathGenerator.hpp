#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <map>
#include <unordered_map>
#include <bitset>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "constant.hpp"
#include "graph.hpp"
#include "auxDataGenerator/pathNode.hpp"
#include "auxDataGenerator/pathGroup.hpp"

using std::bitset;
using std::map;
using std::pair;
using std::make_pair;
using std::priority_queue;
using std::unordered_map;
using std::vector;
namespace gregorian = boost::gregorian;
namespace posix = boost::posix_time;

class PathGenerator {
private:
  class GCPathNode;
  Graph *mGraph;
  map<pair<BusStop*,BusStop*>, bool> reachableMemo;

  const bitset<Constant::MAX_BUS_STOP> NOTHING_VISITED;
  const bitset<Constant::MAX_CORRIDOR> NO_ACTIVE_CORRIDOR;
  const bitset<Constant::MAX_CORRIDOR> HEADING_TO_END_NOT_APPLICABLE;

  // With internal garbage collection
  class GCPathNode : public PathNode {
    public:
      GCPathNode *prev;
      static unordered_map<GCPathNode*,int> pointerCount;

      GCPathNode(
          bitset<Constant::MAX_CORRIDOR> _activeCorridor,
          bitset<Constant::MAX_CORRIDOR> _headingToEnd,
          BusStop* _busStop,
          int _action,
          GCPathNode* _prev
        ) :
          PathNode(_activeCorridor, _headingToEnd, _busStop, _action),
          prev(_prev)
        {
        if (prev != NULL) {
          // printf("ADDED\n");
          pointerCount[prev] += 1;
        }
      };
      ~GCPathNode() {
        if (prev != NULL) {
          pointerCount[prev] -= 1;
          if (pointerCount[prev] == 0) {
            // printf("ERASED\n");
            pointerCount.erase(prev);
            delete prev;
          }
        }
      };

      PathNode asPathNode() {
        return PathNode(activeCorridor, headingToEnd, busStop, action);
      }
  };

  class PqEntry {
    private:
      double estimatedDuration;
      double duration;
      bool onBus;
      Corridor* corridor;
      BusStop* corridorEnd;
      BusStop* busStop;
      bitset<Constant::MAX_BUS_STOP> visitedBusStop; // Visited = arrived here, then left
      GCPathNode* gCPathNode;

    public:
      PqEntry(
          double _estimatedDuration,
          double _duration,
          bool _onBus,
          BusStop* _busStop,
          bitset<Constant::MAX_BUS_STOP> _visitedBusStop,
          bitset<Constant::MAX_CORRIDOR> activeCorridor,
          bitset<Constant::MAX_CORRIDOR> headingToEnd,
          int action,
          GCPathNode* prevGCPathNode // IMPORTANT: THIS IS THE PREVIOUS ONE
        ) :
          estimatedDuration(_estimatedDuration),
          duration(_duration),
          onBus(_onBus),
          busStop(_busStop),
          visitedBusStop(_visitedBusStop)
        {
        gCPathNode = new GCPathNode(activeCorridor, headingToEnd, busStop, action, prevGCPathNode);
      };
      ~PqEntry() {
        // printf("State: %d\n", GCPathNode::pointerCount[gCPathNode]);
        if (GCPathNode::pointerCount[gCPathNode] == 0) {
          delete gCPathNode;
        }
      };

      double getEstimatedDuration() const { return estimatedDuration; }
      double getDuration() { return duration; }
      bool isOnBus() { return onBus; }
      BusStop* getBusStop() { return busStop; }
      bitset<Constant::MAX_BUS_STOP> getVisitedBusStop() { return visitedBusStop; }
      bitset<Constant::MAX_CORRIDOR> getActiveCorridor() { return gCPathNode->activeCorridor; }
      bitset<Constant::MAX_CORRIDOR> getHeadingToEnd() { return gCPathNode->headingToEnd; }
      GCPathNode* getGCPathNode() { return gCPathNode; }

      // Shorthand
      BusStop* getNextBusStop(Corridor* corridor, BusStop* corridorEnd) {
        if (!onBus) return NULL;
        return busStop->getNext(corridor, corridorEnd);
      }
      bool hasVisited(BusStop* busStop) {
        if (busStop == NULL) return false;
        return visitedBusStop.test(busStop->index) == 1;
      }
      bool isActiveCorridor(Corridor* corridor) {
        return gCPathNode->activeCorridor.test(corridor->index) == 1;
      }
      bool isHeadingToEnd(Corridor* corridor) {
        return gCPathNode->headingToEnd.test(corridor->index) == 1;
      }
  };

  class PqEntryComparator {
    public:
      bool operator()(const PqEntry* a, const PqEntry* b) {
        return a->getEstimatedDuration() > b->getEstimatedDuration();
      }
  };

  PathGroup getPaths(BusStop* from, BusStop* to) {
    printf("%s -> %s:\n", from->name.c_str(), to->name.c_str());
    fflush(stdout);

    PathGroup pathGroup(from, to);
    priority_queue<PqEntry*, vector<PqEntry*>, PqEntryComparator> pq;

    // First state
    pq.push(new PqEntry(
        getBusTravelTime(from, to),
        0,
        false,
        from,
        NOTHING_VISITED,
        NO_ACTIVE_CORRIDOR,
        HEADING_TO_END_NOT_APPLICABLE,
        PathNode::ACTION_NONE,
        NULL
    ));

    double bestTime = 1e+10;
    while (!pq.empty() && (pathGroup.paths.size() < Constant::MAX_PATH_TO_FIND)){
      PqEntry* top = pq.top();
      pq.pop();

      // printf("Now in %s\n", top->getBusStop()->name.c_str());
      // fflush(stdout);

      // if (pq.size() > 100000) {
      //   break;
      // }

      // Guards to stop, fugly I know
      bool skip = false;
      if (pathGroup.paths.size() > 0) {
        bool tooSlowRelative = (top->getDuration() > bestTime*Constant::MAX_TIME_MULTIPLIER_FROM_BEST);
        bool tooSlowAbsolute = (top->getDuration() - bestTime > Constant::MAX_TIME_DIFFERENCE_FROM_BEST);
        if (tooSlowRelative && tooSlowAbsolute) {
          skip = true;
        }
      }

      if (!skip) {
        if ((top->getBusStop() == to) && !top->isOnBus()) {
          if (pathGroup.paths.size() == 0) {
            bestTime = top->getDuration();
          }
          pathGroup.paths.push_back(reconstructPath(from, to, top->getGCPathNode()));
          pathGroup.ranks.push_back(top->getDuration());
        } else {
          // Relax
          vector<PqEntry*> nexts = getPossibleActions(top, to);
          for (PqEntry* next : nexts) {
            pq.push(next);
          }
        }
      }

      delete top;
    }

    while (!pq.empty()) {
      PqEntry* top = pq.top();
      pq.pop();
      delete top;
    }

    // Post processor: remove multiple paths with same signature
    // This can happen if there are >1 possible place to get off without affecting overall travel path, e.g:
    // a  b  c  d  e
    // |--|--|        corridor A
    //    |--|--|--|  corridor B
    // In this case, travel from a to e can be done either by getting of at b or c
    // We will greedily lengthen the first corridor, which means getting off at c
    // From waiting time perspective, it won't matter
    removeSameSignaturedPath(pathGroup);

    return pathGroup;
  }

  // Crazy but worth the time saving by not passing vectors
  vector<PathNode> reconstructPath(BusStop* from, BusStop* to, GCPathNode* gCPathNode) {
    vector<PathNode> path;

    GCPathNode* currPathNode = gCPathNode;
    bitset<Constant::MAX_CORRIDOR> lastActiveCorridor;
    while (currPathNode->action != PathNode::ACTION_NONE) {
      path.push_back(currPathNode->asPathNode());
      currPathNode = currPathNode->prev;
    }

    // Propagate active corridor
    for (int i = 0; i+1 < path.size(); i++) {
      if (path[i].action == PathNode::ACTION_IN_BUS) {
        path[i+1].activeCorridor &= path[i].activeCorridor;
        path[i+1].headingToEnd &= path[i].activeCorridor;
      }
    }

    reverse(path.begin(), path.end());
    return path;
  }

  vector<PqEntry*> getPossibleActions(PqEntry* now, BusStop* destination) {
    vector<PqEntry*> result;
    BusStop* currentBusStop = now->getBusStop();

    if (now->isOnBus()) {
      // Move forward
      {
        // Collect all possible next bus stop
        set<BusStop*> nextBusStopSet;
        for (Corridor* corridor : currentBusStop->corridors) {
          if (now->isActiveCorridor(corridor)) {
            BusStop* corridorEnd = NULL;
            if (now->isHeadingToEnd(corridor)) {
              corridorEnd = corridor->endBusStop;
            } else {
              corridorEnd = corridor->startBusStop;
            }

            BusStop* nextBusStop = currentBusStop->getNext(corridor, corridorEnd);
            if (nextBusStop != NULL) {
              nextBusStopSet.insert(nextBusStop);
            }
          }
        }

        // Move forward together per next possible bus stop
        for (BusStop* nextBusStop : nextBusStopSet) {
          // Filter the active corridors & towards end
          bitset<Constant::MAX_CORRIDOR> retainedActiveCorridor = getRetainedActiveCorridor(now, nextBusStop);
          result.push_back(new PqEntry(
              now->getDuration() + getBusTravelTime(currentBusStop, nextBusStop) + getBusTravelTime(nextBusStop, destination),
              now->getDuration() + getBusTravelTime(currentBusStop, nextBusStop),
              true,
              nextBusStop,
              getModifiedVisitedBusStop(now, currentBusStop), // Once you leave a bus stop, never return there
              now->getActiveCorridor() & retainedActiveCorridor,
              now->getHeadingToEnd() & retainedActiveCorridor,
              PathNode::ACTION_IN_BUS,
              now->getGCPathNode()
          ));
        }
      }

      // Get off
      {
        // Must not be redundant GET_OFF -> GET_ON -> GET_OFF
        if (!wasRedundantGetOnGetOff(now->getGCPathNode(), currentBusStop)) {
          result.push_back(new PqEntry(
              now->getEstimatedDuration(),
              now->getDuration(),
              false,
              currentBusStop,
              now->getVisitedBusStop(),
              NO_ACTIVE_CORRIDOR,
              HEADING_TO_END_NOT_APPLICABLE,
              PathNode::ACTION_GET_OFF,
              now->getGCPathNode()
          ));
        }
      }
    } else {
      // Walk via bridge
      {
        for (BusStop* nextBusStop : currentBusStop->bridges) {
          if (nextBusStop != currentBusStop) {
            result.push_back(new PqEntry(
                now->getDuration() + getWalkTravelTime(currentBusStop, nextBusStop) + getBusTravelTime(nextBusStop, destination),
                now->getDuration() + getWalkTravelTime(currentBusStop, nextBusStop),
                false,
                nextBusStop,
                getModifiedVisitedBusStop(now, currentBusStop),
                NO_ACTIVE_CORRIDOR,
                HEADING_TO_END_NOT_APPLICABLE,
                PathNode::ACTION_CROSS_BRIDGE,
                now->getGCPathNode()
            ));
          }
        }
      }

      // Get on
      {
        if (!now->hasVisited(currentBusStop)) {
          // Collect all possible next bus stop after this one
          set<BusStop*> nextBusStopSet;
          for (Corridor* corridor : currentBusStop->corridors) {
            BusStop* next = currentBusStop->getNext(corridor, corridor->startBusStop);
            BusStop* prev = currentBusStop->getNext(corridor, corridor->endBusStop);

            if (next != NULL) nextBusStopSet.insert(next);
            if (prev != NULL) nextBusStopSet.insert(prev);
          }

          // Try to head towards the next bus stop, while aligning the active corridor + heading to end
          for (BusStop* nextBusStop : nextBusStopSet) {
            bitset<Constant::MAX_CORRIDOR> activeCorridor = getAlignedActiveCorridor(now, nextBusStop);
            bitset<Constant::MAX_CORRIDOR> headingToEnd = getAlignedHeadingToEnd(now, nextBusStop);
            result.push_back(new PqEntry(
                now->getDuration() + Constant::SIMULATED_WAITING_TIME + getBusTravelTime(currentBusStop, destination),
                now->getDuration() + Constant::SIMULATED_WAITING_TIME,
                true,
                currentBusStop,
                now->getVisitedBusStop(),
                activeCorridor,
                headingToEnd & activeCorridor, // Just to make sure no wild '1'
                PathNode::ACTION_GET_ON,
                now->getGCPathNode()
            ));
          }
        }
      }
    }

    return result;
  }

  bitset<Constant::MAX_BUS_STOP> getModifiedVisitedBusStop(PqEntry* pqEntry, BusStop* visited) {
    bitset<Constant::MAX_BUS_STOP> visitedBusStop = pqEntry->getVisitedBusStop();
    visitedBusStop.set(visited->index, 1);
    return visitedBusStop;
  }

  bitset<Constant::MAX_CORRIDOR> getRetainedActiveCorridor(PqEntry* now, BusStop* targetBusStop) {
    bitset<Constant::MAX_CORRIDOR> retainedActiveCorridor;
    BusStop* busStop = now->getBusStop();

    // Collect corridor still on track
    for (Corridor* c : busStop->corridors) {
      BusStop* nextBusStop = NULL;
      if (now->isHeadingToEnd(c)) {
        nextBusStop = busStop->getNext(c, c->endBusStop);
      } else {
        nextBusStop = busStop->getNext(c, c->startBusStop);
      }

      // Does this corridor & corridorEnd reach that targetBusStop?
      if (nextBusStop == targetBusStop) {
        retainedActiveCorridor.set(c->index, 1);
      }
    }
    return retainedActiveCorridor;
  }

  bitset<Constant::MAX_CORRIDOR> getAlignedActiveCorridor(PqEntry* now, BusStop* targetBusStop) {
    bitset<Constant::MAX_CORRIDOR> alignedActiveCorridor;
    BusStop* busStop = now->getBusStop();

    // Collect corridor on track
    for (Corridor* c : busStop->corridors) {
      BusStop* way1 = busStop->getNext(c, c->endBusStop);
      BusStop* way2 = busStop->getNext(c, c->startBusStop);

      // Does this corridor & corridorEnd reach that targetBusStop?
      if ((way1 == targetBusStop) || (way2 == targetBusStop)) {
        alignedActiveCorridor.set(c->index, 1);
      }
    }
    return alignedActiveCorridor;
  }

  bitset<Constant::MAX_CORRIDOR> getAlignedHeadingToEnd(PqEntry* now, BusStop* targetBusStop) {
    bitset<Constant::MAX_CORRIDOR> headingToEnd;
    BusStop* busStop = now->getBusStop();

    for (Corridor* c : busStop->corridors) {
      BusStop* toEnd = busStop->getNext(c, c->endBusStop);

      if (toEnd == targetBusStop) {
        headingToEnd.set(c->index, 1);
      }
    }
    return headingToEnd;
  }

  void removeSameSignaturedPath(PathGroup &pathGroup) {
    // Sort by: +corridor signature, -segment length as vector
    vector<pair<pair<string, vector<int>>, int>> identities;
    for (int i = 0; i < pathGroup.paths.size(); i++) {
      string corridorSignature = getCorridorSignature(pathGroup.paths[i]);
      vector<int> segmentLengths = getSegmentLengths(pathGroup.paths[i]);

      // DIRTY HACK HERE: negating segmentLengths for descending sort
      for (int j = 0; j < segmentLengths.size(); j++) {
        segmentLengths[j] = -segmentLengths[j];
      }

      identities.push_back(make_pair(make_pair(corridorSignature, segmentLengths), i));
    }

    sort(identities.begin(), identities.end());

    // Unique
    string lastSignature = "";
    vector<int> takenIdxs;
    for (int i = 0; i < identities.size(); i++) {
      string corridorSignature = identities[i].first.first;
      int idx = identities[i].second;

      if (corridorSignature != lastSignature) {
        takenIdxs.push_back(idx);
      }
      lastSignature = corridorSignature;
    }

    // Replenish
    vector<double> ranks = pathGroup.ranks;
    vector<vector<PathNode>> paths = pathGroup.paths;
    pathGroup.ranks.clear();
    pathGroup.paths.clear();
    for (int i = 0; i < takenIdxs.size(); i++) {
      int idx = takenIdxs[i];
      pathGroup.ranks.push_back(ranks[idx]);
      pathGroup.paths.push_back(paths[idx]);
    }
  }

  // Return something like: "[1|kota,] [8|lebakbulus,3|kalideres]"
  string getCorridorSignature(vector<PathNode> &pathNodes) {
    string result = "";
    for (int i = 0; i < pathNodes.size(); i++) {
      if (pathNodes[i].action != PathNode::ACTION_GET_ON) {
        continue;
      }

      result += "[";

      for (Corridor* c : mGraph->corridors) {
        if (pathNodes[i].isActiveCorridor(c)) {
          result += c->name;
          result += "|";
          result += pathNodes[i].getCorridorEnd(c)->name;
          result += ",";
        }
      }

      result += "] ";
    }
    return result;
  }

  vector<int> getSegmentLengths(vector<PathNode> &pathNodes) {
    vector<int> result;

    int currSize;
    for (int i = 0; i < pathNodes.size(); i++) {
      if (pathNodes[i].action == PathNode::ACTION_GET_ON) {
        currSize = 1;
      } else if (pathNodes[i].action == PathNode::ACTION_IN_BUS) {
        currSize++;
      } else if (pathNodes[i].action == PathNode::ACTION_GET_OFF) {
        result.push_back(currSize);
        currSize = 0;
      }
    }

    return result;
  }

  // Returns true on:
  //   was: on------>off->on------->now
  //   can: on--------------------->now
  bool wasRedundantGetOnGetOff(GCPathNode* gCPathNode, BusStop* targetBusStop) {
    // Find second last get on bus stop + corridor + corridor end
    GCPathNode* curr = gCPathNode;
    GCPathNode* secondLastGetOn = NULL;
    bool metLastGetOn = false;
    while (curr != NULL) {
      if (curr->action == PathNode::ACTION_GET_ON) {
        if (!metLastGetOn) {
          metLastGetOn = true;
        } else {
          secondLastGetOn = curr;
          break;
        }
      }
      curr = curr->prev;
    }

    // No get on, no redundant get on get off
    if (secondLastGetOn == NULL) return false;

    // Try if it chooses correct corridor, can that bus reach this busStop
    return isReachable(secondLastGetOn->busStop, targetBusStop);
  }

  bool isReachable(BusStop* from, BusStop* to) {
    if (reachableMemo.count(make_pair(from, to)) > 0) {
      return reachableMemo[make_pair(from, to)];
    }

    bool result = false;
    for (Corridor* c : from->corridors) {
      vector<BusStop*> destinations = {c->startBusStop, c->endBusStop};
      for (BusStop* d : destinations) {
        BusStop* curr = from;
        while (curr != NULL) {
          if (curr == to) {
            result = true;
            break;
          }
          curr = curr->getNext(c, d);
        }

        if (result) break;
      }
      if (result) break;
    }

    // printf("=> '%s' to '%s' %d\n", from->name.c_str(), to->name.c_str(), result);
    reachableMemo[make_pair(from, to)] = result;
    return result;
  }
  double getBusTravelTime(BusStop* from, BusStop* to) {
    return from->distance(to) / Constant::SIMULATED_BUS_SPEED;
  }
  double getWalkTravelTime(BusStop* from, BusStop* to) {
    return from->distance(to) / Constant::SIMULATED_WALK_SPEED;
  }

  bool isWhiteListed(BusStop* busStop) {
    for (string s : Constant::DEFAULT_SELECTED_CORRIDORS) {
      for (Corridor* c : busStop->corridors) {
        if (c->name == s) {
          return true;
        }
      }
    }
    return false;
  }

public:
  PathGenerator(Graph *graph) {
    mGraph = graph;
  }
  ~PathGenerator() {};

  vector<PathGroup> generateAllPairPaths() {
    vector<PathGroup> result;

    for (BusStop* from : mGraph->busStops) {
      if (!isWhiteListed(from)) continue;

      for (BusStop* to : mGraph->busStops) {
        if (!isWhiteListed(to)) continue;

        // if (from->name != "Kalideres") continue;
        // if (to->name != "S Parman Podomoro City") continue;

        PathGroup pathGroup = getPaths(from, to);

        result.push_back(pathGroup);

        // printf("%s => %s\n", from->name.c_str(), to->name.c_str());
        // for (int i = 0; i < pathGroup.paths.size(); i++) {
        //   printf("Rank %lf:\n", pathGroup.ranks[i]);
        //   for (PathNode node : pathGroup.paths[i]) {
        //     node.print();
        //   }
        //   printf("\n");
        // }
      }
    }

    return result;
  }
};

// Initialize the static map
unordered_map<PathGenerator::GCPathNode*,int> PathGenerator::GCPathNode::pointerCount;
