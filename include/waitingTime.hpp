#pragma once

#include <string>
#include <map>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>

#include "busStop.hpp"
#include "corridor.hpp"

using std::map;
using std::string;

class WaitingTime {
public:
  Corridor* corridor;
  BusStop* busStop;
  BusStop* destinationBusStop;
  int minuteOfDay;

  map<int, int> average;
  map<int, int> median;
  map<int, int> p75;
  map<int, int> p80;
  map<int, int> p85;
  map<int, int> p90;

  WaitingTime(Corridor* _corridor, BusStop* _busStop, BusStop* _destinationBusStop, int _minuteOfDay) :
      corridor(_corridor),
      busStop(_busStop),
      destinationBusStop(_destinationBusStop),
      minuteOfDay(_minuteOfDay)
    {
  }
  ~WaitingTime() {};
};
