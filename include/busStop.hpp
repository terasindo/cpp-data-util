#pragma once

#include "point.hpp"

#include <vector>
#include <string>
#include <utility>
#include <unordered_map>
#include <unordered_set>

using std::pair;
using std::make_pair;
using std::string;
using std::vector;
using std::unordered_map;
using std::unordered_set;

class Corridor;
class BusStop : public Point {
private:
  vector<pair<Corridor*, BusStop*>> cachedNeigbors;

public:
  int index;
  string name;
  string slug;
  vector<string> aliases;
  vector<BusStop*> bridges;
  unordered_map<Corridor*, unordered_map<BusStop*, BusStop*>> next;
  unordered_set<Corridor*> corridors; // Corridors it serves
  bool isTerminal;
  bool isCommon;

  BusStop() : BusStop(0, 0.0, 0.0) {
    fprintf(stderr, "WARNING: Empty bus stop constructor should never be used!\n");
  }

  BusStop(int _index, double _latitude, double _longitude) :
      BusStop(_index, _latitude, _longitude, false) {};

  BusStop(int _index, double _latitude, double _longitude, bool _isTerminal) :
      index(_index), Point(_latitude, _longitude), isTerminal(_isTerminal) {};

  BusStop(int _index, double _latitude, double _longitude, bool _isTerminal, string _name) :
      index(_index), Point(_latitude, _longitude), isTerminal(_isTerminal), name(_name) {};

  ~BusStop () {
    aliases.clear();
    bridges.clear();
    next.clear();
    corridors.clear();
  }

  void setCoordinate(double latitude, double longitude) {
    Point::x = latitude;
    Point::y = longitude;
  }

  BusStop* getNext(Corridor* corridor, BusStop* destinationBusStop) {
    // printf("Here at '%s' attempt: '%s'\n", name.c_str(), destinationBusStop->name.c_str());
    if (next.count(corridor) == 0) {
      return NULL;
    }

    if (next[corridor].count(destinationBusStop) == 0) {
      return NULL;
    }

    return next[corridor][destinationBusStop];
  }

  vector<pair<Corridor*, BusStop*>> getNeighbors() {
    if (cachedNeigbors.size() > 0) {
      return cachedNeigbors;
    }

    for (auto kv : next) {
      Corridor *corridor = kv.first;
      for (auto destinationAndNext : kv.second) {
        BusStop* destination = destinationAndNext.first;
        BusStop* next = destinationAndNext.second;

        cachedNeigbors.push_back(make_pair(corridor, next));
      }
    }

    for (BusStop* busStop : bridges) {
      for (Corridor* corridor : busStop->corridors) {
        cachedNeigbors.push_back(make_pair(corridor, busStop));
      }
    }

    return cachedNeigbors;
  }

  double distance(BusStop* busStop) {
    return Point::distance(Point(busStop->x, busStop->y));
  }
};
