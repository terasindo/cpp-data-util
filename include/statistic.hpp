#pragma once

#include <algorithm>
#include <vector>
#include <cmath>

#include "point.hpp"
#include "math.hpp"

using std::vector;

class Statistic {
public:
  static double triangularKernel(double x) {
    return 1 - fabs(x);
  }

  static vector<Point> getPdf(const vector<double> &arr, int size, int width, int minX, int maxX) {
    vector<Point> result;

    if (arr.size() == 0) {
      return result;
    }

    int range = maxX - minX;
    double step = (double)range / (size - 1);
    if (range == 0) {
      // Special case...
      result.push_back(Point(minX, 1));
      return result;
    }

    vector<Point> buckets;
    for (int i = 0; i < size; i++) {
      buckets.push_back(Point(minX + i * step, 0));
    }

    for (int i = 0; i < arr.size(); i++) {
      double value = arr[i];
      int bucketIdx = (int)(floor((value - minX) / step) + 1e-5);

      double supposedArea = 0;
      double usedArea = 0;
      for (int j = -width; j <= width; j++) {
        supposedArea += triangularKernel((double)j/width);

        int idx = bucketIdx + j;
        if ((0 <= idx) && (idx < size)) {
          usedArea += triangularKernel((double)j/width);
        }
      }

      // If totally outside, treat this as outlier
      if (!Math::gt(usedArea, 0, 1e-5)) {
        continue;
      }

      double multiplier = supposedArea / usedArea;

      for (int j = -width; j <= width; j++) {
        int idx = bucketIdx + j;
        if ((0 <= idx) && (idx < size)) {
          buckets[idx].y += multiplier * triangularKernel((double)j/width);
        }
      }
    }

    double area = 0;
    for (int i = 0; i < size; i++) {
      area += buckets[i].y;
    }

    if (Math::gt(area, 0, 1e-5)) {
      for (int i = 0; i < size; i++) {
        buckets[i].y /= area;
      }
    }

    return buckets;
  }

  static double getExpectedValue(const vector<Point> &pdf) {
    if (pdf.size() == 0) {
      return 0;
    }

    double expected = 0;
    for (int i = 0; i < pdf.size(); i++) {
      expected += pdf[i].x * pdf[i].y;
    }
    return expected;
  }

  static double getXWithLeftTailArea(const vector<Point> &pdf, double area) {
    if (pdf.size() == 0) {
      return 0;
    }

    double accumulator = 0;
    int last = 0;
    for (int i = 0; i < pdf.size(); i++) {
      last = i;
      accumulator += pdf[i].y;

      if (accumulator >= area) {
        break;
      }
    }

    return pdf[last].x;
  }
};
