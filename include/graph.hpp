#pragma once

#include <cstdio>
#include <vector>
#include <algorithm>
#include <set>
#include <unordered_map>

#include "busStop.hpp"
#include "corridor.hpp"
#include "constant.hpp"

using std::string;
using std::vector;
using std::set;
using std::unordered_map;
using std::pair;
using std::make_pair;

class Graph {
private:
  unordered_map<Corridor*, vector<BusStop*>> cachedBusStopsInCorridor;
  unordered_map<Corridor*, set<BusStop*>> cachedBusStopsInCorridorSet;
  unordered_map<Corridor*, vector<Point>> cachedCorridorPolyline;

public:
  vector<BusStop*> busStops;
  vector<Corridor*> corridors;

  Graph(vector<BusStop*> _busStops, vector<Corridor*> _corridors) :
      busStops(_busStops), corridors(_corridors)
    {
  };
  Graph(string filePath) {
    cachedBusStopsInCorridor.clear();

    busStops.clear();
    corridors.clear();

    FILE *file;
    file = fopen(filePath.c_str(), "r+");

    char buff[1024];

    int nBusStop;
    fscanf(file, "%d\n", &nBusStop);
    for (int i = 0; i < nBusStop; i++) {
      fscanf(file, "%[^\n]\n", buff);

      string name = buff;

      int nAlias;
      double latitude;
      double longitude;
      int isTerminal;
      int isCommon;
      string slug;
      fscanf(file, "%d %lf %lf %d %d %s\n", &nAlias, &latitude, &longitude, &isTerminal, &isCommon, buff);
      slug = buff;

      vector<string> aliases;
      for (int j = 0; j < nAlias; j++) {
        fscanf(file, "%[^\n]\n", buff);
        string alias = buff;
        aliases.push_back(alias);
      }

      BusStop* busStop = new BusStop(busStops.size(), latitude, longitude);
      busStop->name = name;
      busStop->aliases = aliases;
      busStop->isTerminal = (isTerminal == 1);
      busStop->isCommon = (isCommon == 1);
      busStop->slug = slug;

      busStops.push_back(busStop);
    }

    int nAllBridge;
    fscanf(file, "%d\n", &nAllBridge);
    for (int i = 0; i < nAllBridge; i++) {
      int u, v;
      fscanf(file, "%d %d\n", &u, &v);
      (busStops[u]->bridges).push_back(busStops[v]);
    }

    int nCorridor;
    fscanf(file, "%d\n", &nCorridor);
    for (int i = 0; i < nCorridor; i++) {
      fscanf(file, "%[^\n]\n", buff);
      string name = buff;
      fscanf(file, "%[^\n]\n", buff);
      string fullName = buff;

      int nPointPolylineFromStart;
      int nPointPolylineFromEnd;
      int startBusStopId;
      int endBusStopId;
      string slug;
      fscanf(file, "%d %d %d %d %s\n", &nPointPolylineFromStart, &nPointPolylineFromEnd, &startBusStopId, &endBusStopId, buff);
      slug = buff;

      vector<Point> polylineFromStart;
      for (int j = 0; j < nPointPolylineFromStart; j++) {
        double latitude;
        double longitude;

        fscanf(file, "%lf %lf", &latitude, &longitude);
        polylineFromStart.push_back(Point(latitude, longitude));
      }
      fscanf(file, "\n");

      vector<Point> polylineFromEnd;
      for (int j = 0; j < nPointPolylineFromEnd; j++) {
        double latitude;
        double longitude;

        fscanf(file, "%lf %lf", &latitude, &longitude);
        polylineFromEnd.push_back(Point(latitude, longitude));
      }
      fscanf(file, "\n");

      Corridor* corridor = new Corridor();
      corridor->index = corridors.size();
      corridor->name = name;
      corridor->fullName = fullName;
      corridor->startBusStop = busStops[startBusStopId];
      corridor->endBusStop = busStops[endBusStopId];
      corridor->polylineFromStart = polylineFromStart;
      corridor->polylineFromEnd = polylineFromEnd;
      corridor->slug = slug;

      corridors.push_back(corridor);
    }

    int nEdge;
    fscanf(file, "%d\n", &nEdge);
    for (int i = 0; i < nEdge; i++) {
      int fromBusStopId;
      int toBusStopId;
      int corridorId;
      int destinationBusStopId;
      fscanf(file, "%d %d %d %d\n", &fromBusStopId, &toBusStopId, &corridorId, &destinationBusStopId);

      Corridor* corridor = corridors[corridorId];
      BusStop* fromBusStop = busStops[fromBusStopId];
      BusStop* toBusStop = busStops[toBusStopId];
      BusStop* destinationBusStop = busStops[destinationBusStopId];

      // Attach the edge
      fromBusStop->next[corridor][destinationBusStop] = toBusStop;
      fromBusStop->corridors.insert(corridor);
    }

    fclose(file);
  }

  ~Graph() {
    for (int i = 0; i < busStops.size(); i++) {
      delete busStops[i];
    }
    busStops.clear();

    for (int i = 0; i < corridors.size(); i++) {
      delete corridors[i];
    }
    corridors.clear();
  };

  // Don't call this too many times, save the value instead
  vector<pair<BusStop*, BusStop*>> getEdges() {
    vector<pair<BusStop*, BusStop*>> result;

    for (Corridor* c : corridors) {
      BusStop* startBusStop = c->startBusStop;
      BusStop* endBusStop = c->endBusStop;
      for (int rep = 0; rep < 2; rep++) {
        BusStop* currentBusStop = startBusStop;
        while (currentBusStop != NULL) {
          BusStop* nextBusStop = currentBusStop->getNext(c, endBusStop);

          if (nextBusStop != NULL) {
            result.push_back(make_pair(currentBusStop, nextBusStop));
          }

          currentBusStop = nextBusStop;
        }

        std::swap(startBusStop, endBusStop);
      }
    }

    sort(result.begin(), result.end());
    result.erase(unique(result.begin(), result.end()), result.end());

    return result;
  }

  // Return alphabetically sorted bus stops serving given corridor
  vector<BusStop*> getBusStopsInCorridor(Corridor* corridor) {
    if (cachedBusStopsInCorridor.count(corridor)) {
      return cachedBusStopsInCorridor[corridor];
    }

    vector<BusStop*> &busStopsRef = cachedBusStopsInCorridor[corridor];
    for (int i = 0; i < busStops.size(); i++) {
      if (busStops[i]->corridors.count(corridor)) {
        busStopsRef.push_back(busStops[i]);
      }
    }

    return busStopsRef;
  }

  set<BusStop*> getBusStopsInCorridorSet(Corridor* corridor) {
    if (cachedBusStopsInCorridorSet.count(corridor)) {
      return cachedBusStopsInCorridorSet[corridor];
    }

    set<BusStop*> &busStopsRef = cachedBusStopsInCorridorSet[corridor];
    for (int i = 0; i < busStops.size(); i++) {
      if (busStops[i]->corridors.count(corridor)) {
        busStopsRef.insert(busStops[i]);
      }
    }

    return busStopsRef;
  }

  // Return oriented bus stops serving given corridor's orientation
  vector<BusStop*> getOrientedBusStopsInCorridor(Corridor* corridor, BusStop* corridorStartBusStop) {
    BusStop* corridorEndBusStop;
    if (corridorStartBusStop == corridor->startBusStop) {
      corridorEndBusStop = corridor->endBusStop;
    } else {
      corridorEndBusStop = corridor->startBusStop;
    }

    vector<BusStop*> orientedBusStops;
    BusStop* currentBusStop = corridorStartBusStop;
    while (currentBusStop != NULL) {
      orientedBusStops.push_back(currentBusStop);
      currentBusStop = currentBusStop->getNext(corridor, corridorEndBusStop);
    }
    return orientedBusStops;
  }

  // Returns 1 cycle polyline (start -> end -> start)
  vector<Point> getCorridorPolyline(Corridor* corridor) {
    if (cachedCorridorPolyline.count(corridor)) {
      return cachedCorridorPolyline[corridor];
    }

    vector<Point> &corridorPolylineRef = cachedCorridorPolyline[corridor];
    corridorPolylineRef = corridor->polylineFromStart;
    corridorPolylineRef.insert(corridorPolylineRef.end(), corridor->polylineFromStart.begin(), corridor->polylineFromStart.end());
    return corridorPolylineRef;
  }

  vector<Point> getOrientedPolyline(Corridor* corridor, BusStop* corridorStartingBusStop) {
    if (corridorStartingBusStop == corridor->startBusStop) {
      return corridor->polylineFromStart;
    } else {
      return corridor->polylineFromEnd;
    }
  }

  /**
   * Given a chain of bus stop visits
   * Return full bus stop chain visit for given corridor respecting given ordering of bus stop visits
   * Return empty vector if no suitable bus stop chain exist
   */
  vector<BusStop*> getSuitableBusStopChain(Corridor* corridor, vector<BusStop*> &uniqueBusStopVisitChain) {
    pair<BusStop*,BusStop*> orientation = getCorridorOrientation(corridor, uniqueBusStopVisitChain);

    BusStop* start = orientation.first;
    BusStop* end = orientation.second;

    vector<BusStop*> chain;

    if ((start == NULL) || (end == NULL)) {
      return chain;
    }

    BusStop* currentBusStop = start;
    while (currentBusStop != NULL) {
      chain.push_back(currentBusStop);

      currentBusStop = currentBusStop->getNext(corridor, end);
    }
    return chain;
  }

  /**
   * Return the start bus stop terminal for given start and end bus stop in current route
   *
   * head to tail: A----------o--------o---o-------------o------B
   * start to end:            o--------o---o------------->
   * return (A, B)
   *
   * head to tail: A----------o--------o---o-------------o------B
   * start to end:            <--------o---o-------------o
   * return (B, A)
   */
  pair<BusStop*,BusStop*> getCorridorOrientation(Corridor* corridor, vector<BusStop*> &uniqueBusStopVisitChain) {
    BusStop* headBusStop = corridor->startBusStop;
    BusStop* tailBusStop = corridor->endBusStop;

    if (isInside(corridor, uniqueBusStopVisitChain, headBusStop, tailBusStop)) {
      return make_pair(headBusStop, tailBusStop);
    }
    if (isInside(corridor, uniqueBusStopVisitChain, tailBusStop, headBusStop)) {
      return make_pair(tailBusStop, headBusStop);
    }

    // Damnit, the bus did something unpredictable
    return make_pair((BusStop*)NULL, (BusStop*)NULL);
  }

  /**
   * Return true if start to end is contiguous subsequence of head to tail
   *
   * head to tail: o----------o--------o---o-------------o------>
   * start to end:            o--------o---o------------->
   * return true
   *
   * head to tail: o----------o--------o---o-------------o------>
   * start to end:            <--------o---o-------------o
   * return false
   */
  bool isInside(
      Corridor* corridor,
      vector<BusStop*> &uniqueBusStopVisitChain,
      BusStop* headBusStop,
      BusStop* tailBusStop
    ){

    int busStopIdxVisitedAt[Constant::MAX_BUS_STOP];
    memset(busStopIdxVisitedAt, -1, sizeof(busStopIdxVisitedAt));

    int index = 1;
    BusStop* currentBusStop = headBusStop;
    BusStop* terminalBusStop = tailBusStop;
    while (currentBusStop != NULL) {
      if (busStopIdxVisitedAt[currentBusStop->index] == -1) {
        busStopIdxVisitedAt[currentBusStop->index] = index;
      }

      index++;
      currentBusStop = currentBusStop->getNext(corridor, terminalBusStop);
    }

    vector<int> visitOrder;
    for (int i = 0; i < uniqueBusStopVisitChain.size(); i++) {
      int visitedAt = busStopIdxVisitedAt[uniqueBusStopVisitChain[i]->index];
      if (visitedAt > -1) {
        visitOrder.push_back(visitedAt);
      }
    }

    // Don't know if this make sense/will happen, but just put it here for safety
    if (visitOrder.size() == 0) {
      return false;
    }
    return visitOrder[0] < visitOrder.back();
  }

  /**
   * Return full bus stop chain starting from given startBusStop to endBusStop within corridor
   */
  vector<BusStop*> getBusStopChain(BusStop* startBusStop, BusStop* endBusStop, BusStop* routeStartBusStop, Corridor* corridor) {
    BusStop* busStop = routeStartBusStop;
    BusStop* terminalBusStop;
    if (routeStartBusStop == corridor->startBusStop) {
      terminalBusStop = corridor->endBusStop;
    } else {
      terminalBusStop = corridor->startBusStop;
    }

    bool writing = false;
    vector<BusStop*> busStopChain;
    while (busStop != NULL) {
      if (busStop == startBusStop) {
        writing = true;
      }

      if (writing) {
        busStopChain.push_back(busStop);
      }

      if (busStop == endBusStop) {
        writing = false;
      }

      busStop = busStop->getNext(corridor, terminalBusStop);
    }

    return busStopChain;
  };
};
