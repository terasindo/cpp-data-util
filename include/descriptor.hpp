#pragma once

#include "constant.hpp"

#include <cstdio>
#include <vector>
#include <iostream>
#include <boost/date_time/gregorian/gregorian.hpp>

using std::string;
using std::vector;
namespace gregorian = boost::gregorian;

class Descriptor {
public:
  gregorian::date lastExtractionDate;
  vector<string> selectedBusIds;
  vector<string> selectedCorridors;

  Descriptor() {};
  ~Descriptor() {
    selectedBusIds.clear();
    selectedCorridors.clear();
  };

  void readFromFile(string filePath) {
    fprintf(stdout, "Reading descriptor\n");

    FILE *file;
    file = fopen(filePath.c_str(), "r+");

    char buff[128];
    if (fscanf(file, "%s\n", buff) == -1) {
      fprintf(stdout, "Nothing is in descriptor, init with default values\n");
      lastExtractionDate = Constant::DEFAULT_LAST_EXTRACTION_DATE;
      selectedBusIds = Constant::DEFAULT_SELECTED_BUS_IDS;
      selectedCorridors = Constant::DEFAULT_SELECTED_CORRIDORS;
      fclose(file);
      return;
    }

    lastExtractionDate = gregorian::from_simple_string(buff);

    int nSelectedRoutes;
    fscanf(file, "%d\n", &nSelectedRoutes);
    for (int i = 0; i < nSelectedRoutes; i++) {
      fscanf(file, "%s\n", buff);
      string s = buff;
      selectedCorridors.push_back(s);
    }

    int nselectedBusIds;
    fscanf(file, "%d\n", &nselectedBusIds);
    for (int i = 0; i < nselectedBusIds; i++) {
      fscanf(file, "%s\n", buff);
      string s = buff;
      selectedBusIds.push_back(s);
    }

    fclose(file);
    fprintf(stdout, "Read complete\n");
  }

  void writeToFile(string filePath) {
    fprintf(stdout, "Writing descriptor\n");

    FILE *file;
    file = fopen(filePath.c_str(), "w+");

    fprintf(file, "%s\n", gregorian::to_iso_extended_string(lastExtractionDate).c_str());
    fprintf(file, "%d\n", (int)selectedCorridors.size());
    for (int i = 0; i < selectedCorridors.size(); i++) {
      fprintf(file, "%s\n", selectedCorridors[i].c_str());
    }
    fprintf(file, "%d\n", (int)selectedBusIds.size());
    for (int i = 0; i < selectedBusIds.size(); i++) {
      fprintf(file, "%s\n", selectedBusIds[i].c_str());
    }

    fclose(file);
    fprintf(stdout, "Write complete\n");
  }

  void print() {
    printf("%s\n", gregorian::to_iso_extended_string(lastExtractionDate).c_str());
    for (int i = 0; i < selectedCorridors.size(); i++) {
      printf("%s\n", selectedCorridors[i].c_str());
    }
    for (int i = 0; i < selectedBusIds.size(); i++) {
      printf("%s\n", selectedBusIds[i].c_str());
    }
  }
};
