/*
  Class to store constants that:
  - rarely changes, unless new corridors/buses is introduced
  - won't be mocked/changed in tests
  - sometimes needs to be tuned
  - is a flag for a certain configuration (e.g: enableFastButExperimentalMode)
 */

#pragma once

#include <vector>
#include <boost/date_time/gregorian/gregorian.hpp>

namespace Constant {
  // We have partial data on 2015-12-14
  // But it is better to start extracting on 2015-12-15 so we always start with full data
  // Therefore, the last extraction date is set to 2015-12-14
  const boost::gregorian::date DEFAULT_LAST_EXTRACTION_DATE(2015, 12, 14);
  const boost::gregorian::date UNIX_EPOCH_DATE(1970, 1, 1);

  const std::vector<std::string> DEFAULT_SELECTED_BUS_IDS = {};
  const std::vector<std::string> DEFAULT_SELECTED_CORRIDORS = {};
  const int MAX_CORE_USED = 3;

  const int LOCAL_TIME_UTC_OFFSET_HOUR = 7;
  const int MAX_CORRIDOR = 32;
  const int MAX_BUS_STOP = 256;
  const int MAX_BUS_STOP_IN_CORRIDOR = 32;
  const int MAX_EDGE = 1024;
  const int MAX_COMMON_BUS_STOP_IN_CORRIDOR = 16;
  const int MAX_POSITION_SIZE = 48 * 60; // Can contain up to at most this minutes worth of data
  const int MAX_WAITING_TIME = 90;
  const int MAX_TRAVEL_TIME = 90;

  const int DEFAULT_SERVING_THRESHOLD = 5;
  const double DEFAULT_TOO_FAR_ROUGH_THRESHOLD = 0.004;
  const double DEFAULT_TOO_FAR_FINE_THRESHOLD = 0.003; // Grogol 1 to Jelambar distance
  const double DEFAULT_SO_CLOSE_THRESHOLD = 0.0009; // About 7/10 Grogol 1 to Grogol 2
  constexpr int DEFAULT_STATE_EXPIRATION_DURATION_SECOND = 30 * 60;
  constexpr double DEFAULT_TELEPORTATION_MIN_DISTANCE = (DEFAULT_TOO_FAR_ROUGH_THRESHOLD * 30);
  constexpr int DEFAULT_MIN_DURATION_TO_BE_STAYING_MINUTE = 7;

  const bool USE_TRIANGLE_INEQUALITY_OPTIMIZATION = true;

  const std::vector<int> AGGREGATE_ARRIVAL_WINDOW_DAY = {30};
  const std::vector<int> AGGREGATE_TRAVEL_WINDOW_DAY = {30};
  const int AGGREGATE_GRANULARITY_MINUTE = 30;
  const int AGGREGATE_TRAVEL_QUERY_WINDOW_MINUTE = 10;
  const int AGGREGATE_INTERPOLATE_WINDOW = 3;
  const int PDF_SIZE = 50;
  const int KDE_WIDTH = 4;

  const int MAX_PATH_TO_FIND = 20;
  const double MAX_TIME_MULTIPLIER_FROM_BEST = 1.7;
  const double MAX_TIME_DIFFERENCE_FROM_BEST = 20;
  const double SIMULATED_BUS_SPEED = 0.003;
  const double SIMULATED_WALK_SPEED = 0.0003;
  const double SIMULATED_WAITING_TIME = 7;

  const bool DEBUG_SEGMENTIZER = false;
  const bool DEBUG_ARRIVAL = false;
  const bool DEBUG_TRAVEL = false;
}