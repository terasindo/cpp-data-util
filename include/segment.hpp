#pragma once

#include <vector>
#include <string>

#include "position.hpp"
#include "corridor.hpp"

using std::vector;
using std::string;

class Segment {
public:
  string busId;
  Corridor *corridor;
  vector<Position> positions;

  Segment() {};
  Segment(Corridor* _corridor, vector<Position> _positions) :
      corridor(_corridor),
      positions(_positions)
    {
  };

  ~Segment() {
    positions.clear();
  };

  int getStartTime() const {
    if (positions.size() == 0) {
      return -1;
    }
    return positions[0].unixTime;
  }

  int getEndTime() const {
    if (positions.size() == 0) {
      return -1;
    }
    return positions.back().unixTime;
  }
};
