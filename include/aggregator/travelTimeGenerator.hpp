#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "math.hpp"
#include "statistic.hpp"
#include "constant.hpp"
#include "graph.hpp"
#include "travelTime.hpp"

using std::set;
using std::pair;
using std::make_pair;
using std::vector;
namespace gregorian = boost::gregorian;
namespace posix = boost::posix_time;

class TravelTimeGenerator {
private:
  Graph *mGraph;
  gregorian::date mDate;
  int mMaxDayBefore;

  // Structure like travel[from..to][day-15][i'th travel] = (minute begin, duration)
  vector<vector<pair<int, int>>> travels[Constant::MAX_EDGE];

  // Descripting the indexes used in the mega-structure
  vector<pair<BusStop*, BusStop*>> idxToBusStopFromTo;

  // Reverse mappings
  map<pair<BusStop*, BusStop*>, int> busStopFromToToIdx;

  void construct() {
    idxToBusStopFromTo = mGraph->getEdges();

    // Fill the reverse mappings
    for (int i = 0; i < idxToBusStopFromTo.size(); i++) {
      busStopFromToToIdx[idxToBusStopFromTo[i]] = i;
    }

    // Fill in slot
    for (int i = 0; i < idxToBusStopFromTo.size(); i++) {
      for (int day = 0; day < mMaxDayBefore; day++) {
        vector<pair<int, int>> temp;
        travels[i].push_back(temp);
      }
    }
  }
  void readData(string travelFolderPath) {
    for (int deltaDay = 1; deltaDay <= mMaxDayBefore; deltaDay++) {
      gregorian::date currentDate = mDate - gregorian::date_duration(deltaDay);
      string currentDateStr = gregorian::to_iso_extended_string(currentDate);
      string filePath = travelFolderPath + currentDateStr + ".travel";

      fprintf(stdout, "Reading %s\n", filePath.c_str());
      FILE *file = fopen(filePath.c_str(), "r");
      if (!file) {
        // File not exist
        fprintf(stdout, "File not found, skipped\n");

        // To compensate, add 1 more day
        mMaxDayBefore++;
        continue;
      }

      char buff[128];

      // Prepare hydration
      map<string,BusStop*> nameToBusStop;
      for (int i = 0; i < mGraph->busStops.size(); i++) {
        nameToBusStop[mGraph->busStops[i]->name] = mGraph->busStops[i];
      }

      int nBusStop;
      vector<string> idxToBusStopName;

      // Read metadata
      if (fscanf(file, "%d\n", &nBusStop) == -1) {
        // No state, quit
        fclose(file);
        fprintf(stdout, "File empty, skipped\n");
        continue;
      }
      for (int i = 0; i < nBusStop; i++) {
        fscanf(file, "%[^\n]\n", buff);

        string busStopName = buff;
        idxToBusStopName.push_back(busStopName);
      }

      // Read actual data
      int nTravel;
      fscanf(file, "%d\n", &nTravel);
      for (int i = 0; i < nTravel; i++) {
        fscanf(file, "%[^\n]\n", buff);
        string busId = buff;

        int fromBusStopIdx;
        int toBusStopIdx;
        int unixTime;
        int durationSec;
        fscanf(file, "%d %d %d %d\n", &fromBusStopIdx, &toBusStopIdx, &unixTime, &durationSec);

        BusStop *fromBusStop = nameToBusStop[idxToBusStopName[fromBusStopIdx]];
        BusStop *toBusStop = nameToBusStop[idxToBusStopName[toBusStopIdx]];

        // Add to mega structure
        addTravel(fromBusStop, toBusStop, deltaDay, unixTime, durationSec);
      }

      fprintf(stdout, "Finished reading\n");
      fclose(file);
    }

    // Sort everything
    for (int i = 0; i < idxToBusStopFromTo.size(); i++) {
      for (int day = 0; day < mMaxDayBefore; day++) {
        sort(travels[i][day].begin(), travels[i][day].end());
      }
    }
  }
  void addTravel(BusStop *fromBusStop, BusStop *toBusStop, int deltaDay, int unixTime, int durationSec) {
    pair<BusStop*, BusStop*> fromTo = make_pair(fromBusStop, toBusStop);
    int fromToIdx = busStopFromToToIdx[fromTo];

    posix::ptime time = posix::ptime(Constant::UNIX_EPOCH_DATE) + posix::seconds(unixTime) + posix::hours(Constant::LOCAL_TIME_UTC_OFFSET_HOUR);
    posix::time_duration duration = time.time_of_day();
    int minuteOfDay = 60*duration.hours() + duration.minutes();

    travels[fromToIdx][deltaDay-1].push_back(make_pair(minuteOfDay, durationSec));
    if ((minuteOfDay <= Constant::AGGREGATE_TRAVEL_QUERY_WINDOW_MINUTE) && (deltaDay >= 2)) {
      // Wrap on yesterday
      travels[fromToIdx][deltaDay-2].push_back(make_pair(24*60 + minuteOfDay, durationSec));
    }
  }

  vector<TravelTime> getDailyTravelTime(int fromToIdx) {
    vector<TravelTime> result;

    BusStop *fromBusStop = idxToBusStopFromTo[fromToIdx].first;
    BusStop *toBusStop = idxToBusStopFromTo[fromToIdx].second;

    // if (fromBusStop->name != "Kota") return result;
    // if (toBusStop->name != "Kali Besar Barat") return result;

    for (int minuteOfDay = 0; minuteOfDay < 24*60; minuteOfDay += Constant::AGGREGATE_GRANULARITY_MINUTE) {
      result.push_back(TravelTime(fromBusStop, toBusStop, minuteOfDay));
    }

    // Get the data
    const vector<vector<pair<int,int>>> &localTravels = travels[fromToIdx];

    // Mega sliding window
    for (int deltaDayIdx = 0; deltaDayIdx < Constant::AGGREGATE_TRAVEL_WINDOW_DAY.size(); deltaDayIdx++) {
      int deltaDay = Constant::AGGREGATE_TRAVEL_WINDOW_DAY[deltaDayIdx];

      // Init pointers
      // NOTE: inclusive head, exclusive tail => [head, tail)
      int heads[deltaDay];
      int tails[deltaDay];
      memset(heads, 0, sizeof(heads));
      memset(tails, 0, sizeof(tails));

      int minuteOfDayIdx = 0;
      for (int minuteOfDay = 0; minuteOfDay < 24*60; minuteOfDay += Constant::AGGREGATE_GRANULARITY_MINUTE) {
        int minuteOfDayStart = minuteOfDay - Constant::AGGREGATE_TRAVEL_QUERY_WINDOW_MINUTE;
        int minuteOfDayEnd = minuteOfDay + Constant::AGGREGATE_TRAVEL_QUERY_WINDOW_MINUTE;

        // Update pointers
        for (int i = 0; i < deltaDay; i++) {
          while ((heads[i] < localTravels[i].size()) && (localTravels[i][heads[i]].first < minuteOfDayStart)) {
            heads[i]++;
          }
          while ((tails[i] < localTravels[i].size()) && (localTravels[i][tails[i]].first < minuteOfDayEnd)) {
            tails[i]++;
          }
        }

        // Calculate deltas
        vector<double> travelMinutes;
        int maxTravelMinutes = 0;
        for (int i = 0; i < deltaDay; i++) {
          if ((heads[i] < localTravels[i].size()) && (tails[i] < localTravels[i].size())) {
            for (int j = heads[i]; j < tails[i]; j++) {
              int travelMinute = std::max(1, localTravels[i][j].second / 60);
              travelMinutes.push_back(travelMinute);
              maxTravelMinutes = std::max(maxTravelMinutes, travelMinute);
            }
          }
        }

        const vector<Point> &pdf = Statistic::getPdf(
            travelMinutes,
            Constant::PDF_SIZE,
            Constant::KDE_WIDTH,
            0,
            Constant::MAX_TRAVEL_TIME
        );

        if (pdf.size() == 0) {
          result[minuteOfDayIdx].average[deltaDay] = Constant::MAX_TRAVEL_TIME;
          result[minuteOfDayIdx].median[deltaDay] = Constant::MAX_TRAVEL_TIME;
          result[minuteOfDayIdx].p75[deltaDay] = Constant::MAX_TRAVEL_TIME;
          result[minuteOfDayIdx].p80[deltaDay] = Constant::MAX_TRAVEL_TIME;
          result[minuteOfDayIdx].p85[deltaDay] = Constant::MAX_TRAVEL_TIME;
          result[minuteOfDayIdx].p90[deltaDay] = Constant::MAX_TRAVEL_TIME;
        } else {
          result[minuteOfDayIdx].average[deltaDay] = std::max(1, (int)Statistic::getExpectedValue(pdf));
          result[minuteOfDayIdx].median[deltaDay] = std::max(1, (int)Statistic::getXWithLeftTailArea(pdf, 0.5));
          result[minuteOfDayIdx].p75[deltaDay] = std::max(1, (int)Statistic::getXWithLeftTailArea(pdf, 0.75));
          result[minuteOfDayIdx].p80[deltaDay] = std::max(1, (int)Statistic::getXWithLeftTailArea(pdf, 0.80));
          result[minuteOfDayIdx].p85[deltaDay] = std::max(1, (int)Statistic::getXWithLeftTailArea(pdf, 0.85));
          result[minuteOfDayIdx].p90[deltaDay] = std::max(1, (int)Statistic::getXWithLeftTailArea(pdf, 0.90));
        }

        minuteOfDayIdx++;

        // for (int x : travelMinutes) {
        //   printf("%d ", x);
        // }
        // printf("\n");

        // printf(
        //     "%d\t%lf\t%lf\t%lf\t%lf\n",
        //     minuteOfDay,
        //     Statistic::getExpectedValue(pdf),
        //     Statistic::getXWithLeftTailArea(pdf, 0.5),
        //     Statistic::getXWithLeftTailArea(pdf, 0.95),
        //     Statistic::getXWithLeftTailArea(pdf, 0.99)
        // );

        // printf(
        //     "%s [%s => %s] (%02d:%02d) %d %d %d %d\n",
        //     corridor->name.c_str(),
        //     busStop->name.c_str(),
        //     destinationBusStop->name.c_str(),
        //     minuteOfDay/60,
        //     minuteOfDay%60,
        //     result[minuteOfDayIdx].average[deltaDay],
        //     result[minuteOfDayIdx].median[deltaDay],
        //     result[minuteOfDayIdx].p95[deltaDay],
        //     result[minuteOfDayIdx].p99[deltaDay]
        // );
      }
    }

    // Interpolate missing data
    for (int deltaDayIdx = 0; deltaDayIdx < Constant::AGGREGATE_TRAVEL_WINDOW_DAY.size(); deltaDayIdx++) {
      int deltaDay = Constant::AGGREGATE_TRAVEL_WINDOW_DAY[deltaDayIdx];

      int minuteOfDayIdx = 0;
      for (int minuteOfDay = 0; minuteOfDay < 24*60; minuteOfDay += Constant::AGGREGATE_GRANULARITY_MINUTE) {
        if (result[minuteOfDayIdx].average[deltaDay] == Constant::MAX_TRAVEL_TIME) {
          // Try to average from closest data
          int averageSum = 0;
          int medianSum = 0;
          int p75Sum = 0;
          int p80Sum = 0;
          int p85Sum = 0;
          int p90Sum = 0;
          int count = 0;
          for (int delta = -Constant::AGGREGATE_INTERPOLATE_WINDOW; delta <= Constant::AGGREGATE_INTERPOLATE_WINDOW; delta++) {
            int idx = (minuteOfDayIdx + delta + result.size()) % result.size();

            if (result[idx].average[deltaDay] < Constant::MAX_TRAVEL_TIME) {
              averageSum += result[idx].average[deltaDay];
              medianSum += result[idx].median[deltaDay];
              p75Sum += result[idx].p75[deltaDay];
              p80Sum += result[idx].p80[deltaDay];
              p85Sum += result[idx].p85[deltaDay];
              p90Sum += result[idx].p90[deltaDay];
              count++;
            }
          }

          if (count > 0) {
            result[minuteOfDayIdx].average[deltaDay] = std::max(1, averageSum / count);
            result[minuteOfDayIdx].median[deltaDay] = std::max(1, medianSum / count);
            result[minuteOfDayIdx].p75[deltaDay] = std::max(1, p75Sum / count);
            result[minuteOfDayIdx].p80[deltaDay] = std::max(1, p80Sum / count);
            result[minuteOfDayIdx].p85[deltaDay] = std::max(1, p85Sum / count);
            result[minuteOfDayIdx].p90[deltaDay] = std::max(1, p90Sum / count);
          }
        }

        minuteOfDayIdx++;
      }
    }

    return result;
  }

public:
  TravelTimeGenerator(Graph *graph, gregorian::date date, int maxDayBefore, string travelFolderPath) {
    mGraph = graph;
    mDate = date;
    mMaxDayBefore = maxDayBefore;

    // Populate and read mega structure
    construct();
    readData(travelFolderPath);
  }
  ~TravelTimeGenerator() {};

  vector<TravelTime> generateTravelTime() {
    vector<TravelTime> result;

    for (int i = 0; i < idxToBusStopFromTo.size(); i++) {
      // printf("from <%s> to <%s>\n", idxToBusStopFromTo[i].first->name.c_str(), idxToBusStopFromTo[i].second->name.c_str());
      const vector<TravelTime> &travelTimes = getDailyTravelTime(i);

      // Accumulate
      for (TravelTime wt : travelTimes) {
        result.push_back(wt);
      }
    }

    return result;
  }
};
