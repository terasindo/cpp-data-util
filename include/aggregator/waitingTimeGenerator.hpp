#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "math.hpp"
#include "statistic.hpp"
#include "constant.hpp"
#include "graph.hpp"
#include "waitingTime.hpp"

using std::set;
using std::pair;
using std::make_pair;
using std::vector;
namespace gregorian = boost::gregorian;
namespace posix = boost::posix_time;

class WaitingTimeGenerator {
private:
  Graph *mGraph;
  gregorian::date mDate;
  int mMaxDayBefore;

  // Structure like arrivals[9A][end][semanggi][day-15][i'th arrival minute]
  vector<vector<int>> arrivalMinutes[Constant::MAX_CORRIDOR][Constant::MAX_COMMON_BUS_STOP_IN_CORRIDOR][Constant::MAX_BUS_STOP_IN_CORRIDOR];

  // Descripting the indexes used in the mega-structure
  vector<Corridor*> idxToCorridor;
  vector<BusStop*> idxToDestination[Constant::MAX_CORRIDOR];
  vector<BusStop*> idxToBusStop[Constant::MAX_CORRIDOR][Constant::MAX_COMMON_BUS_STOP_IN_CORRIDOR];
  vector<BusStop*> idxToCorridorEnd[Constant::MAX_CORRIDOR][Constant::MAX_COMMON_BUS_STOP_IN_CORRIDOR];

  // Reverse mappings
  map<Corridor*, int> corridorToIdx;
  map<BusStop*, int> destinationToIdx[Constant::MAX_CORRIDOR];
  map<BusStop*, int> busStopToIdx[Constant::MAX_CORRIDOR][Constant::MAX_COMMON_BUS_STOP_IN_CORRIDOR];
  map<BusStop*, int> corridorEndToIdx[Constant::MAX_CORRIDOR][Constant::MAX_COMMON_BUS_STOP_IN_CORRIDOR];

  void construct() {
    idxToCorridor = mGraph->corridors;

    for (int i = 0; i < idxToCorridor.size(); i++) {
      Corridor *corridor = idxToCorridor[i];

      // Extract the common bus stop
      BusStop* currentBusStop = corridor->startBusStop;
      while (currentBusStop != NULL) {
        if (currentBusStop->isCommon) {
          idxToDestination[i].push_back(currentBusStop);
        }

        currentBusStop = currentBusStop->getNext(corridor, corridor->endBusStop);
      }
    }

    for (int i = 0; i < idxToCorridor.size(); i++) {
      for (int j = 0; j < idxToDestination[i].size(); j++) {
        Corridor *corridor = idxToCorridor[i];
        BusStop *destinationBusStop = idxToDestination[i][j];

        vector<BusStop*> busStopsInCorridor = mGraph->getBusStopsInCorridor(corridor);

        // Take all but the destination
        for (BusStop* busStop : busStopsInCorridor) {
          if (busStop != destinationBusStop) {
            idxToBusStop[i][j].push_back(busStop);

            // Figure out the final destination (busStop -> destinationBusStop -> finalDestination?)
            idxToCorridorEnd[i][j].push_back(getCorridorEnd(corridor, busStop, destinationBusStop));
          }
        }
      }
    }

    // Fill the reverse mappings
    for (int i = 0; i < idxToCorridor.size(); i++) {
      Corridor *corridor = idxToCorridor[i];
      corridorToIdx[corridor] = i;

      for (int j = 0; j < idxToDestination[i].size(); j++) {
        BusStop* destinationBusStop = idxToDestination[i][j];
        destinationToIdx[i][destinationBusStop] = j;

        for (int k = 0; k < idxToBusStop[i][j].size(); k++) {
          BusStop *busStop = idxToBusStop[i][j][k];
          busStopToIdx[i][j][busStop] = k;

          BusStop *corridorEnd = idxToCorridorEnd[i][j][k];
          corridorEndToIdx[i][j][corridorEnd] = k;
        }
      }
    }

    // Fill in slot
    for (int i = 0; i < idxToCorridor.size(); i++) {
      for (int j = 0; j < idxToDestination[i].size(); j++) {
        for (int k = 0; k < idxToBusStop[i][j].size(); k++) {
          for (int day = 0; day < mMaxDayBefore; day++) {
            vector<int> temp;
            arrivalMinutes[i][j][k].push_back(temp);
          }
        }
      }
    }
  }

  // Get where will it ends
  BusStop* getCorridorEnd(Corridor *corridor, BusStop *fromBusStop, BusStop *toBusStop) {
    // Traverse from "from" to corridor end
    // If "to" was visited, then it is the correct direction

    BusStop *currentBusStop = fromBusStop;
    while (currentBusStop != NULL) {
      if (currentBusStop == toBusStop) {
        return corridor->endBusStop;
      }
      currentBusStop = currentBusStop->getNext(corridor, corridor->endBusStop);
    }
    return corridor->startBusStop;
  }

  void readData(string arrivalFolderPath) {
    for (int deltaDay = 1; deltaDay <= mMaxDayBefore; deltaDay++) {
      gregorian::date currentDate = mDate - gregorian::date_duration(deltaDay);
      string currentDateStr = gregorian::to_iso_extended_string(currentDate);
      string filePath = arrivalFolderPath + currentDateStr + ".arrival";

      fprintf(stdout, "Reading %s\n", filePath.c_str());
      FILE *file = fopen(filePath.c_str(), "r");
      if (!file) {
        // File not exist
        fprintf(stdout, "File not found, skipped\n");

        // To compensate, add 1 more day
        mMaxDayBefore++;
        continue;
      }

      char buff[128];

      // Prepare hydration
      map<string,BusStop*> nameToBusStop;
      map<string,Corridor*> nameToCorridor;
      for (int i = 0; i < mGraph->busStops.size(); i++) {
        nameToBusStop[mGraph->busStops[i]->name] = mGraph->busStops[i];
      }
      for (int i = 0; i < mGraph->corridors.size(); i++) {
        nameToCorridor[mGraph->corridors[i]->name] = mGraph->corridors[i];
      }

      int nBusStop;
      int nCorridor;
      vector<string> idxToBusStopName;
      vector<string> idxToCorridorName;

      // Read metadata
      if (fscanf(file, "%d\n", &nBusStop) == -1) {
        // No state, quit
        fclose(file);
        fprintf(stdout, "File empty, skipped\n");
        continue;
      }
      for (int i = 0; i < nBusStop; i++) {
        fscanf(file, "%[^\n]\n", buff);

        string busStopName = buff;
        idxToBusStopName.push_back(busStopName);
      }
      fscanf(file, "%d\n", &nCorridor);
      for (int i = 0; i < nCorridor; i++) {
        fscanf(file, "%[^\n]\n", buff);

        string corridorName  = buff;
        idxToCorridorName.push_back(corridorName);
      }

      // Read actual data
      int nArrival;
      fscanf(file, "%d\n", &nArrival);
      for (int i = 0; i < nArrival; i++) {
        fscanf(file, "%[^\n]\n", buff);
        string busId = buff;

        int corridorIdx;
        int busStopIdx;
        int destinationBusStopIdx;
        int unixTime;
        fscanf(file, "%d %d %d %d\n", &corridorIdx, &busStopIdx, &destinationBusStopIdx, &unixTime);

        Corridor *corridor = nameToCorridor[idxToCorridorName[corridorIdx]];
        BusStop *busStop = nameToBusStop[idxToBusStopName[busStopIdx]];
        BusStop *destinationBusStop = nameToBusStop[idxToBusStopName[destinationBusStopIdx]];

        // Add to mega structure
        addArrival(corridor, busStop, destinationBusStop, deltaDay, unixTime);
      }

      fprintf(stdout, "Finished reading\n");
      fclose(file);
    }

    // Sort everything
    for (int i = 0; i < idxToCorridor.size(); i++) {
      for (int j = 0; j < idxToDestination[i].size(); j++) {
        for (int k = 0; k < idxToBusStop[i][j].size(); k++) {
          for (int day = 0; day < mMaxDayBefore; day++) {
            sort(arrivalMinutes[i][j][k][day].begin(), arrivalMinutes[i][j][k][day].end());
          }
        }
      }
    }
  }
  void addArrival(Corridor *corridor, BusStop *busStop, BusStop *destinationBusStop, int deltaDay, int unixTime) {
    int corridorIdx = corridorToIdx[corridor];
    int destinationIdx = destinationToIdx[corridorIdx][destinationBusStop];
    int busStopIdx = busStopToIdx[corridorIdx][destinationIdx][busStop];

    posix::ptime time = posix::ptime(Constant::UNIX_EPOCH_DATE) + posix::seconds(unixTime) + posix::hours(Constant::LOCAL_TIME_UTC_OFFSET_HOUR);
    posix::time_duration duration = time.time_of_day();
    int minuteOfDay = 60*duration.hours() + duration.minutes();

    // Propagate from closer common bus stops to the destination
    BusStop *currentBusStop = busStop;
    BusStop *corridorEnd = idxToCorridorEnd[corridorIdx][destinationIdx][busStopIdx];
    bool reachedDestination = false;
    while (!reachedDestination) {
      if (currentBusStop->isCommon && (currentBusStop != busStop)) {
        int localDestinationIdx = destinationToIdx[corridorIdx][currentBusStop];
        int localBusStopIdx = busStopToIdx[corridorIdx][localDestinationIdx][busStop];

        // printf("[%s] %s to %s, adding %s\n", corridor->name.c_str(), busStop->name.c_str(), destinationBusStop->name.c_str(), currentBusStop->name.c_str());

        arrivalMinutes[corridorIdx][localDestinationIdx][localBusStopIdx][deltaDay-1].push_back(minuteOfDay);
        if ((minuteOfDay <= Constant::MAX_WAITING_TIME) && (deltaDay >= 2)) {
          // Wrap on yesterday
          arrivalMinutes[corridorIdx][localDestinationIdx][localBusStopIdx][deltaDay-2].push_back(24*60 + minuteOfDay);
        }
      }

      reachedDestination = (currentBusStop == destinationBusStop);
      currentBusStop = currentBusStop->getNext(corridor, corridorEnd);
    }
  }

  vector<WaitingTime> getDailyWaitingTime(int corridorIdx, int destinationIdx, int busStopIdx) {
    vector<WaitingTime> result;

    Corridor *corridor = idxToCorridor[corridorIdx];
    BusStop *destinationBusStop = idxToDestination[corridorIdx][destinationIdx];
    BusStop *busStop = idxToBusStop[corridorIdx][destinationIdx][busStopIdx];

    // if (corridor->name != "9") return result;
    // if (busStop->name != "S Parman Podomoro City") return result;
    // if (destinationBusStop->name != "Pluit") return result;

    for (int minuteOfDay = 0; minuteOfDay < 24*60; minuteOfDay += Constant::AGGREGATE_GRANULARITY_MINUTE) {
      result.push_back(WaitingTime(corridor, busStop, destinationBusStop, minuteOfDay));
    }

    // Get the data
    const vector<vector<int>> &localArrivalMinutes = arrivalMinutes[corridorIdx][destinationIdx][busStopIdx];

    // Mega sliding window
    for (int deltaDayIdx = 0; deltaDayIdx < Constant::AGGREGATE_ARRIVAL_WINDOW_DAY.size(); deltaDayIdx++) {
      int deltaDay = Constant::AGGREGATE_ARRIVAL_WINDOW_DAY[deltaDayIdx];

      // Init pointers
      int heads[deltaDay];
      memset(heads, 0, sizeof(heads));

      int minuteOfDayIdx = 0;
      for (int minuteOfDay = 0; minuteOfDay < 24*60; minuteOfDay += Constant::AGGREGATE_GRANULARITY_MINUTE) {
        // Update pointers
        for (int i = 0; i < deltaDay; i++) {
          while ((heads[i] < localArrivalMinutes[i].size()) && (localArrivalMinutes[i][heads[i]] <= minuteOfDay)) {
            heads[i]++;
          }
        }

        // Calculate deltas
        vector<double> deltaMinutes;
        deltaMinutes.reserve(deltaDay);
        for (int i = 0; i < deltaDay; i++) {
          if ((heads[i] < localArrivalMinutes[i].size()) && (localArrivalMinutes[i][heads[i]] > minuteOfDay)) {
            deltaMinutes.push_back(localArrivalMinutes[i][heads[i]] - minuteOfDay);
          }
        }

        const vector<Point> &pdf = Statistic::getPdf(
            deltaMinutes,
            Constant::PDF_SIZE,
            Constant::KDE_WIDTH,
            0,
            Constant::MAX_WAITING_TIME
        );

        if (pdf.size() == 0) {
          result[minuteOfDayIdx].average[deltaDay] = Constant::MAX_WAITING_TIME;
          result[minuteOfDayIdx].median[deltaDay] = Constant::MAX_WAITING_TIME;
          result[minuteOfDayIdx].p75[deltaDay] = Constant::MAX_WAITING_TIME;
          result[minuteOfDayIdx].p80[deltaDay] = Constant::MAX_WAITING_TIME;
          result[minuteOfDayIdx].p85[deltaDay] = Constant::MAX_WAITING_TIME;
          result[minuteOfDayIdx].p90[deltaDay] = Constant::MAX_WAITING_TIME;
        } else {
          result[minuteOfDayIdx].average[deltaDay] = std::max(1, (int)Statistic::getExpectedValue(pdf));
          result[minuteOfDayIdx].median[deltaDay] = std::max(1, (int)Statistic::getXWithLeftTailArea(pdf, 0.5));
          result[minuteOfDayIdx].p75[deltaDay] = std::max(1, (int)Statistic::getXWithLeftTailArea(pdf, 0.75));
          result[minuteOfDayIdx].p80[deltaDay] = std::max(1, (int)Statistic::getXWithLeftTailArea(pdf, 0.80));
          result[minuteOfDayIdx].p85[deltaDay] = std::max(1, (int)Statistic::getXWithLeftTailArea(pdf, 0.85));
          result[minuteOfDayIdx].p90[deltaDay] = std::max(1, (int)Statistic::getXWithLeftTailArea(pdf, 0.90));
        }

        minuteOfDayIdx++;
        // for (int x : deltaMinutes) {
        //   printf("%d ", x);
        // }
        // printf("\n");

        // printf(
        //     "%d\t%d\t%d\t%d\t%d\n",
        //     minuteOfDay,
        //     result[minuteOfDayIdx].average[deltaDay],
        //     result[minuteOfDayIdx].median[deltaDay],
        //     result[minuteOfDayIdx].p95[deltaDay],
        //     result[minuteOfDayIdx].p99[deltaDay]
        // );

        // printf(
        //     "%s [%s => %s] (%02d:%02d) %d %d %d %d\n",
        //     corridor->name.c_str(),
        //     busStop->name.c_str(),
        //     destinationBusStop->name.c_str(),
        //     minuteOfDay/60,
        //     minuteOfDay%60,
        //     result[minuteOfDayIdx].average[deltaDay],
        //     result[minuteOfDayIdx].median[deltaDay],
        //     result[minuteOfDayIdx].p95[deltaDay],
        //     result[minuteOfDayIdx].p99[deltaDay]
        // );
      }
    }

    // Interpolate missing data
    for (int deltaDayIdx = 0; deltaDayIdx < Constant::AGGREGATE_ARRIVAL_WINDOW_DAY.size(); deltaDayIdx++) {
      int deltaDay = Constant::AGGREGATE_ARRIVAL_WINDOW_DAY[deltaDayIdx];

      int minuteOfDayIdx = 0;
      for (int minuteOfDay = 0; minuteOfDay < 24*60; minuteOfDay += Constant::AGGREGATE_GRANULARITY_MINUTE) {
        if (result[minuteOfDayIdx].average[deltaDay] == Constant::MAX_WAITING_TIME) {
          // Try to average from closest data
          int averageSum = 0;
          int medianSum = 0;
          int p75Sum = 0;
          int p80Sum = 0;
          int p85Sum = 0;
          int p90Sum = 0;
          int count = 0;
          for (int delta = -Constant::AGGREGATE_INTERPOLATE_WINDOW; delta <= Constant::AGGREGATE_INTERPOLATE_WINDOW; delta++) {
            int idx = (minuteOfDayIdx + delta + result.size()) % result.size();

            if (result[idx].average[deltaDay] < Constant::MAX_WAITING_TIME) {
              averageSum += result[idx].average[deltaDay];
              medianSum += result[idx].median[deltaDay];
              p75Sum += result[idx].p75[deltaDay];
              p80Sum += result[idx].p80[deltaDay];
              p85Sum += result[idx].p85[deltaDay];
              p90Sum += result[idx].p90[deltaDay];
              count++;
            }
          }

          if (count > 0) {
            result[minuteOfDayIdx].average[deltaDay] = std::max(1, averageSum / count);
            result[minuteOfDayIdx].median[deltaDay] = std::max(1, medianSum / count);
            result[minuteOfDayIdx].p75[deltaDay] = std::max(1, p75Sum / count);
            result[minuteOfDayIdx].p80[deltaDay] = std::max(1, p80Sum / count);
            result[minuteOfDayIdx].p85[deltaDay] = std::max(1, p85Sum / count);
            result[minuteOfDayIdx].p90[deltaDay] = std::max(1, p90Sum / count);
          }
        }

        minuteOfDayIdx++;
      }
    }

    return result;
  }

public:
  WaitingTimeGenerator(Graph *graph, gregorian::date date, int maxDayBefore, string arrivalFolderPath) {
    mGraph = graph;
    mDate = date;
    mMaxDayBefore = maxDayBefore;

    // Populate and read mega structure
    construct();
    readData(arrivalFolderPath);
  }
  ~WaitingTimeGenerator() {};

  vector<WaitingTime> generateWaitingTime() {
    vector<WaitingTime> result;

    // Foreach c:corridor
    //   Foreach o:orientation
    //     Foreach b:corridor.busStops
    //       result += waitingTimeDaily(from=b, to=o.end, corridor=c, getWaitingTime(c, b, o.end))

    for (int i = 0; i < idxToCorridor.size(); i++) {
      for (int j = 0; j < idxToDestination[i].size(); j++) {
        for (int k = 0; k < idxToBusStop[i][j].size(); k++) {
          // printf("%s, from <%s> to <%s>\n", idxToCorridor[i]->name.c_str(), idxToBusStop[i][j][k]->name.c_str(), idxToDestination[i][j].second->name.c_str());
          const vector<WaitingTime> &waitingTimes = getDailyWaitingTime(i, j, k);

          // Accumulate
          for (WaitingTime wt : waitingTimes) {
            result.push_back(wt);
          }
        }
      }
    }

    return result;
  }
};
