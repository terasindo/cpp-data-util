#pragma once

#include <vector>
#include <string>
#include <unordered_map>

#include "point.hpp"
#include "busStop.hpp"

using std::string;
using std::vector;

class Point;
class BusStop;
class Corridor {
private:
  vector<Point> polylineAll;
public:
  int index;
  string name;
  string slug;
  string fullName;
  BusStop* startBusStop;
  BusStop* endBusStop;
  vector<Point> polylineFromStart;
  vector<Point> polylineFromEnd;

  Corridor() :
      Corridor(0, 0, "") {}

  Corridor(string _name) :
      Corridor(0, 0, _name) {}

  Corridor(BusStop* _startBusStop, BusStop* _endBusStop) :
      Corridor(_startBusStop, _endBusStop, "") {}

  Corridor(BusStop* _startBusStop, BusStop* _endBusStop, string _name) :
      startBusStop(_startBusStop), endBusStop(_endBusStop), name(_name) {}

  ~Corridor () {
    polylineFromStart.clear();
    polylineFromEnd.clear();
  }

  vector<Point> getPolyline() {
    if (polylineAll.size() == polylineFromStart.size() + polylineFromEnd.size()) {
      return polylineAll;
    }

    polylineAll = polylineFromStart;
    polylineAll.insert(polylineAll.end(), polylineFromEnd.begin(), polylineFromEnd.end());
    return polylineAll;
  }
};
