#pragma once

#include <vector>

#include "stateBundle.hpp"
#include "arrival.hpp"
#include "travel.hpp"

using std::vector;

class ExtractorResult {
public:
  StateBundle finalState;
  vector<Arrival> arrivals;
  vector<Travel> travels;

  ExtractorResult() {};
  ~ExtractorResult() {};
};
