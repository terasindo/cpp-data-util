#pragma once

#include <algorithm>
#include <vector>
#include <cmath>

#include "point.hpp"

using std::vector;

class Math {
public:
  static constexpr double PI = acos(-1.0);
  static constexpr double EPS = 1e-14;

  static double clamp(double a, double b, double x) {
    return std::max(a, std::min(x, b));
  }

  static bool eq(double a, double b, double tolerance) {
    return fabs(a - b) < tolerance;
  }

  static bool gt(double a, double b, double tolerance) {
    return !eq(a, b, tolerance) && (a > b);
  }

  /**
   * Returns distance from a point to a line. Remember that line is infinite.
   */
  static double pointToLineDistance(const Point &point, const Point &lineEndA, const Point &lineEndB) {
    Point a = lineEndB - lineEndA;
    Point b = point - lineEndA;

    return fabs(a.cross(b)) / a.norm();
  }

  static double pointToPolylineDistance(const Point &point, const vector<Point> &polyline) {
    double distance = pointToLineSegmentDistance(point, polyline[0], polyline[1]);
    for (int i = 2; i < polyline.size(); i++) {
      distance = std::min(distance, pointToLineSegmentDistance(point, polyline[i-1], polyline[i]));
    }
    return distance;
  }

  // Why bother creating this? Because it return as early as possible
  static bool isPointToPolylineDistanceLessThan(const Point &point, const vector<Point> &polyline, double value) {
    double distance = pointToLineSegmentDistance(point, polyline[0], polyline[1]);
    for (int i = 2; i < polyline.size(); i++) {
      distance = std::min(distance, pointToLineSegmentDistance(point, polyline[i-1], polyline[i]));

      if (distance < value) {
        return true;
      }
    }
    return (distance < value);
  }

  /**
   * Point P formed shadow with line (A, B) if the projection of AP to AB is totally inside AB
   *
   * P to line (A, B) forms shadow (symbolized with '*'):
   *    P
   *   /
   *  /
   * A--*-----B
   *
   * P to line (A, B) doesn't form shadow:
   *    P
   *     \
   *      \
   *       A--------B
   */
  static bool isPointToLineSegmentFormedShadow(const Point &point, const Point &segmentEndA, const Point &segmentEndB) {
    Point AB = segmentEndB - segmentEndA;
    double scalarProjectionResult = scalarProjection(point, segmentEndA, segmentEndB);
    return (0 <= scalarProjectionResult) && (scalarProjectionResult <= AB.norm());
  };

  /**
   * Returns the shortest distance required for the point to travel and touch the line segment.
   */
  static double pointToLineSegmentDistance(const Point &point, const Point &segmentEndA, const Point &segmentEndB) {
    if (isPointToLineSegmentFormedShadow(point, segmentEndA, segmentEndB)) {
      return pointToLineDistance(point, segmentEndA, segmentEndB);
    } else {
      return std::min(point.distance(segmentEndA), point.distance(segmentEndB));
    }
  };

  /**
   * Given point P and line segment(A, B).
   * |AP|*cos(theta), where theta is angle formed by AB to AP.
   *
   * In these examples, |AB| = 5:
   *    P
   *   /
   *  /           The result is 3
   * A==>--B
   *
   *    P
   *     \        The result is -3
   *      \
   *    <==A-----B
   *
   *         P
   *       _/
   *     _/
   *   _/
   *  /           The result is 8
   * A====B==>
   */
  static double scalarProjection(const Point &point, const Point &segmentEndA, const Point &segmentEndB) {
    Point AB = segmentEndB - segmentEndA;
    Point AP = point - segmentEndA;

    return AB.dot(AP) / AB.norm();
  };

  static double turnAngle(const Point &pointA, const Point &pointB, const Point &pointC) {
    Point AB = pointB - pointA;
    Point BC = pointC - pointB;

    double angle = acos(AB.dot(BC) / (AB.norm() * BC.norm()));
    return 180 - (angle * 180 / PI);
  };

  /**
   * Given polyline and basePolyline.
   *
   * Imagine if basePolyline is a road.
   * You are walking in that road from a position to another position, with each step recorded and later becomes polyline.
   * You may start and end at any point of that road.
   *
   * For each step you make, return the distance from that point to the road's starting point which make the most sense.
   *
   * Algorithm:
   * For each point P in polyline, find one segment AB in basePolyline which is closest to it.
   * Now computes the length from basePolyline's first segment to segment before AB, then adds it with scalarProjection(P, A,B)
   *
   * To speed it up, process P in increasing fashion + always believe the closest segment is always located in/after last processed segment.
   *
   * Note:
   * This method is purely improvisation, there is no such thing as polyline-polyline projection in real world.
   * Algorithm may change, as long as it makes sense.
   */
  static vector<double> polylinePolylineProjection(vector<Point> &polyline, vector<Point> &basePolyline) {
    vector<double> result;
    result.reserve(polyline.size());

    int j = 0; // Current active segment is (basePolyline[j], basePolyline[j+1])

    double currentSegmentLength = basePolyline[j].distance(basePolyline[j+1]);
    double distanceFullyCovered = 0;

    // Process polyline in increasing fashion
    for (int i = 0; i < polyline.size(); i++) {
      const Point &point = polyline[i];

      // Sorry for being ugly, but this is required for performance
      if (j+2 < basePolyline.size()) {
        double currentDistance = pointToLineSegmentDistance(point, basePolyline[j], basePolyline[j+1]);
        double nextDistance = pointToLineSegmentDistance(point, basePolyline[j+1], basePolyline[j+2]);

        // While current active segment is worse candidate for point's closest segment than the next one, move forward
        while ((j+2 < basePolyline.size()) && (currentDistance + EPS > nextDistance)) {
          distanceFullyCovered += basePolyline[j].distance(basePolyline[j+1]);
          j++;

          currentSegmentLength = basePolyline[j].distance(basePolyline[j+1]);

          currentDistance = nextDistance;
          if (j+2 < basePolyline.size()) {
            nextDistance = pointToLineSegmentDistance(point, basePolyline[j+1], basePolyline[j+2]);
          }
        }
      }

      double distancePartiallyCovered = clamp(
        0,
        scalarProjection(point, basePolyline[j], basePolyline[j+1]),
        currentSegmentLength
      );

      // Now return the distance covered from basePolyline[0] to this point
      result.push_back(distanceFullyCovered + distancePartiallyCovered);
    }

    return result;
  }
};
