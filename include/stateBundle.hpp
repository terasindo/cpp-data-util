#pragma once

#include <cstdio>
#include <map>
#include <unordered_map>

#include "position.hpp"
#include "graph.hpp"

using std::string;
using std::vector;
using std::map;
using std::unordered_map;

class StateBundle {
private:
  unordered_map<string, map<Corridor*, vector<Position>>> stateByBusId;

public:
  StateBundle() {};
  ~StateBundle() {};

  void readFromFile(Graph *graph, string filePath) {
    fprintf(stdout, "Reading state %s\n", filePath.c_str());
    stateByBusId.clear();

    FILE *file = fopen(filePath.c_str(), "r");
    if (!file) {
      // File not exist
      fprintf(stdout, "File not found, skipped\n");
      return;
    }

    char buff[128];

    // Prepare hydration
    map<string,BusStop*> nameToBusStop;
    map<string,Corridor*> nameToCorridor;
    for (int i = 0; i < graph->busStops.size(); i++) {
      nameToBusStop[graph->busStops[i]->name] = graph->busStops[i];
    }
    for (int i = 0; i < graph->corridors.size(); i++) {
      nameToCorridor[graph->corridors[i]->name] = graph->corridors[i];
    }

    int nBusStop;
    int nCorridor;
    vector<string> idxToBusStopName;
    vector<string> idxToCorridorName;

    // Read metadata
    if (fscanf(file, "%d\n", &nBusStop) == -1) {
      // No state, quit
      fclose(file);
      fprintf(stdout, "File empty, skipped\n");
      return;
    }
    for (int i = 0; i < nBusStop; i++) {
      fscanf(file, "%[^\n]\n", buff);

      string busStopName = buff;
      idxToBusStopName.push_back(busStopName);
    }
    fscanf(file, "%d\n", &nCorridor);
    for (int i = 0; i < nCorridor; i++) {
      fscanf(file, "%[^\n]\n", buff);

      string corridorName  = buff;
      idxToCorridorName.push_back(corridorName);
    }

    // Read actual data
    int nBus;
    fscanf(file, "%d\n", &nBus);
    for (int i = 0; i < nBus; i++) {
      fscanf(file, "%[^\n]\n", buff);
      string busId = buff;

      int nCorridorState;
      fscanf(file, "%d\n", &nCorridorState);
      for (int j = 0; j < nCorridorState; j++) {
        int corridorIdx;
        int nPosition;
        fscanf(file, "%d %d\n", &corridorIdx, &nPosition);

        Corridor *corridor = nameToCorridor[idxToCorridorName[corridorIdx]];
        vector<Position> &positionRef = stateByBusId[busId][corridor];
        for (int k = 0; k < nPosition; k++) {
          double latitude;
          double longitude;
          int unixTime;
          int closestBusStopIdx;
          fscanf(file, "%lf %lf %d %d\n", &latitude, &latitude, &unixTime, &closestBusStopIdx);

          string busStopName = idxToBusStopName[closestBusStopIdx];
          positionRef.push_back(Position(unixTime, latitude, longitude, nameToBusStop[busStopName]));
        }
      }
    }

    fclose(file);
    fprintf(stdout, "Finished reading\n");
  }

  void writeToFile(Graph *graph, string filePath) {
    fprintf(stdout, "Writing state %s\n", filePath.c_str());
    FILE *file = fopen(filePath.c_str(), "w+");

    map<BusStop*,int> busStopToIdx;
    map<Corridor*,int> corridorToIdx;

    // Write metadata
    fprintf(file, "%d\n", (int)graph->busStops.size());
    for (int i = 0; i < graph->busStops.size(); i++) {
      busStopToIdx[graph->busStops[i]] = i;
      fprintf(file, "%s\n", graph->busStops[i]->name.c_str());
    }
    fprintf(file, "%d\n", (int)graph->corridors.size());
    for (int i = 0; i < graph->corridors.size(); i++) {
      corridorToIdx[graph->corridors[i]] = i;
      fprintf(file, "%s\n", graph->corridors[i]->name.c_str());
    }

    // Write actual data
    fprintf(file, "%d\n", (int)stateByBusId.size());
    for (auto busIdAndState : stateByBusId) {
      string busId = busIdAndState.first;
      fprintf(file, "%s\n", busId.c_str());

      fprintf(file, "%d\n", (int)busIdAndState.second.size());
      for (auto corridorAndPositions : busIdAndState.second) {
        Corridor *corridor = corridorAndPositions.first;
        vector<Position> &positions = corridorAndPositions.second;

        fprintf(file, "%d %d\n", corridorToIdx[corridor], (int)positions.size());
        for (Position position : positions) {
          fprintf(file, "%lf %lf %d %d\n", position.x, position.y, position.unixTime, busStopToIdx[position.closestBusStop]);
        }
      }
    }

    fclose(file);
  }

  void erase(string busId) {
    stateByBusId.erase(busId);
  }

  vector<string> getBusIds() {
    vector<string> vec;
    vec.reserve(stateByBusId.size());
    for (auto kv : stateByBusId) {
      vec.push_back(kv.first);
    }
    return vec;
  }

  map<Corridor*, vector<Position>> getState(string busId) {
    return stateByBusId[busId];
  }

  vector<Position> getCorridorState(string busId, Corridor* corridor) {
    return stateByBusId[busId][corridor];
  }

  void addCorridorStateByCloning(string busId, Corridor *corridor, const vector<Position> &state) {
    vector<Position> &positionsRef = stateByBusId[busId][corridor];

    positionsRef.clear();
    positionsRef.reserve(state.size());

    for (int i = 0; i < state.size(); i++) {
      positionsRef.push_back(state[i]);
    }
  }

  void addStateByCloning(string busId, const map<Corridor*, vector<Position>> &state) {
    map<Corridor*, vector<Position>> &stateRef = stateByBusId[busId];

    stateRef.clear();
    for (auto kv : state) {
      stateRef[kv.first].clear();

      for (int i = 0; i < kv.second.size(); i++) {
        stateRef[kv.first].push_back(kv.second[i]);
      }
    }
  }
};
