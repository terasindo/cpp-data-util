#pragma once

#include "stdio.h"
#include "math.h"

class Point {
public:
  double x;
  double y;

  Point() : Point(0, 0) {};
  Point(double _x, double _y) : x(_x), y(_y) {};
  ~Point() {};

  double norm() const {
    return sqrt(normSq());
  }

  double normSq() const {
    return x*x + y*y;
  }

  double distance(const Point &o) const {
    return sqrt(distanceSq(o));
  }

  double distanceSq(const Point &o) const {
    double dx = o.x - x;
    double dy = o.y - y;
    return dx*dx + dy*dy;
  }

  double cross(const Point &o) const {
    return x*o.y - y*o.x;
  }

  double dot(const Point &o) const {
    return x*o.x + y*o.y;
  }

  Point operator-(const Point &o) const {
    return Point(x - o.x, y - o.y);
  }
};
