#pragma once

#include <string>
#include <map>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>

#include "busStop.hpp"
#include "corridor.hpp"

using std::map;
using std::string;

class TravelTime {
public:
  BusStop* fromBusStop;
  BusStop* toBusStop;
  int minuteOfDay;

  map<int, int> average;
  map<int, int> median;
  map<int, int> p75;
  map<int, int> p80;
  map<int, int> p85;
  map<int, int> p90;

  TravelTime(BusStop* _fromBusStop, BusStop* _toBusStop, int _minuteOfDay) :
      fromBusStop(_fromBusStop),
      toBusStop(_toBusStop),
      minuteOfDay(_minuteOfDay)
    {
  }
  ~TravelTime() {};
};
