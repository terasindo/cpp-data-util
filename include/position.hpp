#pragma once
#include "point.hpp"
#include "busStop.hpp"
#include "constant.hpp"

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

class Position : public Point {
public:
  int unixTime;
  BusStop *closestBusStop;

  Position(int _unixTime) :
      Position(_unixTime, 0, 0, 0) {}

  Position(int _unixTime, double _latitude, double _longitude) :
      Position(_unixTime, _latitude, _longitude, 0) {}

  Position(BusStop *_closestBusStop, double _latitude, double _longitude) :
      Position(-1, _latitude, _longitude, _closestBusStop) {}

  Position(BusStop *_closestBusStop, int _unixTime) :
      Position(_unixTime, 0, 0, _closestBusStop) {}

  Position(int _unixTime, double _latitude, double _longitude, BusStop *_closestBusStop) :
      unixTime(_unixTime),
      closestBusStop(_closestBusStop),
      Point(_latitude, _longitude)
    {
  }

  Position() : Position(0, 0, 0, 0) {};
  ~Position() {};

  boost::posix_time::ptime getTime() {
    return boost::posix_time::ptime(Constant::UNIX_EPOCH_DATE) + boost::posix_time::seconds(unixTime);
  }

  boost::posix_time::ptime getTime(int utcOffsetHour) {
    return boost::posix_time::ptime(Constant::UNIX_EPOCH_DATE)
        + boost::posix_time::seconds(unixTime)
        + boost::posix_time::hours(utcOffsetHour);
  }

  string getReadableTime() {
    boost::posix_time::ptime time = getTime();
    return boost::posix_time::to_simple_string(time);
  }

  string getReadableTime(int utcOffsetHour) {
    boost::posix_time::ptime time = getTime(utcOffsetHour);
    return boost::posix_time::to_simple_string(time);
  }
};
