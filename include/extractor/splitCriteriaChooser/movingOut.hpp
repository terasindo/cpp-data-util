#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>

#include "math.hpp"
#include "constant.hpp"
#include "descriptor.hpp"
#include "graph.hpp"
#include "positionLogBundle.hpp"
#include "stateBundle.hpp"
#include "segment.hpp"
#include "extractorResult.hpp"
#include "extractor/splitCriteria.hpp"
#include "extractor/splitCriteriaChooser/_common.hpp"

using std::pair;
using std::vector;
using std::make_pair;

namespace SplitCriteriaType {
  class MovingOut {
  private:
    /*
      This is finding closest point given a hint on which is the probably closest one
      Uses triangle inequality property to shrink search space
      Consult Gozali's thesis
      This algorithm has been modified to handle line segments instead of points
     */
    class TIClosestDistanceFinder {
    private:
      int allCount;
      int didCount;

      vector<Point> polyline;
      vector<vector<pair<double,int>>> closestNeighbor;
      int lastClosestSegment; // A segment is (polyline[x], polyline[x+1])
    public:
      TIClosestDistanceFinder(vector<Point> _polyline) : polyline(_polyline) {
        for (int i = 0; i < polyline.size(); i++) {
          vector<pair<double,int>> vec;
          for (int j = 0; j < (int)polyline.size()-1; j++) {
            double dist = Math::pointToLineSegmentDistance(polyline[i], polyline[j], polyline[j+1]);
            vec.push_back(make_pair(dist, j));
          }

          sort(vec.begin(), vec.end());
          closestNeighbor.push_back(vec);
        }

        // No info, no heuristic
        // Simply set to 0
        lastClosestSegment = 0;
      }

      bool getClosestDistanceLessThan(const Point &p, double value) {
        // Snap to closest point between polyline[lastClosestSegment] or polyline[lastClosestSegment+1]
        int closestPoint;
        if (p.distanceSq(polyline[lastClosestSegment]) < p.distanceSq(polyline[lastClosestSegment+1])) {
          closestPoint = lastClosestSegment;
        } else {
          closestPoint = lastClosestSegment + 1;
        }

        // Radially iterate outward from closestPoint, checking each line segment from the closest
        // If distance(closestPoint, i) > 2*distance(closestPoint, p), then the rest of unchecked point is guaranteed to be not a solution
        double limit = 2 * p.distance(polyline[closestPoint]);
        double result = -1;
        for (int i = 0; i < closestNeighbor[closestPoint].size(); i++) {
          if (closestNeighbor[closestPoint][i].first > limit) {
            break;
          }

          int pointIdx = closestNeighbor[closestPoint][i].second;
          double dist = Math::pointToLineSegmentDistance(p, polyline[pointIdx], polyline[pointIdx+1]);
          if ((result < 0) || (dist < result)) {
            result = dist;
            lastClosestSegment = pointIdx;

            if (result < value) {
              return true;
            }
          }
        }

        return false;
      }
    };

    Corridor* corridor;
    Position* positions;
    vector<Point> polyline;
    TIClosestDistanceFinder closestDistanceFinder;

    double tooFarRoughThreshold = Constant::DEFAULT_TOO_FAR_ROUGH_THRESHOLD;

  public:
    MovingOut(Corridor* _corridor, Position* _positions) :
      corridor(_corridor),
      positions(_positions),
      polyline(_corridor->getPolyline()),
      closestDistanceFinder(_corridor->getPolyline())
      {
    };
    ~MovingOut() {};

    void setTooFarRoughThreshold(double value) {
      tooFarRoughThreshold = value;
    }

    SplitCriteria getSplitCriteria(int head, int tail) {
      if (tail - head + 1 == 0) {
        return SplitCriteria();
      }

      Position lastActivity = positions[tail];
      if (Constant::USE_TRIANGLE_INEQUALITY_OPTIMIZATION) {
        if (!closestDistanceFinder.getClosestDistanceLessThan(lastActivity, tooFarRoughThreshold)) {
          return SplitCriteria(tail - head, 1);
        }
      } else {
        if (!Math::isPointToPolylineDistanceLessThan(lastActivity, polyline, tooFarRoughThreshold)) {
          return SplitCriteria(tail - head, 1);
        }
      }

      return SplitCriteria();
    }
  };
}
