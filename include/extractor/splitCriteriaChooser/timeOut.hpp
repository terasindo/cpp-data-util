#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>

#include "math.hpp"
#include "constant.hpp"
#include "descriptor.hpp"
#include "graph.hpp"
#include "positionLogBundle.hpp"
#include "stateBundle.hpp"
#include "segment.hpp"
#include "extractorResult.hpp"
#include "extractor/splitCriteria.hpp"
#include "extractor/splitCriteriaChooser/_common.hpp"

using std::set;
using std::pair;
using std::vector;

namespace SplitCriteriaType {
  class TimeOut {
  private:
    Corridor* corridor;
    Position* positions;

    int stateExpirationDurationSecond = Constant::DEFAULT_STATE_EXPIRATION_DURATION_SECOND;
    // This split criteria type is stateless

  public:
    TimeOut(Corridor* _corridor, Position* _positions) : corridor(_corridor), positions(_positions) {
    };
    ~TimeOut() {};

    void setStateExpirationDurationSecond(int value) {
      stateExpirationDurationSecond = value;
    }

    SplitCriteria getSplitCriteria(int head, int tail) {
      if (tail - head + 1 < 2) {
        return SplitCriteria();
      }

      Position secondLast = positions[tail - 1];
      Position last = positions[tail];

      if (last.unixTime - secondLast.unixTime < stateExpirationDurationSecond) {
        return SplitCriteria();
      }

      return SplitCriteria(tail - head, 1);
    }
  };
}
