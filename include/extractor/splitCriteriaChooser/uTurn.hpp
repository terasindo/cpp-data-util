#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>

#include "math.hpp"
#include "constant.hpp"
#include "descriptor.hpp"
#include "graph.hpp"
#include "positionLogBundle.hpp"
#include "stateBundle.hpp"
#include "segment.hpp"
#include "extractorResult.hpp"
#include "extractor/splitCriteria.hpp"
#include "extractor/splitCriteriaChooser/_common.hpp"

using std::set;
using std::pair;
using std::vector;

namespace SplitCriteriaType {
  class UTurn {
  private:
    class FalseUTurnData {
    public:
      string corridorName;
      set<string> midBusStopNames;
      set<string> rightBusStopNames;

      int getBusStopFamily(string busStopName) {
        if (midBusStopNames.count(busStopName) == 1) {
          return 0;
        }
        if (rightBusStopNames.count(busStopName) == 1) {
          return 1;
        }
        // Must be on left
        return -1;
      }
    };

    Corridor* corridor;
    Position* positions;

    // State
    int curHead;
    int curTail;
    bool seen[Constant::MAX_BUS_STOP];
    vector<int> indexes;

    // False U turn
    bool canHaveFalseUTurn;
    vector<FalseUTurnData> falseUTurnDataList;

    // Constants
    const int INF = 2123123123;
    double tooFarRoughThreshold = Constant::DEFAULT_TOO_FAR_ROUGH_THRESHOLD;
    double tooFarFineThreshold = Constant::DEFAULT_TOO_FAR_FINE_THRESHOLD;
    double soCloseThreshold = Constant::DEFAULT_SO_CLOSE_THRESHOLD;
    double teleportationMinDistance = Constant::DEFAULT_TELEPORTATION_MIN_DISTANCE;
    double teleportationMinDistanceSq = teleportationMinDistanceSq * teleportationMinDistanceSq;
    int servingThreshold = Constant::DEFAULT_SERVING_THRESHOLD;

  public:
    UTurn(Corridor* _corridor, Position* _positions) : corridor(_corridor), positions(_positions) {
      curHead = -1;
      curTail = -1;
      memset(seen, 0, sizeof(seen));
      indexes.clear();
      indexes.reserve(Constant::MAX_BUS_STOP);

      // Register possible falsey u-turn
      addFalseUTurnData();
      canHaveFalseUTurn = falseUTurnDataList.size() > 0;
    };
    ~UTurn() {};

    SplitCriteria getSplitCriteria(int head, int tail) {
      if (tail - head + 1 <= 2) {
        return SplitCriteria();
      }

      if ((curHead != head) || (curTail > tail)) {
        // State has expired/invalid, renew...
        curHead = head;
        curTail = head;
        memset(seen, 0, sizeof(seen));
        indexes.clear();
      }

      int cycleHeadIdx;
      bool cycleDetected = false;
      while (curTail <= tail) { // Forward as necessary using previous state
        BusStop* &closestBusStopRef = positions[curTail].closestBusStop;
        if ((indexes.size() == 0) || (closestBusStopRef != positions[indexes.back()].closestBusStop)) {
          if (seen[closestBusStopRef->index]) {
            cycleDetected = true;
            cycleHeadIdx = curTail;
            break;
          }
          indexes.push_back(curTail);
          seen[closestBusStopRef->index] = true;
        } else {
          indexes[indexes.size() - 1] = curTail;
        }

        curTail++;
      }

      if (!cycleDetected) {
        return SplitCriteria();
      }

      vector<int> cycleElements;
      cycleElements.push_back(cycleHeadIdx);
      int i = (int)indexes.size() - 1;
      while ((i >= 0) && (positions[indexes[i]].closestBusStop != positions[cycleHeadIdx].closestBusStop)) {
        cycleElements.push_back(indexes[i]);
        i--;
      }

      reverse(cycleElements.begin(), cycleElements.end());

      // printf("Cycle elements:\n");
      // for (int i : cycleElements) {
      //   printf("%s, ", positions[i].closestBusStop->name.c_str());
      // }
      // printf("\n\n");

      if (isFalseUTurn(head, tail, cycleElements)) {
        return SplitCriteria();
      }

      // Cut the U turn
      int snipIdx = -1;
      for (int i = cycleElements.size() - 1; i >= 0; i--) { // The end tend to be a terminal
        if (positions[cycleElements[i]].closestBusStop->isTerminal) {
          // Terminal found, we snip there
          snipIdx = SplitCriteriaType::Common::findClosestFromEnd(positions, head, positions[cycleElements[i]].closestBusStop, cycleElements[i]);
          break;
        }
      }

      // If there is no terminal, snip it in the cycle end
      if (snipIdx == -1) {
        int sz = cycleElements.size() - 2;
        snipIdx = SplitCriteriaType::Common::findClosestFromEnd(positions, head, positions[cycleElements[sz]].closestBusStop, cycleElements[sz]);
      }

      // ---H----S--T
      //    xxxxxx    takeLeft should have this amount
      //         xxxx takeRight should have this amount
      return SplitCriteria(snipIdx - head + 1, tail - snipIdx + 1);
    }

    void addFalseUTurnData() {
      if (corridor->name == "12") {
        FalseUTurnData data;
        data.corridorName = "12";

        data.rightBusStopNames.insert("Plumpang Pertamina");
        data.rightBusStopNames.insert("Walikota Jakarta Utara");
        data.rightBusStopNames.insert("Permai Koja");
        data.rightBusStopNames.insert("Enggano");
        data.rightBusStopNames.insert("Tanjung Priok");

        data.midBusStopNames.insert("Sunter Boulevard Barat");
        data.midBusStopNames.insert("Sunter Kelapa Gading");

        falseUTurnDataList.push_back(data);
      }
    }

    bool isFalseUTurn(int head, int tail, const vector<int> &cycleElements) {
      for (FalseUTurnData falseUTurnData : falseUTurnDataList) {
        int headFamily = falseUTurnData.getBusStopFamily(positions[head].closestBusStop->name);
        int tailFamily = falseUTurnData.getBusStopFamily(positions[tail].closestBusStop->name);

        bool cycleElementsAreMid = true;
        for (int cycleElement : cycleElements) {
          string busStopName = positions[cycleElement].closestBusStop->name;
          cycleElementsAreMid = cycleElementsAreMid && (falseUTurnData.getBusStopFamily(busStopName) == 0);
        }

        if ((headFamily * tailFamily != 1) && cycleElementsAreMid) {
          return true;
        }
      }
      return false;
    }
  };
}
