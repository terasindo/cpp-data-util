#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>

#include "math.hpp"
#include "constant.hpp"
#include "descriptor.hpp"
#include "graph.hpp"
#include "positionLogBundle.hpp"
#include "stateBundle.hpp"
#include "segment.hpp"
#include "extractorResult.hpp"
#include "extractor/splitCriteria.hpp"
#include "extractor/splitCriteriaChooser/_common.hpp"

using std::set;
using std::pair;
using std::vector;

namespace SplitCriteriaType {
  class SecondLastIsTerminal {
  private:
    Corridor* corridor;
    Position* positions;

    // This split criteria type is stateless

  public:
    SecondLastIsTerminal(Corridor* _corridor, Position* _positions) : corridor(_corridor), positions(_positions) {
    };
    ~SecondLastIsTerminal() {};

    SplitCriteria getSplitCriteria(int head, int tail) {
      if (tail - head + 1 <= 2) {
        return SplitCriteria();
      }

      Position first = positions[head];
      Position secondLast = positions[tail - 1];
      Position last = positions[tail];

      bool isTerminal = (secondLast.closestBusStop == corridor->startBusStop) ||
                        (secondLast.closestBusStop == corridor->endBusStop);
      bool isMoving = (secondLast.closestBusStop != first.closestBusStop) &&
                      (secondLast.closestBusStop != last.closestBusStop);

      if (!isMoving || !isTerminal) {
        return SplitCriteria();
      }

      int closestIdx = SplitCriteriaType::Common::findClosestFromEnd(positions, head, secondLast.closestBusStop, tail - 1);
      // ---H----S--T
      //    xxxxxx    takeLeft should have this amount
      //         xxxx takeRight should have this amount
      return SplitCriteria(closestIdx - head + 1, tail - closestIdx + 1);
    }
  };
}
