#pragma once

namespace SplitCriteriaType {
  class Common {
  public:
   /**
     * With params:
     *   [a, b, c, c, c, d, b]
     *   c
     *   4
     * Will iterates starting from index 4 backward, and while queue[i] is still c,
     * it find queue element closest to c
     *
     * Return -1 if not found
     */
    static int findClosestFromEnd(Position positions[], int head, BusStop* pivotBusStop, int endIdx) {
      if (head > endIdx) return -1;

      int closestIdx = -1;
      double closestDistanceSq = -1;

      for (int i = endIdx; i >= head; i--) {
        if (positions[i].closestBusStop != pivotBusStop) {
          break;
        }

        double distanceSq = pivotBusStop->distanceSq(positions[i]);
        if ((closestDistanceSq < 0) || (distanceSq < closestDistanceSq)) {
          closestDistanceSq = distanceSq;
          closestIdx = i;
        }
      }
      return closestIdx;
    }
  };
}
