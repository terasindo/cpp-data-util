#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>

#include "math.hpp"
#include "constant.hpp"
#include "corridor.hpp"
#include "position.hpp"
#include "extractorResult.hpp"
#include "extractor/splitCriteria.hpp"
#include "extractor/splitCriteriaChooser/uTurn.hpp"
#include "extractor/splitCriteriaChooser/secondLastIsTerminal.hpp"
#include "extractor/splitCriteriaChooser/movingOut.hpp"
#include "extractor/splitCriteriaChooser/arrivedOnTerminal.hpp"
#include "extractor/splitCriteriaChooser/timeOut.hpp"

using std::set;
using std::pair;
using std::vector;

class SplitCriteriaChooser {
private:
  SplitCriteriaType::UTurn uTurn;
  SplitCriteriaType::SecondLastIsTerminal secondLastIsTerminal;
  SplitCriteriaType::MovingOut movingOut;
  SplitCriteriaType::ArrivedOnTerminal arrivedOnTerminal;
  SplitCriteriaType::TimeOut timeOut;

  const int INF = 2123123123;
  const int MAX_BUS_STOP_BUFFER = 128;

  int stateExpirationDurationUnix = 30 * 60 * 1000;
  double tooFarRoughThreshold = 0.004;
  double tooFarFineThreshold = 0.003; // Grogol 1 to Jelambar distance
  double soCloseThreshold = 0.0009; // About 7/10 Grogol 1 to Grogol 2
  double soCloseThresholdSq = soCloseThreshold * soCloseThreshold;
  double teleportationMinDistance = (tooFarRoughThreshold * 10);
  double teleportationMinDistanceSq = teleportationMinDistanceSq * teleportationMinDistanceSq;
  double servingThreshold = 5;

public:
  SplitCriteriaChooser(Corridor* _corridor, Position* _positions) :
    timeOut(_corridor, _positions),
    uTurn(_corridor, _positions),
    secondLastIsTerminal(_corridor, _positions),
    movingOut(_corridor, _positions),
    arrivedOnTerminal(_corridor, _positions)
    {
  }
  ~SplitCriteriaChooser() {};

  SplitCriteria getSplitCriteria(int head, int tail) {
    SplitCriteria splitCriteria;

    splitCriteria = timeOut.getSplitCriteria(head, tail);
    if (splitCriteria.isSplit()) {
      if (Constant::DEBUG_SEGMENTIZER) printf("Split by timeout\n");
      return splitCriteria;
    }
    splitCriteria = uTurn.getSplitCriteria(head, tail);
    if (splitCriteria.isSplit()) {
      if (Constant::DEBUG_SEGMENTIZER) printf("Split by u turn\n");
      return splitCriteria;
    }
    splitCriteria = secondLastIsTerminal.getSplitCriteria(head, tail);
    if (splitCriteria.isSplit()) {
      if (Constant::DEBUG_SEGMENTIZER) printf("Split by second last is terminal\n");
      return splitCriteria;
    }
    splitCriteria = movingOut.getSplitCriteria(head, tail);
    if (splitCriteria.isSplit()) {
      if (Constant::DEBUG_SEGMENTIZER) printf("Split by moving out\n");
      return splitCriteria;
    }
    return arrivedOnTerminal.getSplitCriteria(head, tail);
  }
};
