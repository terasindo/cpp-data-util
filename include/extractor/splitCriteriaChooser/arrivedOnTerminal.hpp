#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>

#include "math.hpp"
#include "constant.hpp"
#include "descriptor.hpp"
#include "graph.hpp"
#include "positionLogBundle.hpp"
#include "stateBundle.hpp"
#include "segment.hpp"
#include "constant.hpp"
#include "extractor/splitCriteria.hpp"
#include "extractor/splitCriteriaChooser/_common.hpp"

using std::set;
using std::pair;
using std::vector;

namespace SplitCriteriaType {
  class ArrivedOnTerminal {
  private:
    Corridor* corridor;
    Position* positions;

    int minDurationToBeStayingMinute = Constant::DEFAULT_MIN_DURATION_TO_BE_STAYING_MINUTE;

  public:
    ArrivedOnTerminal(Corridor* _corridor, Position* _positions) : corridor(_corridor), positions(_positions) {
    };
    ~ArrivedOnTerminal() {};

    void setMinDurationToBeStayingMinute(int value) {
      minDurationToBeStayingMinute = value;
    }

    SplitCriteria getSplitCriteria(int head, int tail) {
      if (tail - head + 1 <= 2) {
        return SplitCriteria();
      }

      Position first = positions[head];
      Position last = positions[tail];

      bool isTerminalStart = (last.closestBusStop == corridor->startBusStop);
      bool isTerminalEnd = (last.closestBusStop == corridor->endBusStop);
      bool isTerminal = isTerminalStart || isTerminalEnd;
      bool isMoving = (last.closestBusStop != first.closestBusStop);
      if (!isMoving || !isTerminal) {
        return SplitCriteria();
      }

      int pastIdx = tail - minDurationToBeStayingMinute;
      if ((pastIdx < head) || (last.closestBusStop != positions[pastIdx].closestBusStop)) {
        return SplitCriteria();
      }

      int closestIdx = SplitCriteriaType::Common::findClosestFromEnd(positions, head, last.closestBusStop, tail);
      // ---H----S--T
      //    xxxxxx    takeLeft should have this amount
      //         xxxx takeRight should have this amount
      return SplitCriteria(closestIdx - head + 1, tail - closestIdx + 1);
    }
  };
}
