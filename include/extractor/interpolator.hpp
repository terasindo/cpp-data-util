#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>

#include "math.hpp"
#include "constant.hpp"
#include "descriptor.hpp"
#include "graph.hpp"
#include "positionLogBundle.hpp"
#include "stateBundle.hpp"
#include "segment.hpp"
#include "arrival.hpp"
#include "extractor/splitCriteria.hpp"

using std::set;
using std::pair;
using std::vector;

class Interpolator {
private:
  /**
   * Draw a graph, where:
   *  x: time
   *  y: distance covered since start of bus stop
   *
   * Given list of bus stops in route, we can find its distances from the start bus stop.
   * Call that projectionY.
   *
   * Now, find the x coordinates for each point i whose y coordinate is projectionY[i].
   * Call that x[i], and it is the time where a bus reached bus stop i.
   */
  vector<Arrival> segmentToArrivals(Graph* graph, Segment &segment) {
    vector<Arrival> result;
    if (segment.positions.size() == 0) {
      return result;
    }

    BusStop* startBusStop = segment.positions[0].closestBusStop;
    BusStop* endBusStop = segment.positions.back().closestBusStop;

    vector<BusStop*> uniqueBusStopVisitChain;
    for (int i = 0; i < segment.positions.size(); i++) {
      if ((i == 0) || (segment.positions[i].closestBusStop != segment.positions[i-1].closestBusStop)) {
        uniqueBusStopVisitChain.push_back(segment.positions[i].closestBusStop);
      }
    }

    pair<BusStop*, BusStop*> corridorOrientation = graph->getCorridorOrientation(segment.corridor, uniqueBusStopVisitChain);
    BusStop* corridorStartBusStop = corridorOrientation.first;
    BusStop* corridorEndBusStop = corridorOrientation.second;

    if ((corridorStartBusStop == NULL) || (corridorEndBusStop == NULL)) {
      return result;
    }

    // Still return all points in polyline
    vector<Point> basePolyline = graph->getOrientedPolyline(segment.corridor, corridorStartBusStop);

    // Get the polyline
    vector<Point> segmentPolyline;
    segmentPolyline.reserve(segment.positions.size());
    for (int i = 0; i < segment.positions.size(); i++) {
      segmentPolyline.push_back((Point)segment.positions[i]);
    }

    // Convert to line graph
    vector<double> lineY = Math::polylinePolylineProjection(segmentPolyline, basePolyline);
    vector<Point> lineGraph;
    for (int i = 0; i < segment.positions.size(); i++) {
      lineGraph.push_back(Point(segment.positions[i].unixTime, lineY[i]));
    }
    lineGraph = trim(lineGraph, Constant::DEFAULT_SO_CLOSE_THRESHOLD);

    vector<BusStop*> busStopChain = graph->getBusStopChain(startBusStop, endBusStop, corridorStartBusStop, segment.corridor);

    // Get the polyline
    vector<Point> busStopChainPolyline;
    busStopChainPolyline.reserve(busStopChain.size());
    for (int i = 0; i < busStopChain.size(); i++) {
      busStopChainPolyline.push_back((Point)*busStopChain[i]);
    }
    vector<double> projectionY = Math::polylinePolylineProjection(busStopChainPolyline, basePolyline);
    vector<double> projectionX = findXGivenYInLineGraph(lineGraph, projectionY, Constant::DEFAULT_SO_CLOSE_THRESHOLD);

    // Snap endBusStop to next nearest common bus stop
    BusStop* destinationBusStop = endBusStop;
    while ((destinationBusStop != NULL) && !destinationBusStop->isCommon) {
      if (destinationBusStop == corridorEndBusStop) {
        // This is unlikely happening... but defensive is always okay
        break;
      }
      destinationBusStop = destinationBusStop->getNext(segment.corridor, corridorEndBusStop);
    }

    // Finally, the arrival!
    for (int i = 0; i < projectionX.size(); i++) {
      // X is rounded because it is unixTime
      int unixTime = round(projectionX[i]);

      if (unixTime > 0) {
        result.push_back(Arrival(segment.corridor, busStopChain[i], destinationBusStop, unixTime, segment.busId));
      }
    }

    return result;
  }

public:
  Interpolator() {};
  ~Interpolator() {};

  vector<Arrival> interpolateToArrivals(Graph *graph, vector<Segment> &segments) {
    vector<Arrival> result;

    int x = 0;
    for (Segment segment : segments) {
      vector<Arrival> arrivals = segmentToArrivals(graph, segment);
      if (Constant::DEBUG_ARRIVAL) printf("SEGMENT %d GOT %d ARRIVALS\n", ++x, (int)arrivals.size());
      for (Arrival arrival : arrivals) {
        result.push_back(arrival);
      }
    }

    return result;
  }


  vector<Point> trim(vector<Point> &polyline, double tolerance) {
    if (polyline.size() <= 1) {
      return polyline;
    }

    double headValue = polyline[0].y;
    int p1 = 0;
    while ((p1+1 < polyline.size()) && Math::eq(headValue, polyline[p1+1].y, tolerance)) {
      p1++;
    }

    double tailValue = polyline.back().y;
    int p2 = polyline.size() - 1;
    while ((p2-1 >= 0) && Math::eq(tailValue, polyline[p2-1].y, tolerance)) {
      p2--;
    }

    // Slice polyline[p1..p2]
    vector<Point> result;
    for (int i = p1; i <= p2; i++) {
      result.push_back(polyline[i]);
    }
    return result;
  }

  vector<double> findXGivenYInLineGraph(vector<Point> &polyline, vector<double> &yValues, double tolerance) {
    vector<double> xValues;
    xValues.reserve(yValues.size());

    if (polyline.size() == 0) {
      return xValues;
    }

    double lastX = polyline[0].x;
    int j = 0;
    for (int i = 0; i < yValues.size(); i++) {
      double y = yValues[i];

      while ((j+1 < polyline.size()) && (y - tolerance > polyline[j+1].y)) {
        j++;
        lastX = polyline[j].x;
      }

      if (y < polyline[j].y) {
        // Not even enough, just pick the first
        xValues.push_back(polyline[0].x);
      } else if (j+1 < polyline.size()) {
        // Interpolate
        double dy = polyline[j+1].y - polyline[j].y;
        double dx = polyline[j+1].x - polyline[j].x;
        double sy = y - polyline[j].y;

        if (fabs(dy) < 1e-5) {
          xValues[i] = lastX;
        } else {
          double grad = Math::clamp(0, 1, sy/dy);
          xValues.push_back(lastX + (dx * grad));
        }
      } else {
        // Running out of line, just pick the last
        xValues.push_back(polyline[j].x);
      }
    }

    return xValues;
  }
};
