#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>

#include "math.hpp"
#include "constant.hpp"
#include "descriptor.hpp"
#include "graph.hpp"
#include "positionLogBundle.hpp"
#include "stateBundle.hpp"
#include "segment.hpp"
#include "arrival.hpp"

using std::set;
using std::sort;
using std::queue;
using std::pair;
using std::vector;

class SegmentCorrector {
private:
  class SummarizedSegment {
  public:
    int startTime;
    int endTime;
    int idx;
  };

  class LcsPacket {
  public:
    vector<int> positionIdxs;
    BusStop* closestBusStop;
  };

  vector<LcsPacket> getLcs(const vector<BusStop*> &busStops, const vector<LcsPacket> &data) {
    int sz1 = busStops.size();
    int sz2 = data.size();
    int dp[sz1+1][sz2+1];
    int pred[sz1+1][sz2+1]; // 0:[0][-1], 1:[-1][0], 2:[-1][-1]

    // Base
    for (int i = 0; i <= sz1; i++) {
      dp[i][0] = 0;
    }
    for (int j = 0; j <= sz2; j++) {
      dp[0][j] = 0;
    }

    // Reccurence
    for (int i = 1; i <= sz1; i++) {
      for (int j = 1; j <= sz2; j++) {
        int maxVal = -1;

        // Discard data
        if (dp[i][j-1] > maxVal) {
          maxVal = dp[i][j-1];
          pred[i][j] = 0;
        }

        // Discard busStop
        if (dp[i-1][j] > maxVal) {
          maxVal = dp[i-1][j];
          pred[i][j] = 1;
        }

        // Match both?
        if (busStops[i-1] == data[j-1].closestBusStop) {
          if (dp[i-1][j-1] + 1 > maxVal) {
            maxVal = dp[i-1][j-1] + 1;
            pred[i][j] = 2;
          }
        }

        dp[i][j] = maxVal;
      }
    }

    // Backtrack
    vector<LcsPacket> result;
    int pi = sz1;
    int pj = sz2;
    while ((pi > 0) && (pj > 0)) {
      if (pred[pi][pj] == 2) {
        result.push_back(data[pj-1]);
        pi--;
        pj--;
      } else if (pred[pi][pj] == 1) {
        pi--;
      } else if (pred[pi][pj] == 0) {
        pj--;
      }
    }
    reverse(result.begin(), result.end());
    return result;
  }

public:
  SegmentCorrector() {};
  ~SegmentCorrector() {};

  void fix(Graph *graph, vector<Segment> &segments) {
    // Sorry, everything is by reference (2x faster)
    externalCorridorFix(graph, segments);

    internalCorridorFix(graph, segments);
  }

  void externalCorridorFix(Graph *graph, vector<Segment> &segments) {
    sort(segments.begin(), segments.end(), [](const Segment &a, const Segment &b) {
      return a.busId < b.busId;
    });

    // Attach context
    int lastPos = 0;
    for (int i = 1; i <= segments.size(); i++) { // Sorry, using lazy <=
      if ((i == segments.size()) || (segments[i].busId != segments[i-1].busId)) {
        vector<Corridor*> parentCorridors = getParentCorridors(segments, lastPos, i-1);
        for (int i = 0; i < parentCorridors.size(); i++) {
          treat(graph, segments[i], parentCorridors[i]);
        }
      }
    }
  }

  vector<Corridor*> getParentCorridors(const vector<Segment> &segments, int head, int tail) {
    vector<Corridor*> parentCorridors;
    parentCorridors.reserve(tail - head + 1);

    vector<SummarizedSegment> summarizedSegments;
    summarizedSegments.reserve(tail - head + 1);
    for (int i = head; i <= tail; i++) {
      SummarizedSegment summarizedSegment;

      summarizedSegment.idx = i;
      summarizedSegment.startTime = segments[i].getStartTime();
      summarizedSegment.endTime = segments[i].getEndTime();

      summarizedSegments.push_back(summarizedSegment);
    }

    sort(summarizedSegments.begin(), summarizedSegments.end(), [](const SummarizedSegment &a, const SummarizedSegment &b) {
      if (a.startTime != b.startTime) {
        return a.startTime < b.startTime;
      }
      return a.endTime > b.endTime;
    });

    queue<SummarizedSegment> q;
    for (int i = 0; i < summarizedSegments.size(); i++) {
      int start = summarizedSegments[i].startTime;
      int end = summarizedSegments[i].endTime;

      while (!q.empty() && (q.front().endTime <= start)) {
        q.pop();
      }

      q.push(summarizedSegments[i]);

      parentCorridors.push_back(segments[q.front().idx].corridor);
    }

    return parentCorridors;
  }

  void treat(Graph *graph, Segment &segment, Corridor* parentCorridor) {
    Corridor* corridor = segment.corridor;

    string corridorName = corridor->name;
    string parentCorridorName = parentCorridor->name;

    if ((corridorName == "2B") || (corridorName == "3") || (corridorName == "3A")) {
      if (parentCorridorName == "8") {
        removeBusStopByJoin(segment, graph->getBusStopsInCorridorSet(parentCorridor));
      }
    } else if ((corridorName == "5") || (corridorName == "7B")) {
      if (parentCorridorName == "7A") {
        removeBusStopByJoin(segment, graph->getBusStopsInCorridorSet(parentCorridor));
      }
    } else if (corridorName == "6") {
      if (parentCorridorName == "6A") {
        removeBusStopByJoin(segment, graph->getBusStopsInCorridorSet(parentCorridor));
      }
    } else if (corridorName == "12") {
      if (parentCorridorName == "10") {
        removeBusStopByJoin(segment, graph->getBusStopsInCorridorSet(parentCorridor));
      }
    }
  }

  void removeBusStopByJoin(Segment &segment, const set<BusStop*> &realBusStops) {
    Segment result = segment;

    result.positions.clear();
    for (int i = 0; i < segment.positions.size(); i++) {
      if (realBusStops.count(segment.positions[i].closestBusStop) > 0) {
        result.positions.push_back(segment.positions[i]);
      }
    }

    segment = result;
  }

  void internalCorridorFix(Graph *graph, vector<Segment> &segments) {
    // Corridor based fixing
    for (int i = 0; i < segments.size(); i++) {
      cleanInvalidPositions(graph, segments[i]);
    }
  }

  void cleanInvalidPositions(Graph *graph, Segment &segment) {
    // Create LCS packet
    vector<LcsPacket> lcsPackets;
    for (int i = 0; i < segment.positions.size(); i++) {
      if ((lcsPackets.size() > 0) && (lcsPackets.back().closestBusStop == segment.positions[i].closestBusStop)) {
        lcsPackets.back().positionIdxs.push_back(i);
      } else {
        LcsPacket packet;
        packet.positionIdxs.push_back(i);
        packet.closestBusStop = segment.positions[i].closestBusStop;
        lcsPackets.push_back(packet);
      }
    }

    BusStop* corridorStart = segment.corridor->startBusStop;
    BusStop* corridorEnd = segment.corridor->endBusStop;

    vector<LcsPacket> maxLcs;

    for (int i = 0; i < 2; i++) {
      vector<BusStop*> busStops = graph->getOrientedBusStopsInCorridor(segment.corridor, corridorStart);
      vector<LcsPacket> lcs = getLcs(busStops, lcsPackets);

      if (maxLcs.size() < lcs.size()) {
        maxLcs = lcs;
      }

      std::swap(corridorStart, corridorEnd);
    }

    // Inflate data
    vector<Position> cleanedPositions;
    for (int i = 0; i < maxLcs.size(); i++) {
      for (int j = 0; j < maxLcs[i].positionIdxs.size(); j++) {
        int idx = maxLcs[i].positionIdxs[j];
        cleanedPositions.push_back(segment.positions[idx]);
      }
    }
    segment.positions = cleanedPositions;
  }
};
