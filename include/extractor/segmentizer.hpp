#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>

#include "math.hpp"
#include "constant.hpp"
#include "descriptor.hpp"
#include "graph.hpp"
#include "positionLogBundle.hpp"
#include "stateBundle.hpp"
#include "segment.hpp"
#include "extractorResult.hpp"
#include "extractor/splitCriteria.hpp"
#include "extractor/splitCriteriaChooser/index.hpp"

using std::set;
using std::pair;
using std::vector;

class Segmentizer {
private:
  const int INF = 2123123123;
  const int MAX_BUS_STOP_BUFFER = 128;

  double tooFarRoughThreshold = Constant::DEFAULT_TOO_FAR_ROUGH_THRESHOLD;
  double tooFarFineThreshold = Constant::DEFAULT_TOO_FAR_FINE_THRESHOLD;
  int servingThreshold = Constant::DEFAULT_SERVING_THRESHOLD;

public:
  Segmentizer() {}
  ~Segmentizer() {}

  pair<vector<Segment>, StateBundle> segmentize(Descriptor *descriptor, Graph *graph, PositionLogBundle positionLogBundle, StateBundle stateBundle) {
    vector<string> busIds = positionLogBundle.getBusIds();
    sort(busIds.begin(), busIds.end());

    // Take into account busId white list
    set<string> busIdWhiteList;
    for (string s : descriptor->selectedBusIds) {
      busIdWhiteList.insert(s);
    }

    // Take into account corridor white list
    set<string> corridorWhiteList;
    for (string s : descriptor->selectedCorridors) {
      corridorWhiteList.insert(s);
    }

    vector<Segment> segments;
    StateBundle finalState;

    for (int i = 0; i < busIds.size(); i++) {
      string busId = busIds[i];

      if ((busIdWhiteList.size() > 0) && (busIdWhiteList.count(busId) == 0)) {
        continue;
      }

      for (int j = 0; j < graph->corridors.size(); j++) {
        Corridor* corridor = graph->corridors[j];
        if ((corridorWhiteList.size() > 0) && (corridorWhiteList.count(corridor->name) == 0)) {
          continue;
        }

        pair<vector<Segment>, vector<Position>> result = segmentizeOne(
            descriptor,
            graph,
            busId,
            positionLogBundle.getPositionLog(busId),
            stateBundle.getCorridorState(busId, corridor),
            corridor
        );

        for (Segment segment : result.first) {
          segments.push_back(segment);
        }
        finalState.addCorridorStateByCloning(busId, corridor, result.second);
      }
      // fprintf(stderr, "[%3.2lf%%] (%d/%d) Segmentizing %s\n", 100*(i+1.)/busIds.size(), (i+1), (int)busIds.size(), busId.c_str());
    }

    return make_pair(segments, finalState);
  }

  pair<vector<Segment>, vector<Position>> segmentizeOne(
        Descriptor *descriptor,
        Graph *graph,
        string busId,
        const vector<Position> &positionLog,
        const vector<Position> &initialState,
        Corridor *corridor
      ) {

    // For result
    vector<Segment> segments;
    vector<Position> finalState;

    // Initial data
    vector<BusStop*> inCorridorBusStops = graph->getBusStopsInCorridor(corridor);
    vector<Point> polyline = graph->getCorridorPolyline(corridor);

    // Think of this as fake queue
    Position positions[Constant::MAX_POSITION_SIZE];
    int pSize = 0; // Size of positions

    // Pass in state (could be empty)
    for (int i = 0; i < initialState.size(); i++) {
      positions[pSize++] = initialState[i];
    }
    // Continue with today's positionLog
    for (int i = 0; i < positionLog.size(); i++) {
      positions[pSize++] = positionLog[i];
    }

    // The "sliding" process starts here
    // ----------------------------------------

    // Get last position
    Position lastPosition;
    if (initialState.size() > 0) {
      lastPosition = initialState.back();
    } else {
      lastPosition = positions[0];
    }

    // Initialize split criteria chooser
    SplitCriteriaChooser splitCriteriaChooser(corridor, positions);

    // Head and tail defines "active set" in the sweep
    int head = 0;
    for (int tail = 0; tail < pSize; tail++) {
      int unixTime = positions[tail].unixTime;

      // Find closest bus stop, put to positions[tail].closestBusStop
      double closestDistanceSq = INF;
      for (BusStop* busStop : inCorridorBusStops) {
        double distanceSq = positions[tail].distanceSq(*busStop);
        if (distanceSq < closestDistanceSq) {
          positions[tail].closestBusStop = busStop;
          closestDistanceSq = distanceSq;
        }
      }

      if (Constant::DEBUG_SEGMENTIZER) printf("%s -> %s\n", positions[head].getReadableTime(7).c_str(), positions[tail].getReadableTime(7).c_str());
      SplitCriteria splitCriteria = splitCriteriaChooser.getSplitCriteria(head, tail);
      if (splitCriteria.isSplit()) {
        if (Constant::DEBUG_SEGMENTIZER) printf("SPLITTING %d %d\n", splitCriteria.getTakeLeft(), splitCriteria.getTakeRight());
        int segmentStartIdx = head;
        int segmentEndIdx = head + splitCriteria.getTakeLeft() - 1;

        // Update head
        head = tail - splitCriteria.getTakeRight() + 1;

        // Now positions[segmentStartIdx..segmentEndIdx] is a valid segment
        set<BusStop*> uniqBusStop;
        for (int i = segmentStartIdx; i <= segmentEndIdx; i++) {
          uniqBusStop.insert(positions[i].closestBusStop);
        }
        if (Constant::DEBUG_SEGMENTIZER) fprintf(stdout, "UNIQ %d\n", (int)uniqBusStop.size());
        if (uniqBusStop.size() > servingThreshold) {
          Segment segment;

          segment.busId = busId;
          segment.corridor = corridor;
          segment.positions.clear();
          for (int i = segmentStartIdx; i <= segmentEndIdx; i++) {
            // Take only if close enough
            if (Math::isPointToPolylineDistanceLessThan(positions[i], polyline, tooFarFineThreshold)) {
              segment.positions.push_back(positions[i]);
            }
          }

          if (segment.positions.size() > 0) {
            segments.push_back(segment);
          }
        }
      }

      // Update the last position
      lastPosition = positions[tail];
    }
    if (Constant::DEBUG_SEGMENTIZER) fprintf(stdout, "WOW %d\n", (int) segments.size());

    // Gather the remaining unharvested data for the next day (store the state)
    for (int i = head; i < pSize; i++) {
      finalState.push_back(positions[i]);
    }

    return make_pair(segments, finalState);
  }
};
