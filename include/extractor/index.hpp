#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>

#include "math.hpp"
#include "constant.hpp"
#include "descriptor.hpp"
#include "graph.hpp"
#include "positionLogBundle.hpp"
#include "stateBundle.hpp"
#include "segment.hpp"
#include "extractorResult.hpp"
#include "extractor/segmentizer.hpp"
#include "extractor/segmentCorrector.hpp"
#include "extractor/interpolator.hpp"
#include "extractor/travelExtractor.hpp"

using std::set;
using std::pair;
using std::vector;

class Extractor {
public:
  Extractor() {}
  ~Extractor() {};

  ExtractorResult extract(Descriptor *descriptor, Graph *graph, PositionLogBundle positionLogBundle, StateBundle stateBundle) {
    ExtractorResult result;

    Segmentizer segmentizer;
    pair<vector<Segment>, StateBundle> segmentationResult = segmentizer.segmentize(
        descriptor,
        graph,
        positionLogBundle,
        stateBundle
    );

    if (Constant::DEBUG_SEGMENTIZER)  fprintf(stdout, "GOT %d SEGMENTS\n", (int)segmentationResult.first.size());
    for (Segment segment : segmentationResult.first) {
      if (Constant::DEBUG_SEGMENTIZER) printf("%d -> %d\n", segment.positions[0].unixTime, segment.positions.back().unixTime);
      if (Constant::DEBUG_SEGMENTIZER) printf("%s -> %s\n",
          segment.positions[0].getReadableTime(Constant::LOCAL_TIME_UTC_OFFSET_HOUR).c_str(),
          segment.positions.back().getReadableTime(Constant::LOCAL_TIME_UTC_OFFSET_HOUR).c_str()
      );
    }
    result.finalState = segmentationResult.second;

    SegmentCorrector segmentCorrector;
    segmentCorrector.fix(graph, segmentationResult.first);

    Interpolator interpolator;
    result.arrivals = interpolator.interpolateToArrivals(graph, segmentationResult.first);

    TravelExtractor travelExtractor;
    result.travels = travelExtractor.extractTravels(graph, result.arrivals);

    if (Constant::DEBUG_ARRIVAL) printf("GOT %d ARRIVALS\n", (int)result.arrivals.size());
    for (Arrival a : result.arrivals) {
      if (Constant::DEBUG_ARRIVAL) printf("%s -> %s : %s\n", a.getReadableTime(7).c_str(), a.busStop->name.c_str(), a.destinationBusStop->name.c_str());
    }

    if (Constant::DEBUG_TRAVEL) printf("GOT %d TRAVELS\n", (int)result.travels.size());
    for (Travel t : result.travels) {
      if (Constant::DEBUG_TRAVEL) printf("%s) [%2d] %s -> %s\n", t.getReadableStartTime(7).c_str(), t.durationSec/60, t.fromBusStop->name.c_str(), t.toBusStop->name.c_str());
    }

    return result;
  }
};
