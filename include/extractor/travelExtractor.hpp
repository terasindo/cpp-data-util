#pragma once

#include <algorithm>
#include <vector>
#include <queue>
#include <set>

#include "math.hpp"
#include "constant.hpp"
#include "descriptor.hpp"
#include "graph.hpp"
#include "positionLogBundle.hpp"
#include "stateBundle.hpp"
#include "travel.hpp"
#include "arrival.hpp"

using std::set;
using std::pair;
using std::make_pair;
using std::vector;

class TravelExtractor {
private:
  vector<Travel> extractTravelFromOneBus(const set<pair<BusStop*, BusStop*>> &edgeSet, vector<Arrival> &arrivals, int startIdx, int endIdx) {
    vector<Travel> result;
    string busId = arrivals[startIdx].busId;

    int prevTimeUnix;
    BusStop* prevBusStop = arrivals[0].busStop;
    for (int i = startIdx; i < endIdx; i++) {
      BusStop* nowBusStop = arrivals[i].busStop;

      // Overwrite for case c on "a -> b -> c -> c -> b -> a"
      if (nowBusStop == prevBusStop) {
        prevTimeUnix = arrivals[i].unixTime;
        continue;
      }

      if (edgeSet.count(make_pair(prevBusStop, nowBusStop)) > 0) {
        int deltaSecond = arrivals[i].unixTime - prevTimeUnix;
        int startUnixTime = prevTimeUnix;
        result.push_back(Travel(busId, prevBusStop, nowBusStop, deltaSecond, startUnixTime));
      }

      prevBusStop = nowBusStop;
      prevTimeUnix = arrivals[i].unixTime;
    }

    return result;
  }

  set<pair<BusStop*, BusStop*>> getEdgeSet(Graph *graph) {
    set<pair<BusStop*, BusStop*>> result;
    const vector<pair<BusStop*, BusStop*>> &edges = graph->getEdges();

    for (pair<BusStop*, BusStop*> edge : edges) {
      result.insert(edge);
    }
    return result;
  }

public:
  TravelExtractor() {};
  ~TravelExtractor() {};

  vector<Travel> extractTravels(Graph *graph, vector<Arrival> &arrivals) {
    vector<Travel> travels;

    // Get edge set
    const set<pair<BusStop*, BusStop*>> &edgeSet = getEdgeSet(graph);

    // Clone it to avoid mutation
    vector<Arrival> arrivalsCloned = arrivals;

    // Sort to group busIds + ascending arrival time
    std::sort(arrivalsCloned.begin(), arrivalsCloned.end(), [](const Arrival& a, const Arrival& b) {
      if (a.busId != b.busId) {
        return a.busId < b.busId;
      } else {
        return a.unixTime < b.unixTime;
      }
    });

    int head = 0;
    // SORRY I USED 1+ HACK HERE
    for (int tail = 1; tail < 1+arrivalsCloned.size(); tail++) {
      if ((tail >= arrivalsCloned.size()) || (arrivalsCloned[tail-1].busId != arrivalsCloned[tail].busId)) {
        const vector<Travel> &extractedTravels = extractTravelFromOneBus(edgeSet, arrivalsCloned, head, tail-1);

        // Accumulate
        for (Travel t : extractedTravels) {
          travels.push_back(t);
        }

        head = tail;
      }
    }

    return travels;
  }
};
