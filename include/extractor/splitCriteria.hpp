#pragma once

/**
 * A split criteria is an object with key `takeLeft` and `takeRight`.
 * If undefined, means that current queue (state[busId][route.name]) doesn't need to be splitted.
 * If it is the object, that means we need to split it by taking `takeLeft` element as the segment,
 * and `takeRight` element as the remaining element in queue.
 */
class SplitCriteria {
private:
  int takeLeft;
  int takeRight;
  bool splitted;
public:
  SplitCriteria() : takeLeft(-1), takeRight(-1), splitted(false) {};
  SplitCriteria(int _takeLeft, int _takeRight) : takeLeft(_takeLeft), takeRight(_takeRight), splitted(true) {};

  int getTakeLeft() const {
    return takeLeft;
  }
  int getTakeRight() const {
    return takeRight;
  }
  bool isSplit() const {
    return splitted;
  }

  bool operator==(const SplitCriteria &o) const {
    return (takeLeft == o.getTakeLeft()) &&
        (takeRight == o.getTakeRight()) &&
        (splitted == o.isSplit());
  }
};