/*
  The main entry point for aggregating arrivals and travels to ready to use data for web API.
 */

#include <boost/program_options.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <string>
#include <iostream>
#include <thread>
#include <future>
#include <cstdio>
#include <map>
#include <csignal>
#include <cstdlib>
#include <execinfo.h>
#include <unistd.h>

#include "graph.hpp"
#include "arrival.hpp"
#include "travel.hpp"
#include "waitingTime.hpp"
#include "aggregator/waitingTimeGenerator.hpp"
#include "aggregator/travelTimeGenerator.hpp"

using std::string;
using std::cout;
using std::endl;
using std::min;
using std::max;
using std::map;
using std::async;
using std::future;
namespace po = boost::program_options;
namespace gregorian = boost::gregorian;

const string DESCRIPTOR_FILENAME = "descriptor.txt";
const string DEFAULT_ARRIVAL_FOLDER = "../../data/arrival/";
const string DEFAULT_TRAVEL_FOLDER = "../../data/travel/";
const string DEFAULT_AGGREGATED_FOLDER = "../../data/aggregated/";
const string DEFAULT_GRAPH_FILE = "../../data/graph.in";

// Handler for signal 11 error (segmentation fault), to print stack trace
void segmentationFaultHandler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

void writeWaitingTimes(string filePath, const vector<WaitingTime> &waitingTimes, Graph* graph) {
  FILE *file;
  file = fopen(filePath.c_str(), "w+");

  map<BusStop*,int> busStopToIdx;
  map<Corridor*,int> corridorToIdx;

  // Write metadata
  fprintf(file, "%d\n", (int)graph->busStops.size());
  for (int i = 0; i < graph->busStops.size(); i++) {
    busStopToIdx[graph->busStops[i]] = i;
    fprintf(file, "%s\n", graph->busStops[i]->name.c_str());
  }
  fprintf(file, "%d\n", (int)graph->corridors.size());
  for (int i = 0; i < graph->corridors.size(); i++) {
    corridorToIdx[graph->corridors[i]] = i;
    fprintf(file, "%s\n", graph->corridors[i]->name.c_str());
  }
  fprintf(file, "%d\n", (int)Constant::AGGREGATE_ARRIVAL_WINDOW_DAY.size());
  for (int deltaDayIdx = 0; deltaDayIdx < Constant::AGGREGATE_ARRIVAL_WINDOW_DAY.size(); deltaDayIdx++) {
    int deltaDay = Constant::AGGREGATE_ARRIVAL_WINDOW_DAY[deltaDayIdx];
    fprintf(file, "%d\n", deltaDay);
  }

  fprintf(file, "%d\n", Constant::AGGREGATE_GRANULARITY_MINUTE);

  // Write actual data
  fprintf(file, "%d\n", (int)waitingTimes.size());
  for (WaitingTime wt : waitingTimes) {
    fprintf(
        file,
        "%d %d %d %d\n",
        corridorToIdx[wt.corridor],
        busStopToIdx[wt.busStop],
        busStopToIdx[wt.destinationBusStop],
        wt.minuteOfDay
    );

    for (int deltaDayIdx = 0; deltaDayIdx < Constant::AGGREGATE_ARRIVAL_WINDOW_DAY.size(); deltaDayIdx++) {
      int deltaDay = Constant::AGGREGATE_ARRIVAL_WINDOW_DAY[deltaDayIdx];
      fprintf(
          file,
          "%d %d %d %d %d %d\n",
          wt.average[deltaDay],
          wt.median[deltaDay],
          wt.p75[deltaDay],
          wt.p80[deltaDay],
          wt.p85[deltaDay],
          wt.p90[deltaDay]
      );
    }
  }

  fclose(file);
}

void writeTravelTimes(string filePath, const vector<TravelTime> &travelTimes, Graph* graph) {
  FILE *file;
  file = fopen(filePath.c_str(), "w+");

  map<BusStop*,int> busStopToIdx;

  // Write metadata
  fprintf(file, "%d\n", (int)graph->busStops.size());
  for (int i = 0; i < graph->busStops.size(); i++) {
    busStopToIdx[graph->busStops[i]] = i;
    fprintf(file, "%s\n", graph->busStops[i]->name.c_str());
  }
  fprintf(file, "%d\n", (int)Constant::AGGREGATE_TRAVEL_WINDOW_DAY.size());
  for (int deltaDayIdx = 0; deltaDayIdx < Constant::AGGREGATE_TRAVEL_WINDOW_DAY.size(); deltaDayIdx++) {
    int deltaDay = Constant::AGGREGATE_TRAVEL_WINDOW_DAY[deltaDayIdx];
    fprintf(file, "%d\n", deltaDay);
  }

  fprintf(file, "%d\n", Constant::AGGREGATE_GRANULARITY_MINUTE);

  // Write actual data
  fprintf(file, "%d\n", (int)travelTimes.size());
  for (TravelTime tt : travelTimes) {
    fprintf(
        file,
        "%d %d %d\n",
        busStopToIdx[tt.fromBusStop],
        busStopToIdx[tt.toBusStop],
        tt.minuteOfDay
    );

    for (int deltaDayIdx = 0; deltaDayIdx < Constant::AGGREGATE_TRAVEL_WINDOW_DAY.size(); deltaDayIdx++) {
      int deltaDay = Constant::AGGREGATE_TRAVEL_WINDOW_DAY[deltaDayIdx];
      fprintf(
          file,
          "%d %d %d %d %d %d\n",
          tt.average[deltaDay],
          tt.median[deltaDay],
          tt.p75[deltaDay],
          tt.p80[deltaDay],
          tt.p85[deltaDay],
          tt.p90[deltaDay]
      );
    }
  }

  fclose(file);
}

int main(int argc, char *argv[]) {
  signal(SIGSEGV, segmentationFaultHandler);

  po::options_description desc("Allowed options");

  string argGraph;
  string argArrival;
  string argTravel;
  string argAggregated;

  desc.add_options()
    ("help,h", "Produce help message")
    ("date,d", po::value<string>(), "A date for aggregation in YYYY-MM-DD")
    ("wt,w", "Generate waiting time")
    ("tt,t", "Generate travel time")

    // Syntactic salted, no shorthand for these options
    ("graph", po::value<string>(&argGraph)->default_value(DEFAULT_GRAPH_FILE), "Graph file")
    ("arrival", po::value<string>(&argArrival)->default_value(DEFAULT_ARRIVAL_FOLDER), "Arrival input folder")
    ("travel", po::value<string>(&argTravel)->default_value(DEFAULT_TRAVEL_FOLDER), "Travel input folder")
    ("aggregated", po::value<string>(&argAggregated)->default_value(DEFAULT_AGGREGATED_FOLDER), "Aggregated data output folder")
  ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help") || (vm.count("date") == 0)) {
    cout << desc << endl;
    return 0;
  }

  // Ensure dirs
  system(("mkdir -p " + argArrival).c_str());
  system(("mkdir -p " + argTravel).c_str());
  system(("mkdir -p " + argAggregated).c_str());

  // Get the desired aggregation date
  gregorian::date aggregationDate;
  if (vm.count("date")) {
    aggregationDate = gregorian::from_simple_string(vm["date"].as<string>());
  } else {
    // Default to yesterday
    aggregationDate = gregorian::day_clock::local_day() - gregorian::date_duration(1);
  }

  // Load graph
  Graph *graph = new Graph(argGraph);

  // Get some constants
  int arrivalMaxDayBefore = 0;
  for (int x : Constant::AGGREGATE_ARRIVAL_WINDOW_DAY) {
    arrivalMaxDayBefore = max(arrivalMaxDayBefore, x);
  }
  int travelMaxDayBefore = 0;
  for (int x : Constant::AGGREGATE_TRAVEL_WINDOW_DAY) {
    travelMaxDayBefore = max(travelMaxDayBefore, x);
  }


  // Aggregate those shites

  if (vm.count("wt")) {
    WaitingTimeGenerator waitingTimeGenerator(graph, aggregationDate, arrivalMaxDayBefore, argArrival);
    vector<WaitingTime> waitingTimes = waitingTimeGenerator.generateWaitingTime();

    string waitingTimePath = argAggregated + gregorian::to_iso_extended_string(aggregationDate) + ".wt";
    writeWaitingTimes(waitingTimePath, waitingTimes, graph);
  }

  if (vm.count("tt")) {
    TravelTimeGenerator travelTimeGenerator(graph, aggregationDate, travelMaxDayBefore, argTravel);
    vector<TravelTime> travelTimes = travelTimeGenerator.generateTravelTime();

    string travelTimePath = argAggregated + gregorian::to_iso_extended_string(aggregationDate) + ".tt";
    writeTravelTimes(travelTimePath, travelTimes, graph);
  }

  delete graph;
}
