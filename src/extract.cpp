/*
  The main entry point for extracting position logs to arrivals and travels.

  To avoid memory leak shits, all other object are instantiated without new statement but:
  - busStop
  - corridor
  - graph
  - descriptor

  They are destructed at the end of execution.
 */

#include <boost/program_options.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <string>
#include <iostream>
#include <thread>
#include <future>
#include <cstdio>
#include <map>
#include <csignal>
#include <cstdlib>
#include <execinfo.h>
#include <unistd.h>

#include "descriptor.hpp"
#include "graph.hpp"
#include "positionLogBundle.hpp"
#include "arrival.hpp"
#include "travel.hpp"
#include "stateBundle.hpp"
#include "extractorResult.hpp"
#include "extractor/index.hpp"

using std::string;
using std::cout;
using std::endl;
using std::min;
using std::max;
using std::map;
using std::async;
using std::future;
namespace po = boost::program_options;
namespace gregorian = boost::gregorian;

const string DESCRIPTOR_FILENAME = "descriptor.txt";
const string DEFAULT_POSITION_LOG_FOLDER = "../../data/positionLog/";
const string DEFAULT_STATE_FOLDER = "../../data/state/";
const string DEFAULT_ARRIVAL_FOLDER = "../../data/arrival/";
const string DEFAULT_TRAVEL_FOLDER = "../../data/travel/";
const string DEFAULT_GRAPH_FILE = "../../data/graph.in";

// Handler for signal 11 error (segmentation fault), to print stack trace
void segmentationFaultHandler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

void writeArrivals(string filePath, const vector<Arrival> &arrivals, Graph* graph) {
  FILE *file;
  file = fopen(filePath.c_str(), "w+");

  map<BusStop*,int> busStopToIdx;
  map<Corridor*,int> corridorToIdx;

  // Write metadata
  fprintf(file, "%d\n", (int)graph->busStops.size());
  for (int i = 0; i < graph->busStops.size(); i++) {
    busStopToIdx[graph->busStops[i]] = i;
    fprintf(file, "%s\n", graph->busStops[i]->name.c_str());
  }
  fprintf(file, "%d\n", (int)graph->corridors.size());
  for (int i = 0; i < graph->corridors.size(); i++) {
    corridorToIdx[graph->corridors[i]] = i;
    fprintf(file, "%s\n", graph->corridors[i]->name.c_str());
  }

  // Write actual data
  fprintf(file, "%d\n", (int)arrivals.size());
  for (Arrival a : arrivals) {
    fprintf(file, "%s\n", a.busId.c_str());

    fprintf(
        file,
        "%d %d %d %d\n",
        corridorToIdx[a.corridor],
        busStopToIdx[a.busStop],
        busStopToIdx[a.destinationBusStop],
        a.unixTime
    );
  }

  fclose(file);
}

void writeTravels(string filePath, const vector<Travel> &travels, Graph* graph) {
  FILE *file;
  file = fopen(filePath.c_str(), "w+");

  map<BusStop*,int> busStopToIdx;

  // Write metadata
  fprintf(file, "%d\n", (int)graph->busStops.size());
  for (int i = 0; i < graph->busStops.size(); i++) {
    busStopToIdx[graph->busStops[i]] = i;
    fprintf(file, "%s\n", graph->busStops[i]->name.c_str());
  }

  // Write actual data
  fprintf(file, "%d\n", (int)travels.size());
  for (Travel t : travels) {
    fprintf(file, "%s\n", t.busId.c_str());

    fprintf(
        file,
        "%d %d %d %d\n",
        busStopToIdx[t.fromBusStop],
        busStopToIdx[t.toBusStop],
        t.startUnixTime,
        t.durationSec
    );
  }

  fclose(file);
}

void extractOneDay(
    Descriptor* descriptor,
    Graph* graph,
    string positionLogFolderPath,
    string stateFolderPath,
    string arrivalFolderPath,
    string travelFolderPath
  ) {
  string yesterdayStr = gregorian::to_iso_extended_string(descriptor->lastExtractionDate - gregorian::date_duration(1));
  string dateStr = gregorian::to_iso_extended_string(descriptor->lastExtractionDate);

  // Load position log
  PositionLogBundle positionLogBundle;
  positionLogBundle.readFromFile(positionLogFolderPath + dateStr + ".plog");

  // Load state
  StateBundle stateBundle;
  stateBundle.readFromFile(graph, stateFolderPath + yesterdayStr + ".state");

  // Find how much threads we can spawn
  int concurentThreadsSupported = std::thread::hardware_concurrency();
  if (concurentThreadsSupported == 0) {
    concurentThreadsSupported = 1;
  }
  concurentThreadsSupported = min(concurentThreadsSupported, Constant::MAX_CORE_USED);

  // List of bus ids to be processed
  vector<string> busIds = positionLogBundle.getBusIds();
  sort(busIds.begin(), busIds.end());

  // Compute how many jobs per thread
  int nBus = busIds.size();
  int busPerCore = (int)ceil(1.0 * nBus / concurentThreadsSupported);

  // Create extractor
  Extractor extractor;
  fprintf(stdout, "Extracting %s...\n", dateStr.c_str());

  // Split position log & state by bus id, send them to threads
  vector<future<ExtractorResult>> promisedResults;
  for (int i = 0; i < concurentThreadsSupported; i++) {
    PositionLogBundle localPositionLogBundle;
    StateBundle localStateBundle;

    int startIdx = i * busPerCore;
    int endIdx = min((i + 1) * busPerCore - 1, nBus - 1);
    for (int j = startIdx; j <= endIdx; j++) {
      string busId = busIds[j];
      localPositionLogBundle.addPositionLogByCloning(busId, positionLogBundle.getPositionLog(busId));
      localStateBundle.addStateByCloning(busId, stateBundle.getState(busId));

      // IMPORTANT: Remove to save memory
      positionLogBundle.erase(busId);
      stateBundle.erase(busId);
    }

    promisedResults.push_back(
      async(
          std::launch::async,
          &Extractor::extract,
          &extractor,
          descriptor,
          graph,
          localPositionLogBundle,
          stateBundle
      )
    );
  }

  // Get thread's result (arrival, travel, state)
  vector<ExtractorResult> extractorResults;
  for (int i = 0; i < concurentThreadsSupported; i++) {
    extractorResults.push_back(promisedResults[i].get());
  }

  vector<Arrival> arrivals;
  vector<Travel> travels;
  StateBundle finalState;
  for (ExtractorResult extractorResult : extractorResults) {
    // Accumulate arrivals
    for (Arrival a : extractorResult.arrivals) {
      arrivals.push_back(a);
    }

    // Accumulate travels
    for (Travel t : extractorResult.travels) {
      travels.push_back(t);
    }

    // Accumulate final states
    vector<string> busIds = extractorResult.finalState.getBusIds();
    for (string busId : busIds) {
      finalState.addStateByCloning(busId, extractorResult.finalState.getState(busId));
    }
  }

  // Clean up to free memory
  extractorResults.clear();

  // Write result
  string arrivalFilePath = arrivalFolderPath + dateStr + ".arrival";
  writeArrivals(arrivalFilePath, arrivals, graph);

  string travelFilePath = travelFolderPath + dateStr + ".travel";
  writeTravels(travelFilePath, travels, graph);

  string stateFilePath = stateFolderPath + dateStr + ".state";
  finalState.writeToFile(graph, stateFilePath);
}


int main(int argc, char *argv[]) {
  signal(SIGSEGV, segmentationFaultHandler);

  po::options_description desc("Allowed options");

  string argPositionLog;
  string argState;
  string argGraph;
  string argArrival;
  string argTravel;

  desc.add_options()
    ("help,h", "Produce help message")
    ("all,a", "Do extraction from the beginning of time")
    ("date,d", po::value<string>(), "Ending date for extraction in YYYY-MM-DD (defaults to yesterday)")

    // Syntactic salted, no shorthand for these options
    ("positionLog", po::value<string>(&argPositionLog)->default_value(DEFAULT_POSITION_LOG_FOLDER), "Position log directory (all with .plog extension)")
    ("state", po::value<string>(&argState)->default_value(DEFAULT_STATE_FOLDER), "State directory (including the descriptor)")
    ("graph", po::value<string>(&argGraph)->default_value(DEFAULT_GRAPH_FILE), "Graph file")
    ("arrival", po::value<string>(&argArrival)->default_value(DEFAULT_ARRIVAL_FOLDER), "Arrival output folder")
    ("travel", po::value<string>(&argTravel)->default_value(DEFAULT_TRAVEL_FOLDER), "Travel output folder")
  ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    cout << desc << endl;
  }

  // Maybe destroy if all
  if (vm.count("all")) {
    system(("rm -rf " + argState).c_str());
    system(("rm -rf " + argArrival).c_str());
    system(("rm -rf " + argTravel).c_str());
  }

  // Ensure dirs
  system(("mkdir -p " + argState + " && touch " + argState + DESCRIPTOR_FILENAME).c_str());
  system(("mkdir -p " + argArrival).c_str());
  system(("mkdir -p " + argTravel).c_str());

  // Get descriptor
  Descriptor *descriptor = new Descriptor();
  descriptor->readFromFile(argState + DESCRIPTOR_FILENAME);

  // Get target end date
  gregorian::date endDate;
  if (vm.count("date")) {
    endDate = gregorian::from_simple_string(vm["date"].as<string>());
  } else {
    // Default to yesterday
    endDate = gregorian::day_clock::local_day() - gregorian::date_duration(1);
  }

  // Load graph
  Graph *graph = new Graph(argGraph);

  // Extract day by day
  while (descriptor->lastExtractionDate < endDate) {
    descriptor->lastExtractionDate = descriptor->lastExtractionDate + gregorian::date_duration(1);
    extractOneDay(descriptor, graph, argPositionLog, argState, argArrival, argTravel);

    // Save progress
    descriptor->writeToFile(argState + DESCRIPTOR_FILENAME);
  }

  delete descriptor;
  delete graph;
}
