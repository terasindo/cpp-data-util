/*
  Generate auxilliary data for backend API.
 */

#include <boost/program_options.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <string>
#include <iostream>
#include <thread>
#include <future>
#include <cstdio>
#include <map>
#include <csignal>
#include <cstdlib>
#include <execinfo.h>
#include <unistd.h>

#include "graph.hpp"
#include "auxDataGenerator/autocompleteEntry.hpp"
#include "auxDataGenerator/autocompleteGenerator.hpp"
#include "auxDataGenerator/pathNode.hpp"
#include "auxDataGenerator/pathGroup.hpp"
#include "auxDataGenerator/pathGenerator.hpp"

using std::string;
using std::cout;
using std::endl;
using std::min;
using std::max;
using std::map;
using std::vector;
namespace po = boost::program_options;
namespace gregorian = boost::gregorian;

const string DEFAULT_GRAPH_FILE = "../../data/graph.in";
const string DEFAULT_OUTPUT_FOLDER = "../../data/";

// Handler for signal 11 error (segmentation fault), to print stack trace
void segmentationFaultHandler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

void writeAutocompleteEntries(string filePath, const vector<AutocompleteEntry> &entries, Graph* graph) {
  FILE *file;
  file = fopen(filePath.c_str(), "w+");

  map<BusStop*,int> busStopToIdx;
  map<Corridor*,int> corridorToIdx;

  // Write metadata
  fprintf(file, "%d\n", (int)graph->busStops.size());
  for (int i = 0; i < graph->busStops.size(); i++) {
    busStopToIdx[graph->busStops[i]] = i;
    fprintf(file, "%s\n", graph->busStops[i]->name.c_str());
  }
  fprintf(file, "%d\n", (int)graph->corridors.size());
  for (int i = 0; i < graph->corridors.size(); i++) {
    corridorToIdx[graph->corridors[i]] = i;
    fprintf(file, "%s\n", graph->corridors[i]->name.c_str());
  }

  // Write actual data
  fprintf(file, "%d\n", (int)entries.size());
  for (AutocompleteEntry a : entries) {
    fprintf(
        file,
        "%d %d %d %d\n",
        corridorToIdx[a.corridor],
        busStopToIdx[a.fromBusStop],
        busStopToIdx[a.toBusStop],
        busStopToIdx[a.nextCommonBusStop]
    );
  }

  fclose(file);
}

bool activeCorridorComp(const pair<Corridor*, BusStop*> &a, const pair<Corridor*, BusStop*> &b) {
  return a.first->index < b.first->index;
}

void writePaths(string filePath, const vector<PathGroup> &pathGroups, Graph* graph) {
  FILE *file;
  file = fopen(filePath.c_str(), "w+");

  map<BusStop*,int> busStopToIdx;
  map<Corridor*,int> corridorToIdx;

  // Write metadata
  fprintf(file, "%d\n", (int)graph->busStops.size());
  for (int i = 0; i < graph->busStops.size(); i++) {
    busStopToIdx[graph->busStops[i]] = i;
    fprintf(file, "%s\n", graph->busStops[i]->name.c_str());
  }
  fprintf(file, "%d\n", (int)graph->corridors.size());
  for (int i = 0; i < graph->corridors.size(); i++) {
    corridorToIdx[graph->corridors[i]] = i;
    fprintf(file, "%s\n", graph->corridors[i]->name.c_str());
  }

  // Write actual data
  fprintf(file, "%d\n", (int)pathGroups.size());
  for (int i = 0; i < pathGroups.size(); i++) {
    const PathGroup &pathGroup = pathGroups[i];
    fprintf(file, "%d %d %d\n", busStopToIdx[pathGroup.from], busStopToIdx[pathGroup.to], (int)pathGroup.paths.size());
    for (int j = 0; j < pathGroup.paths.size(); j++) {
      fprintf(file, "%d %lf\n", (int)pathGroup.paths[j].size(), pathGroup.ranks[j]);
      for (PathNode pathNode : pathGroup.paths[j]) {
        fprintf(file, "%d %d\n", pathNode.action, busStopToIdx[pathNode.busStop]);

        if (pathNode.action == PathNode::ACTION_GET_ON) {
          vector<pair<Corridor*, BusStop*>> activeCorridors;
          for (Corridor* c : pathNode.busStop->corridors) {
            if (pathNode.isActiveCorridor(c)) {
              activeCorridors.push_back(make_pair(c, pathNode.getCorridorEnd(c)));
            }
          }

          // Sort to get common corridor displayed first
          sort(activeCorridors.begin(), activeCorridors.end(), activeCorridorComp);

          fprintf(file, "%d\n", (int)activeCorridors.size());
          for (int i = 0; i < activeCorridors.size(); i++) {
            fprintf(file, "%d %d\n", corridorToIdx[activeCorridors[i].first], busStopToIdx[activeCorridors[i].second]);
          }
        }
      }
    }
  }

  fclose(file);
}

int main(int argc, char *argv[]) {
  signal(SIGSEGV, segmentationFaultHandler);

  po::options_description desc("Allowed options");

  string argGraph;
  string argOutput;
  string argTravel;
  string argAggregated;

  desc.add_options()
    ("help,h", "Produce help message")
    ("path,p", "Generate all pair path")
    ("autocomplete,c", "Generate autocompleter")

    // Syntactic salted, no shorthand for these options
    ("graph", po::value<string>(&argGraph)->default_value(DEFAULT_GRAPH_FILE), "Graph file")
    ("output", po::value<string>(&argOutput)->default_value(DEFAULT_OUTPUT_FOLDER), "Output folder")
  ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help") || (vm.count("path") + vm.count("autocomplete") == 0)) {
    cout << desc << endl;
    return 0;
  }

  // Ensure dirs
  system(("mkdir -p " + argOutput).c_str());

  // Read data
  Graph *graph = new Graph(argGraph);

  if (vm.count("path")) {
    PathGenerator pathGenerator(graph);
    vector<PathGroup> paths = pathGenerator.generateAllPairPaths();

    string pathPath = argOutput + "path.aux"; // No pun intended
    writePaths(pathPath, paths, graph);
  }

  if (vm.count("autocomplete")) {
    AutocompleteGenerator autocompleteGenerator(graph);
    vector<AutocompleteEntry> entries = autocompleteGenerator.generateAutocompleteEntries();

    string autocompletePath = argOutput + "autocomplete.aux";
    writeAutocompleteEntries(autocompletePath, entries, graph);
  }

  delete graph;
}
