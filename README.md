# C++ Data Util
Terasi tools built with C++ for higher performance.

1. Make sure you have data folder outside (../data)
2. Make sure you have position logs extracted in data folder (../data/positionLog/####-##-##.plog)
3. Generate graph there using node-data-util (../data/graph.in)
4. Do `mkdir build`
5. Do `cd build`
6. Do `cmake ..`
7. Do `make`
8. Do arrival and travel extraction, `./extract -ad 2016-01-30`
9. Do waiting time and travel time aggregation `./aggregate -d 2016-01-31`
10. Generate auxiliary file `./auxDataGenerator -cp` (may take really long time)
11. You should be ready to go
