#include "gmock/gmock.h"

#include "corridor.hpp"
#include "busStop.hpp"
#include "position.hpp"
#include "extractor/splitCriteriaChooser/_common.hpp"

using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;

class SplitCriteriaTypeCommonTest : public Test {
};

TEST_F(SplitCriteriaTypeCommonTest, findClosestFromEnd) {
  BusStop* busStops[] = {
      new BusStop(0, 10, 10),
      new BusStop(1, 20, 20),
      new BusStop(2, 30, 30)
  };

  Position positions[] = {
      Position(busStops[0], -1, -1), // DUMMY
      Position(busStops[0], 6, 8),
      Position(busStops[0], 10, 9),
      Position(busStops[0], 12, 12),
      Position(busStops[0], 12, 16),
      Position(busStops[1], 14, 16),
      Position(busStops[1], 10, 10),
      Position(busStops[1], 21, 21),
      Position(busStops[2], 20, 20),
      Position(busStops[2], 12, 11)
  };

  {
    // Should work when endIndex at front
    EXPECT_THAT(SplitCriteriaType::Common::findClosestFromEnd(positions, 1, busStops[0], 1), Eq(1));
    EXPECT_THAT(SplitCriteriaType::Common::findClosestFromEnd(positions, 1, busStops[0], 2), Eq(2));
    EXPECT_THAT(SplitCriteriaType::Common::findClosestFromEnd(positions, 1, busStops[0], 3), Eq(2));
    EXPECT_THAT(SplitCriteriaType::Common::findClosestFromEnd(positions, 1, busStops[0], 4), Eq(2));
  }

  {
    // Should work when endIndex at mid
    EXPECT_THAT(SplitCriteriaType::Common::findClosestFromEnd(positions, 1, busStops[1], 5), Eq(5));
    EXPECT_THAT(SplitCriteriaType::Common::findClosestFromEnd(positions, 1, busStops[1], 6), Eq(5));
    EXPECT_THAT(SplitCriteriaType::Common::findClosestFromEnd(positions, 1, busStops[1], 7), Eq(7));
  }

  {
    // Should work when endIndex at end
    EXPECT_THAT(SplitCriteriaType::Common::findClosestFromEnd(positions, 1, busStops[2], 8), Eq(8));
    EXPECT_THAT(SplitCriteriaType::Common::findClosestFromEnd(positions, 1, busStops[2], 9), Eq(8));
  }

  for (BusStop* busStop : busStops) {
    delete busStop;
  }
}
