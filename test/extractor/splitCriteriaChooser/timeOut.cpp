#include "gmock/gmock.h"

#include "corridor.hpp"
#include "busStop.hpp"
#include "position.hpp"
#include "extractor/splitCriteriaChooser/timeOut.hpp"

using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;

class SplitCriteriaTypeTimeOutTest : public Test {
};

TEST_F(SplitCriteriaTypeTimeOutTest, getSplitCriteria) {
  BusStop* busStops[] = {
      new BusStop(0, 0, 0),
      new BusStop(1, 10, 10)
  };
  Corridor* corridor = new Corridor(busStops[0], busStops[1]);

  {
    // Should return split object if it is
    {
      Position positions[] = {
          Position(0, -1, -1), // DUMMY
          Position(10, 10, 10),
          Position(12, 10, 10),
          Position(15, 10, 10),
          Position(31, 10, 10)
      };
      SplitCriteriaType::TimeOut chooser(corridor, positions);
      chooser.setStateExpirationDurationSecond(15);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.getTakeLeft(), Eq(3));
      EXPECT_THAT(result.getTakeRight(), Eq(1));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
    {
      Position positions[] = {
          Position(0, -1, -1), // DUMMY
          Position(10, 10, 10),
          Position(12, 10, 10),
          Position(15, 10, 10),
          Position(131, 10, 10)
      };
      SplitCriteriaType::TimeOut chooser(corridor, positions);
      chooser.setStateExpirationDurationSecond(15);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.getTakeLeft(), Eq(3));
      EXPECT_THAT(result.getTakeRight(), Eq(1));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
    {
      Position positions[] = {
          Position(0, -1, -1), // DUMMY
          Position(10, 10, 10),
          Position(12, 10, 10),
          Position(15, 10, 10),
          Position(30, 10, 10)
      };
      SplitCriteriaType::TimeOut chooser(corridor, positions);
      chooser.setStateExpirationDurationSecond(15);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.getTakeLeft(), Eq(3));
      EXPECT_THAT(result.getTakeRight(), Eq(1));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
  }

  {
    // Should return empty split object if it is not
    {
      Position positions[] = {
          Position(0, -1, -1), // DUMMY
          Position(10, 10, 10),
          Position(12, 10, 10),
          Position(15, 10, 10),
          Position(16, 10, 10)
      };
      SplitCriteriaType::TimeOut chooser(corridor, positions);
      chooser.setStateExpirationDurationSecond(15);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(0, -1, -1), // DUMMY
          Position(10, 10, 10),
          Position(12, 10, 10),
          Position(15, 10, 10),
          Position(29, 10, 10)
      };
      SplitCriteriaType::TimeOut chooser(corridor, positions);
      chooser.setStateExpirationDurationSecond(15);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
  }

  delete corridor;
  for (BusStop* busStop : busStops) {
    delete busStop;
  }
}
