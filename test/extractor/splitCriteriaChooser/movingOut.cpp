#include "gmock/gmock.h"

#include "corridor.hpp"
#include "busStop.hpp"
#include "position.hpp"
#include "extractor/splitCriteriaChooser/movingOut.hpp"

using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;

class SplitCriteriaTypeMovingOutTest : public Test {
};


TEST_F(SplitCriteriaTypeMovingOutTest, getSplitCriteria) {
  BusStop* busStops[] = {
      new BusStop(0, 0, 0),
      new BusStop(1, 30, 60)
  };
  Corridor* corridor = new Corridor(busStops[0], busStops[1]);
  corridor->polylineFromStart.push_back(Point(0, 0));
  corridor->polylineFromStart.push_back(Point(10, 20));
  corridor->polylineFromStart.push_back(Point(20, 40));
  corridor->polylineFromStart.push_back(Point(30, 60));

  {
    // Should return split object if it is
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[0], 3, 6),
          Position(busStops[0], 5, 10),
          Position(busStops[0], 7, 14),
          Position(busStops[0], 20, 18)
      };

      SplitCriteriaType::MovingOut chooser(corridor, positions);
      chooser.setTooFarRoughThreshold(5);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);

      EXPECT_THAT(result.getTakeLeft(), Eq(3));
      EXPECT_THAT(result.getTakeRight(), Eq(1));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
    {
      // Noise added
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[0], 4, 6),
          Position(busStops[0], 5, 11),
          Position(busStops[0], 7, 16),
          Position(busStops[0], 15, 18)
      };

      SplitCriteriaType::MovingOut chooser(corridor, positions);
      chooser.setTooFarRoughThreshold(5);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);

      EXPECT_THAT(result.getTakeLeft(), Eq(3));
      EXPECT_THAT(result.getTakeRight(), Eq(1));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
  }

  {
    // Should return no split if it is not
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[0], 3, 6),
          Position(busStops[0], 5, 10),
          Position(busStops[0], 7, 14),
          Position(busStops[0], 9, 18)
      };

      SplitCriteriaType::MovingOut chooser(corridor, positions);
      chooser.setTooFarRoughThreshold(5);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);

      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[0], 3, 6),
          Position(busStops[0], 5, 10),
          Position(busStops[0], 7, 14),
          Position(busStops[0], 9, 18)
      };

      SplitCriteriaType::MovingOut chooser(corridor, positions);
      chooser.setTooFarRoughThreshold(5);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);

      EXPECT_THAT(result.isSplit(), Eq(false));
    }
  }

  delete corridor;
  for (BusStop* busStop : busStops) {
    delete busStop;
  }
}