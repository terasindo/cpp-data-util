#include "gmock/gmock.h"

#include "corridor.hpp"
#include "busStop.hpp"
#include "position.hpp"
#include "extractor/splitCriteriaChooser/secondLastIsTerminal.hpp"

using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;

class SplitCriteriaTypeSecondLastIsTerminalTest : public Test {
};

TEST_F(SplitCriteriaTypeSecondLastIsTerminalTest, getSplitCriteria) {
  BusStop* busStops[] = {
      new BusStop(0, 0, 0),
      new BusStop(1, 10, 10),
      new BusStop(2, 20, 20),
      new BusStop(3, 30, 30),
      new BusStop(4, 40, 40)
  };
  Corridor* corridor = new Corridor(busStops[0], busStops[4]);

  {
    // Should return split object if it is
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[1], 10, 10),
          Position(busStops[2], 20, 20),
          Position(busStops[3], 30, 30),
          Position(busStops[4], 40, 40),
          Position(busStops[3], 30, 30)
      };
      SplitCriteriaType::SecondLastIsTerminal chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.getTakeLeft(), Eq(4));
      EXPECT_THAT(result.getTakeRight(), Eq(2));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[3], 30, 30),
          Position(busStops[2], 20, 20),
          Position(busStops[1], 10, 10),
          Position(busStops[0], 0, 0),
          Position(busStops[1], 10, 10)
      };
      SplitCriteriaType::SecondLastIsTerminal chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.getTakeLeft(), Eq(4));
      EXPECT_THAT(result.getTakeRight(), Eq(2));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
  }

  {
    // Should return split object if it is, respecting closest splitting point
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[1], 10, 10),
          Position(busStops[2], 20, 20),
          Position(busStops[3], 30, 30),
          Position(busStops[4], 35, 35),
          Position(busStops[4], 41, 40), // Here
          Position(busStops[4], 35, 38),
          Position(busStops[4], 32, 32),
          Position(busStops[3], 30, 30)
      };
      SplitCriteriaType::SecondLastIsTerminal chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.getTakeLeft(), Eq(5));
      EXPECT_THAT(result.getTakeRight(), Eq(4));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[3], 30, 30),
          Position(busStops[2], 20, 20),
          Position(busStops[1], 10, 10),
          Position(busStops[0], 4, 4),
          Position(busStops[0], 2, 2),
          Position(busStops[0], 0, 0), // Here
          Position(busStops[1], 10, 10)
      };
      SplitCriteriaType::SecondLastIsTerminal chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.getTakeLeft(), Eq(6));
      EXPECT_THAT(result.getTakeRight(), Eq(2));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
  }

  {
    // Should return undefined if it is too short to be called segment
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[3], -1, -1),
          Position(busStops[4], -1, -1)
      };
      SplitCriteriaType::SecondLastIsTerminal chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[4], -1, -1),
          Position(busStops[3], -1, -1)
      };
      SplitCriteriaType::SecondLastIsTerminal chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[0], -1, -1),
          Position(busStops[1], -1, -1)
      };
      SplitCriteriaType::SecondLastIsTerminal chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[1], -1, -1),
          Position(busStops[0], -1, -1)
      };
      SplitCriteriaType::SecondLastIsTerminal chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
  }

  {
    // Should return undefined if second last is not terminal
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[1], -1, -1),
          Position(busStops[2], -1, -1),
          Position(busStops[3], -1, -1),
          Position(busStops[4], -1, -1)
      };
      SplitCriteriaType::SecondLastIsTerminal chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[2], -1, -1),
          Position(busStops[3], -1, -1),
          Position(busStops[4], -1, -1),
          Position(busStops[3], -1, -1),
          Position(busStops[2], -1, -1)
      };
      SplitCriteriaType::SecondLastIsTerminal chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[2], -1, -1),
          Position(busStops[1], -1, -1),
          Position(busStops[0], -1, -1),
          Position(busStops[1], -1, -1),
          Position(busStops[2], -1, -1)
      };
      SplitCriteriaType::SecondLastIsTerminal chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
  }

  delete corridor;
  for (BusStop* busStop : busStops) {
    delete busStop;
  }
}
