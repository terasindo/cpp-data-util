#include "gmock/gmock.h"

#include "corridor.hpp"
#include "busStop.hpp"
#include "position.hpp"
#include "extractor/splitCriteriaChooser/arrivedOnTerminal.hpp"

using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;

class SplitCriteriaTypeArrivedOnTerminalTest : public Test {
};


TEST_F(SplitCriteriaTypeArrivedOnTerminalTest, getSplitCriteria) {
  BusStop* busStops[] = {
      new BusStop(0, 0, 0),
      new BusStop(1, 10, 0),
      new BusStop(2, 20, 0),
      new BusStop(3, 30, 0),
      new BusStop(4, 40, 0)
  };
  Corridor* corridor = new Corridor(busStops[0], busStops[4]);

  {
    // Should return split object if it is
    {
      Position positions[] = {
          Position(busStops[2], -1, -1), // DUMMY
          Position(busStops[2], 20, 0),
          Position(busStops[3], 30, 0),
          Position(busStops[4], 35, 0),
          Position(busStops[4], 37, 0),
          Position(busStops[4], 39, 0), // Here
          Position(busStops[4], 38, 0)
      };

      SplitCriteriaType::ArrivedOnTerminal chooser(corridor, positions);
      chooser.setMinDurationToBeStayingMinute(3);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);

      EXPECT_THAT(result.getTakeLeft(), Eq(5));
      EXPECT_THAT(result.getTakeRight(), Eq(2));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
    {
      Position positions[] = {
          Position(busStops[2], -1, -1), // DUMMY
          Position(busStops[2], 20, 0),
          Position(busStops[3], 30, 0),
          Position(busStops[4], 35, 0),
          Position(busStops[4], 39, 0), // Here
          Position(busStops[4], 37, 0),
          Position(busStops[4], 37, 0)
      };

      SplitCriteriaType::ArrivedOnTerminal chooser(corridor, positions);
      chooser.setMinDurationToBeStayingMinute(3);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);

      EXPECT_THAT(result.getTakeLeft(), Eq(4));
      EXPECT_THAT(result.getTakeRight(), Eq(3));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }

    {
      Position positions[] = {
          Position(busStops[3], -1, -1), // DUMMY
          Position(busStops[2], 20, 0),
          Position(busStops[1], 10, 0),
          Position(busStops[0], 6, 0),
          Position(busStops[0], 3, 0),
          Position(busStops[0], 1, 0), // Here
          Position(busStops[0], 2, 0)
      };

      SplitCriteriaType::ArrivedOnTerminal chooser(corridor, positions);
      chooser.setMinDurationToBeStayingMinute(3);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);

      EXPECT_THAT(result.getTakeLeft(), Eq(5));
      EXPECT_THAT(result.getTakeRight(), Eq(2));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
    {
      Position positions[] = {
          Position(busStops[2], -1, -1), // DUMMY
          Position(busStops[2], 20, 0),
          Position(busStops[1], 10, 0),
          Position(busStops[0], 6, 0),
          Position(busStops[0], 2, 0), // Here
          Position(busStops[0], 3, 0),
          Position(busStops[0], 4, 0)
      };

      SplitCriteriaType::ArrivedOnTerminal chooser(corridor, positions);
      chooser.setMinDurationToBeStayingMinute(3);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);

      EXPECT_THAT(result.getTakeLeft(), Eq(4));
      EXPECT_THAT(result.getTakeRight(), Eq(3));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
  }

  {
    // Should return no split if it is not
    {
      Position positions[] = {
          Position(busStops[2], -1, -1), // DUMMY
          Position(busStops[2], 20, 0),
          Position(busStops[3], 30, 0),
          Position(busStops[3], 31, 0),
          Position(busStops[3], 32, 0),
          Position(busStops[3], 33, 0),
          Position(busStops[4], 39, 0),
          Position(busStops[4], 38, 0)
      };

      SplitCriteriaType::ArrivedOnTerminal chooser(corridor, positions);
      chooser.setMinDurationToBeStayingMinute(3);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[2], -1, -1), // DUMMY
          Position(busStops[2], 20, 0),
          Position(busStops[3], 30, 0),
          Position(busStops[3], 31, 0),
          Position(busStops[3], 32, 0),
          Position(busStops[3], 33, 0),
          Position(busStops[4], 39, 0),
          Position(busStops[4], 38, 0)
      };

      SplitCriteriaType::ArrivedOnTerminal chooser(corridor, positions);
      chooser.setMinDurationToBeStayingMinute(3);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }

    {
      Position positions[] = {
          Position(busStops[3], -1, -1), // DUMMY
          Position(busStops[2], 20, 0),
          Position(busStops[2], 10, 0),
          Position(busStops[2], 6, 0),
          Position(busStops[1], 3, 0),
          Position(busStops[0], 1, 0),
          Position(busStops[0], 2, 0)
      };

      SplitCriteriaType::ArrivedOnTerminal chooser(corridor, positions);
      chooser.setMinDurationToBeStayingMinute(3);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[2], -1, -1), // DUMMY
          Position(busStops[2], 20, 0),
          Position(busStops[1], 10, 0),
          Position(busStops[1], 6, 0),
          Position(busStops[1], 2, 0),
          Position(busStops[1], 3, 0),
          Position(busStops[0], 4, 0)
      };

      SplitCriteriaType::ArrivedOnTerminal chooser(corridor, positions);
      chooser.setMinDurationToBeStayingMinute(3);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
  }

  delete corridor;
  for (BusStop* busStop : busStops) {
    delete busStop;
  }
}