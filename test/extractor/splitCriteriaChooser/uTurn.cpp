#include "gmock/gmock.h"

#include "corridor.hpp"
#include "busStop.hpp"
#include "position.hpp"
#include "extractor/splitCriteriaChooser/uTurn.hpp"

using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;

class SplitCriteriaTestUTurnTest : public Test {
};

TEST_F(SplitCriteriaTestUTurnTest, getSplitCriteria) {
  BusStop* busStops[] = {
      new BusStop(0, 0, 0),
      new BusStop(1, 10, 10),
      new BusStop(2, 20, 20),
      new BusStop(3, 30, 30),
      new BusStop(4, 40, 40),
      new BusStop(5, 50, 50, true),
      new BusStop(6, 60, 60),
  };

  Corridor* corridor = new Corridor();

  {
    // Should return split object in terminal if there is terminal in U turn
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[2], 20, 20),
          Position(busStops[3], 30, 30), // Cycle start // Here
          Position(busStops[5], 50, 50),
          Position(busStops[4], 40, 40),
          Position(busStops[3], 30, 30)  // Cycle end
      };

      SplitCriteriaType::UTurn chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.getTakeLeft(), Eq(3));
      EXPECT_THAT(result.getTakeRight(), Eq(3));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[0], 0, 0),
          Position(busStops[1], 10, 10),
          Position(busStops[2], 20, 20), // Cycle start
          Position(busStops[3], 30, 30),
          Position(busStops[5], 50, 50), // Here
          Position(busStops[6], 60, 60),
          Position(busStops[4], 40, 40),
          Position(busStops[2], 20, 20)  // Cycle end
      };

      SplitCriteriaType::UTurn chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.getTakeLeft(), Eq(5));
      EXPECT_THAT(result.getTakeRight(), Eq(4));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
  }

  {
    // Should return split object in terminal if there is terminal in U turn, respecting position
    Position positions[] = {
        Position(busStops[0], -1, -1), // DUMMY
        Position(busStops[2], 20, 20),
        Position(busStops[3], 30, 30), // Cycle start
        Position(busStops[3], 31, 30),
        Position(busStops[3], 33, 30),
        Position(busStops[5], 40, 50),
        Position(busStops[5], 50, 50), // Here
        Position(busStops[5], 40, 50),
        Position(busStops[4], 40, 40),
        Position(busStops[3], 30, 35),
        Position(busStops[3], 30, 30) // Cycle end
    };
    SplitCriteriaType::UTurn chooser(corridor, positions);
    SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
    EXPECT_THAT(result.getTakeLeft(), Eq(6));
    EXPECT_THAT(result.getTakeRight(), Eq(5));
    EXPECT_THAT(result.isSplit(), Eq(true));
  }

  {
    // Should return empty split object in terminal if there is no terminal in U turn
    Position positions[] = {
        Position(busStops[0], -1, -1), // DUMMY
        Position(busStops[5], 40, 50), // Terminal is not in U Turn
        Position(busStops[5], 50, 50),
        Position(busStops[4], 40, 40),
        Position(busStops[3], 30, 35), // Cycle start
        Position(busStops[3], 30, 30),
        Position(busStops[2], 20, 20), // Here
        Position(busStops[4], 40, 40),
        Position(busStops[4], 43, 40)  // Cycle end
    };
    SplitCriteriaType::UTurn chooser(corridor, positions);
    SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
    EXPECT_THAT(result.getTakeLeft(), Eq(6));
    EXPECT_THAT(result.getTakeRight(), Eq(3));
    EXPECT_THAT(result.isSplit(), Eq(true));
  }

  {
    // Should return empty split object in terminal if there is no terminal in U turn, respecting position
    Position positions[] = {
        Position(busStops[0], -1, -1), // DUMMY
        Position(busStops[5], 40, 50), // Terminal is not in U Turn
        Position(busStops[5], 50, 50),
        Position(busStops[4], 40, 40),
        Position(busStops[3], 30, 35), // Cycle start
        Position(busStops[3], 30, 30),
        Position(busStops[2], 20, 20), // Here
        Position(busStops[2], 23, 22),
        Position(busStops[2], 22, 21),
        Position(busStops[4], 40, 40),
        Position(busStops[4], 43, 40)  // Cycle end
    };
    SplitCriteriaType::UTurn chooser(corridor, positions);
    SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
    EXPECT_THAT(result.getTakeLeft(), Eq(6));
    EXPECT_THAT(result.getTakeRight(), Eq(5));
    EXPECT_THAT(result.isSplit(), Eq(true));
  }

  {
    // Should return split object in cycle end in U turn
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[2], 20, 20),
          Position(busStops[3], 30, 30), // Cycle start
          Position(busStops[4], 40, 40),
          Position(busStops[6], 60, 60),
          Position(busStops[1], 40, 40), // Here
          Position(busStops[3], 30, 30)  // Cycle end
      };
      SplitCriteriaType::UTurn chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.getTakeLeft(), Eq(5));
      EXPECT_THAT(result.getTakeRight(), Eq(2));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[0], 0, 0),
          Position(busStops[1], 10, 10),
          Position(busStops[2], 20, 20), // Cycle start
          Position(busStops[3], 30, 30),
          Position(busStops[6], 60, 60),
          Position(busStops[4], 40, 40), // Here
          Position(busStops[2], 20, 20)  // Cycle end
      };
      SplitCriteriaType::UTurn chooser(corridor, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.getTakeLeft(), Eq(6));
      EXPECT_THAT(result.getTakeRight(), Eq(2));
      EXPECT_THAT(result.isSplit(), Eq(true));
    }
  }

  {
    // Should return split object in cycle end in U turn, respecting position
    Position positions[] = {
        Position(busStops[0], -1, -1), // DUMMY
        Position(busStops[0], -1, -1), // DUMMY
        Position(busStops[2], 20, 20),
        Position(busStops[3], 30, 30), // Cycle start
        Position(busStops[1], 40, 40),
        Position(busStops[6], 60, 60),
        Position(busStops[4], 30, 20),
        Position(busStops[4], 34, 20),
        Position(busStops[4], 40, 40), // Here
        Position(busStops[4], 34, 20),
        Position(busStops[3], 30, 30)  // Cycle end
    };
    SplitCriteriaType::UTurn chooser(corridor, positions);
    SplitCriteria result = chooser.getSplitCriteria(2, sizeof(positions)/sizeof(*positions) - 1);
    EXPECT_THAT(result.getTakeLeft(), Eq(7));
    EXPECT_THAT(result.getTakeRight(), Eq(3));
    EXPECT_THAT(result.isSplit(), Eq(true));
  }

  {
    // Should return not isSplit if it is not
    Position positions[] = {
        Position(busStops[0], -1, -1), // DUMMY
        Position(busStops[1], -1, -1),
        Position(busStops[2], -1, -1),
        Position(busStops[3], -1, -1),
        Position(busStops[4], -1, -1),
        Position(busStops[5], -1, -1)
    };
    SplitCriteriaType::UTurn chooser(corridor, positions);
    SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
    EXPECT_THAT(result.isSplit(), Eq(false));
  }

  for (BusStop* busStop : busStops) {
    delete busStop;
  }
  delete corridor;
}

TEST_F(SplitCriteriaTestUTurnTest, getSplitCriteriaCorridor12) {
  BusStop* busStops[] = {
      new BusStop(0, 0, 0, false, "Danau Agung"),
      new BusStop(1, 10, 10, false, "Sunter SMP 140"),
      new BusStop(2, 20, 20, false, "Sunter Karya"),           // 1       5
      new BusStop(3, 30, 30, false, "Sunter Boulevard Barat"), //     3
      new BusStop(4, 40, 40, false, "Sunter Kelapa Gading"),   //   2   4
      new BusStop(5, 50, 50, false, "Plumpang Pertamina"),
      new BusStop(6, 60, 60, false, "Walikota Jakarta Utara"),
      new BusStop(7, 70, 70, false, "Permai Koja"),
      new BusStop(8, 80, 80, false, "Enggano"),
      new BusStop(9, 90, 90, true, "Tanjung Priok")
  };

  Corridor* corridor12 = new Corridor("12");

  {
    // Should return empty split object for false U turn
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[1], 20, 20),
          Position(busStops[2], 30, 30),

          Position(busStops[4], 31, 30),
          Position(busStops[3], 33, 30),
          Position(busStops[4], 40, 50)
      };
      SplitCriteriaType::UTurn chooser(corridor12, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[1], 20, 20),
          Position(busStops[2], 30, 30),

          Position(busStops[4], 31, 30),
          Position(busStops[3], 33, 30),
          Position(busStops[4], 40, 50),

          Position(busStops[6], 40, 50)
      };
      SplitCriteriaType::UTurn chooser(corridor12, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[6], 20, 20),
          Position(busStops[5], 30, 30),

          Position(busStops[4], 31, 30),
          Position(busStops[3], 33, 30),
          Position(busStops[4], 40, 50),

          Position(busStops[1], 40, 50)
      };
      SplitCriteriaType::UTurn chooser(corridor12, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[0], 20, 20),
          Position(busStops[0], 30, 30),
          Position(busStops[2], 30, 30),
          Position(busStops[2], 30, 30),

          Position(busStops[4], 31, 30),
          Position(busStops[4], 31, 30),
          Position(busStops[3], 33, 30),
          Position(busStops[3], 33, 30),
          Position(busStops[3], 33, 30),
          Position(busStops[3], 33, 30),
          Position(busStops[4], 40, 50),

          Position(busStops[6], 40, 50)
      };
      SplitCriteriaType::UTurn chooser(corridor12, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[6], 20, 20),
          Position(busStops[6], 30, 30),
          Position(busStops[6], 30, 30),

          Position(busStops[4], 31, 30),
          Position(busStops[3], 33, 30),
          Position(busStops[3], 33, 30),
          Position(busStops[3], 33, 30),
          Position(busStops[4], 40, 50),
          Position(busStops[4], 40, 50),

          Position(busStops[0], 40, 50)
      };
      SplitCriteriaType::UTurn chooser(corridor12, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
  }

  {
    // Should return split object for true U turn
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[0], 0, 0),
          Position(busStops[1], 10, 10),
          Position(busStops[2], 20, 20),
          Position(busStops[4], 40, 40),
          Position(busStops[3], 30, 30),
          Position(busStops[4], 40, 40),
          Position(busStops[2], 20, 20)
      };
      SplitCriteriaType::UTurn chooser(corridor12, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(true));
      EXPECT_THAT(result.getTakeLeft(), Eq(5));
      EXPECT_THAT(result.getTakeRight(), Eq(3));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[7], 70, 70),
          Position(busStops[6], 60, 60),
          Position(busStops[5], 50, 50),
          Position(busStops[4], 40, 40),
          Position(busStops[3], 30, 30),
          Position(busStops[4], 40, 40),
          Position(busStops[5], 50, 50)
      };
      SplitCriteriaType::UTurn chooser(corridor12, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(true));
      EXPECT_THAT(result.getTakeLeft(), Eq(5));
      EXPECT_THAT(result.getTakeRight(), Eq(3));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[0], 0, 0),
          Position(busStops[0], 2, 1),
          Position(busStops[2], 19, 15),
          Position(busStops[4], 39, 35),
          Position(busStops[4], 40, 41),
          Position(busStops[3], 30, 20),
          Position(busStops[3], 30, 29),
          Position(busStops[4], 40, 40),
          Position(busStops[2], 20, 18)
      };
      SplitCriteriaType::UTurn chooser(corridor12, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(true));
      EXPECT_THAT(result.getTakeLeft(), Eq(7));
      EXPECT_THAT(result.getTakeRight(), Eq(3));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[7], 70, 70),
          Position(busStops[7], 70, 65),
          Position(busStops[5], 50, 55),
          Position(busStops[4], 46, 45),
          Position(busStops[4], 40, 40),
          Position(busStops[4], 38, 40),
          Position(busStops[3], 30, 30),
          Position(busStops[4], 40, 40),
          Position(busStops[4], 45, 45),
          Position(busStops[5], 50, 50)
      };
      SplitCriteriaType::UTurn chooser(corridor12, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(true));
      EXPECT_THAT(result.getTakeLeft(), Eq(7));
      EXPECT_THAT(result.getTakeRight(), Eq(4));
    }
  }

  {
    // Should return empty split object for non U turn
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[6], -1, -1),
          Position(busStops[5], -1, -1),
          Position(busStops[4], -1, -1),
          Position(busStops[3], -1, -1),
          Position(busStops[2], -1, -1),
          Position(busStops[1], -1, -1),
          Position(busStops[0], -1, -1)
      };
      SplitCriteriaType::UTurn chooser(corridor12, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[0], -1, -1),
          Position(busStops[1], -1, -1),
          Position(busStops[2], -1, -1),
          Position(busStops[3], -1, -1),
          Position(busStops[4], -1, -1),
          Position(busStops[5], -1, -1),
          Position(busStops[6], -1, -1)
      };
      SplitCriteriaType::UTurn chooser(corridor12, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[6], -1, -1),
          Position(busStops[5], -1, -1),
          Position(busStops[4], -1, -1),
          Position(busStops[4], -1, -1),
          Position(busStops[3], -1, -1),
          Position(busStops[3], -1, -1),
          Position(busStops[2], -1, -1),
          Position(busStops[2], -1, -1),
          Position(busStops[2], -1, -1),
          Position(busStops[1], -1, -1),
          Position(busStops[0], -1, -1)
      };
      SplitCriteriaType::UTurn chooser(corridor12, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
    {
      Position positions[] = {
          Position(busStops[0], -1, -1), // DUMMY
          Position(busStops[0], -1, -1),
          Position(busStops[1], -1, -1),
          Position(busStops[2], -1, -1),
          Position(busStops[2], -1, -1),
          Position(busStops[3], -1, -1),
          Position(busStops[4], -1, -1),
          Position(busStops[4], -1, -1),
          Position(busStops[5], -1, -1),
          Position(busStops[6], -1, -1)
      };
      SplitCriteriaType::UTurn chooser(corridor12, positions);
      SplitCriteria result = chooser.getSplitCriteria(1, sizeof(positions)/sizeof(*positions) - 1);
      EXPECT_THAT(result.isSplit(), Eq(false));
    }
  }

  for (BusStop* busStop : busStops) {
    delete busStop;
  }
  delete corridor12;
}