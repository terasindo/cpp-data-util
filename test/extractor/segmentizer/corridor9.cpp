#include <algorithm>

#include "gmock/gmock.h"

#include "corridor.hpp"
#include "busStop.hpp"
#include "position.hpp"
#include "graph.hpp"
#include "segment.hpp"
#include "stateBundle.hpp"
#include "extractor/segmentizer.hpp"

using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;
using std::pair;

class SegmentationCorridor9Test : public Test {
protected:
  Graph *graph;
  Descriptor *descriptor;

  virtual void SetUp() {
    graph = new Graph("../test/extractor/segmentizer/data/graph.in");
    descriptor = new Descriptor();
    descriptor->selectedCorridors.push_back("9");
  }

  virtual void TearDown() {
    delete graph;
    delete descriptor;
  }
};


TEST_F(SegmentationCorridor9Test, segmentize) {
  // Load position log
  PositionLogBundle positionLogBundle;
  positionLogBundle.readFromFile("../test/extractor/segmentizer/data/9.plog");

  // Load state bundle (currently unused)
  StateBundle stateBundle;

  Segmentizer segmentizer;
  pair<vector<Segment>,StateBundle> result = segmentizer.segmentize(
      descriptor,
      graph,
      positionLogBundle,
      stateBundle
  );

  int timeStartMin[] = {1450218420, 1450228020, 1450233600, 1450241580, 1450248240, 1450256220, 1450267020, 1450274880};
  int timeStartMax[] = {1450221240, 1450228380, 1450235580, 1450242000, 1450249800, 1450258620, 1450268460, 1450275480};
  int timeEndMin[]   = {1450228020, 1450233600, 1450241580, 1450246320, 1450256220, 1450267020, 1450274880, 1450280280};
  int timeEndMax[]   = {1450228380, 1450235580, 1450242000, 1450246740, 1450258620, 1450268460, 1450275480, 1450284300};
  vector<Segment> &segments = result.first;
  int sz = sizeof(timeStartMin)/sizeof(*timeStartMin);
  EXPECT_THAT(segments.size(), Eq(sz));
  for (int i = 0; i < sz; i++) {
    if (segments.size() <= i) {
      FAIL();
    }

    EXPECT_THAT(segments[i].positions[0].unixTime, Ge(timeStartMin[i]));
    EXPECT_THAT(segments[i].positions[0].unixTime, Le(timeStartMax[i]));
    EXPECT_THAT(segments[i].positions.back().unixTime, Ge(timeEndMin[i]));
    EXPECT_THAT(segments[i].positions.back().unixTime, Le(timeEndMax[i]));
  }
}