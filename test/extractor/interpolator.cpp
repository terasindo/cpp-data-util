#include <algorithm>

#include "gmock/gmock.h"

#include "corridor.hpp"
#include "busStop.hpp"
#include "position.hpp"
#include "graph.hpp"
#include "segment.hpp"
#include "stateBundle.hpp"
#include "extractor/interpolator.hpp"

using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;
using std::pair;

class InterpolatorTest : public Test {
protected:
  double EPSILON = 1e-3;
  virtual void SetUp() {
  }

  virtual void TearDown() {
  }
};


TEST_F(InterpolatorTest, findXGivenYInLineGraph) {
  Interpolator interpolator;

  {
    // Should handle general case
    vector<Point> polyline = {
        Point(0, 0),
        Point(10, 5),
        Point(15, 15),
        Point(20, 20)
    };
    vector<double> yValues = {2, 5, 10, 12, 15, 19};
    vector<double> expected = {4, 10, 12.5, 13.5, 15, 19};

    vector<double> result = interpolator.findXGivenYInLineGraph(polyline, yValues, 0);
    ASSERT_THAT(result.size(), Eq(expected.size()));
    for (int i = 0; i < result.size(); i++) {
      EXPECT_NEAR(result[i], expected[i], EPSILON);
    }
  }

  {
    // Should handle general case
    vector<Point> polyline = {
        Point(0, 0),
        Point(10, 5),
        Point(15, 15),
        Point(20, 15),
        Point(25, 25),
        Point(30, 30)
    };
    vector<double> yValues = {5, 10, 12, 15, 15.01, 28};
    vector<double> expected = {10, 12.5, 13.5, 15, 20.005, 28};

    vector<double> result = interpolator.findXGivenYInLineGraph(polyline, yValues, 0);
    ASSERT_THAT(result.size(), Eq(expected.size()));
    for (int i = 0; i < result.size(); i++) {
      EXPECT_NEAR(result[i], expected[i], EPSILON);
    }
  }

  {
    // Should handle bus chilling in 1 place case and using tolerance
    vector<Point> polyline = {
        Point(0, 0),
        Point(10, 5),
        Point(15, 15),
        Point(20, 15),
        Point(25, 25),
        Point(30, 30)
    };
    double tolerance = 0.01;
    vector<double> yValues = {5, 10, 12, 15, 15.001, 15.009, 15.05, 28};
    vector<double> expected = {10, 12.5, 13.5, 15, 15, 15, 20.025, 28};

    vector<double> result = interpolator.findXGivenYInLineGraph(polyline, yValues, tolerance);
    ASSERT_THAT(result.size(), Eq(expected.size()));
    for (int i = 0; i < result.size(); i++) {
      EXPECT_NEAR(result[i], expected[i], EPSILON);
    }
  }
}

TEST_F(InterpolatorTest, trim) {
  Interpolator interpolator;

  // Should trim away plateau in both ends
  vector<Point> polyline = {
      Point(0, 0),
      Point(3, 0.001),
      Point(9, 0.009),
      Point(10, 0.011),
      Point(11, 0.02),
      Point(20, 5),
      Point(25, 15),
      Point(30, 15),
      Point(35, 25),
      Point(40, 30),
      Point(45, 30.009),
      Point(47, 30.001)
  };
  vector<Point> expected = {
      Point(9, 0.009),
      Point(10, 0.011),
      Point(11, 0.02),
      Point(20, 5),
      Point(25, 15),
      Point(30, 15),
      Point(35, 25),
      Point(40, 30)
  };

  double tolerance = 0.01;
  vector<Point> result = interpolator.trim(polyline, tolerance);

  ASSERT_THAT(result.size(), Eq(expected.size()));
  for (int i = 0; i < result.size(); i++) {
    EXPECT_NEAR(result[i].x, expected[i].x, EPSILON);
    EXPECT_NEAR(result[i].y, expected[i].y, EPSILON);
  }
}