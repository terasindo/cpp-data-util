#include <algorithm>

#include "gmock/gmock.h"

#include "corridor.hpp"
#include "busStop.hpp"
#include "position.hpp"
#include "graph.hpp"
#include "segment.hpp"
#include "stateBundle.hpp"
#include "extractor/segmentCorrector.hpp"

using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;
using std::pair;

class SegmentCorrectorTest : public Test {
protected:
  Graph *graph;

  virtual void SetUp() {
    vector<BusStop*> busStops = {
        new BusStop(0, -1, -1, true),
        new BusStop(1, -1, -1, false),
        new BusStop(2, -1, -1, false),
        new BusStop(3, -1, -1, false),
        new BusStop(4, -1, -1, false),
        new BusStop(5, -1, -1, true),
        new BusStop(6, -1, -1, true)
    };
    vector<Corridor*> corridors = {
      new Corridor(busStops[0], busStops[6])
    };

    // Register corridors
    for (BusStop* busStop : busStops) {
      unordered_set<Corridor*> servedCorridors = {corridors[0]};
      busStop->corridors = servedCorridors;
    }

    // Connect edges
    // 0 1 2 3 4 5 6
    // 6 4 3 2 1 0
    busStops[0]->next[corridors[0]][busStops[6]] = busStops[1];
    busStops[1]->next[corridors[0]][busStops[6]] = busStops[2];
    busStops[1]->next[corridors[0]][busStops[0]] = busStops[0];
    busStops[2]->next[corridors[0]][busStops[6]] = busStops[3];
    busStops[2]->next[corridors[0]][busStops[0]] = busStops[1];
    busStops[3]->next[corridors[0]][busStops[6]] = busStops[4];
    busStops[3]->next[corridors[0]][busStops[0]] = busStops[2];
    busStops[4]->next[corridors[0]][busStops[6]] = busStops[5];
    busStops[4]->next[corridors[0]][busStops[0]] = busStops[3];
    busStops[5]->next[corridors[0]][busStops[6]] = busStops[6];
    busStops[6]->next[corridors[0]][busStops[0]] = busStops[4];

    graph = new Graph(busStops, corridors);
  }

  virtual void TearDown() {
    delete graph;
  }
};


TEST_F(SegmentCorrectorTest, cleanInvalidPositions) {
  SegmentCorrector segmentCorrector;

  {
    // Should handle forward case
    vector<Position> positions = {
        Position(graph->busStops[0], -1, 0),
        Position(graph->busStops[1], -1, 1),
        Position(graph->busStops[1], -1, 2),
        Position(graph->busStops[2], -1, 3),
        Position(graph->busStops[4], -1, 4),
        Position(graph->busStops[4], -1, 5),
        Position(graph->busStops[4], -1, 6),
        Position(graph->busStops[6], -1, 7)
    };
    Segment segment;
    segment.corridor = graph->corridors[0];
    segment.positions = positions;
    vector<int> expectedPositionIdxs = {0, 1, 2, 3, 4, 5, 6, 7};

    segmentCorrector.cleanInvalidPositions(graph, segment);

    ASSERT_THAT(segment.positions.size(), Eq(expectedPositionIdxs.size()));
    for (int i = 0; i < segment.positions.size(); i++) {
      EXPECT_THAT(segment.positions[i].closestBusStop, Eq(positions[expectedPositionIdxs[i]].closestBusStop));
      EXPECT_THAT(segment.positions[i].x, Eq(positions[expectedPositionIdxs[i]].x));
      EXPECT_THAT(segment.positions[i].y, Eq(positions[expectedPositionIdxs[i]].y));
    }
  }

  {
    // Should handle backward case
    vector<Position> positions = {
        Position(graph->busStops[6], -1, 0),
        Position(graph->busStops[6], -1, 1),
        Position(graph->busStops[4], -1, 2),
        Position(graph->busStops[2], -1, 3),
        Position(graph->busStops[0], -1, 4)
    };
    Segment segment;
    segment.corridor = graph->corridors[0];
    segment.positions = positions;
    vector<int> expectedPositionIdxs = {0, 1, 2, 3, 4};

    segmentCorrector.cleanInvalidPositions(graph, segment);

    ASSERT_THAT(segment.positions.size(), Eq(expectedPositionIdxs.size()));
    for (int i = 0; i < segment.positions.size(); i++) {
      EXPECT_THAT(segment.positions[i].closestBusStop, Eq(positions[expectedPositionIdxs[i]].closestBusStop));
      EXPECT_THAT(segment.positions[i].x, Eq(positions[expectedPositionIdxs[i]].x));
      EXPECT_THAT(segment.positions[i].y, Eq(positions[expectedPositionIdxs[i]].y));
    }
  }

  {
    // Should handle polluted case
    vector<Position> positions = {
        Position(graph->busStops[6], -1, 0),
        Position(graph->busStops[6], -1, 1),
        Position(graph->busStops[5], -1, 2), // Pollution
        Position(graph->busStops[4], -1, 3),
        Position(graph->busStops[1], -1, 4), // Pollution
        Position(graph->busStops[1], -1, 5), // Pollution
        Position(graph->busStops[2], -1, 6),
        Position(graph->busStops[1], -1, 7),
        Position(graph->busStops[0], -1, 8)
    };
    Segment segment;
    segment.corridor = graph->corridors[0];
    segment.positions = positions;
    vector<int> expectedPositionIdxs = {0, 1, 3, 6, 7, 8};

    segmentCorrector.cleanInvalidPositions(graph, segment);

    ASSERT_THAT(segment.positions.size(), Eq(expectedPositionIdxs.size()));
    for (int i = 0; i < segment.positions.size(); i++) {
      EXPECT_THAT(segment.positions[i].closestBusStop, Eq(positions[expectedPositionIdxs[i]].closestBusStop));
      EXPECT_THAT(segment.positions[i].x, Eq(positions[expectedPositionIdxs[i]].x));
      EXPECT_THAT(segment.positions[i].y, Eq(positions[expectedPositionIdxs[i]].y));
    }
  }

  {
    // Should handle exteme sparse case
    vector<Position> positions = {
        Position(graph->busStops[6], -1, 0),
        Position(graph->busStops[2], -1, 1)
    };
    Segment segment;
    segment.corridor = graph->corridors[0];
    segment.positions = positions;
    vector<int> expectedPositionIdxs = {0, 1};

    segmentCorrector.cleanInvalidPositions(graph, segment);

    ASSERT_THAT(segment.positions.size(), Eq(expectedPositionIdxs.size()));
    for (int i = 0; i < segment.positions.size(); i++) {
      EXPECT_THAT(segment.positions[i].closestBusStop, Eq(positions[expectedPositionIdxs[i]].closestBusStop));
      EXPECT_THAT(segment.positions[i].x, Eq(positions[expectedPositionIdxs[i]].x));
      EXPECT_THAT(segment.positions[i].y, Eq(positions[expectedPositionIdxs[i]].y));
    }
  }
}
