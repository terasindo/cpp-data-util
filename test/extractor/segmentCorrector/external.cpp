#include <algorithm>
#include <set>

#include "gmock/gmock.h"

#include "corridor.hpp"
#include "busStop.hpp"
#include "position.hpp"
#include "graph.hpp"
#include "segment.hpp"
#include "stateBundle.hpp"
#include "extractor/segmentCorrector.hpp"

using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;
using std::pair;
using std::set;

class SegmentCorrectorTest : public Test {
protected:
  virtual void SetUp() {
  }

  virtual void TearDown() {
  }
};


TEST_F(SegmentCorrectorTest, getParentCorridors) {
  SegmentCorrector segmentCorrector;

  Corridor* corridors[] = {
    new Corridor("0"),
    new Corridor("1"),
    new Corridor("2"),
    new Corridor("3"),
    new Corridor("4"),
    new Corridor("5")
  };

  {
    // Should work
    vector<Segment> segments = {
        Segment(corridors[1], {
            Position(10),
            Position(20),
            Position(30),
            Position(40)
        }),
        Segment(corridors[2], {
          Position(20),
          Position(30),
          Position(60)
        }),
        Segment(corridors[3], {
          Position(30),
          Position(50)
        }),
        Segment(corridors[1], {
          Position(70),
          Position(100)
        }),
        Segment(corridors[3], {
          Position(80),
          Position(90)
        }),
        Segment(corridors[2], {
          Position(110),
          Position(130)
        }),
        Segment(corridors[4], {
          Position(120),
          Position(150)
        }),
        Segment(corridors[5], {
          Position(140),
          Position(160)
        }),
        Segment(corridors[1], {
          Position(170),
          Position(200)
        }),
        Segment(corridors[2], {
          Position(170),
          Position(190)
        }),
        Segment(corridors[3], {
          Position(170),
          Position(180)
        }),
        Segment(corridors[1], {
          Position(210),
          Position(220)
        }),
        Segment(corridors[1], {
          Position(220),
          Position(230)
        }),
    };


    vector<Corridor*> expected = {
        segments[0].corridor, segments[0].corridor, segments[0].corridor, segments[3].corridor, segments[3].corridor,
        segments[5].corridor, segments[5].corridor, segments[6].corridor, segments[8].corridor, segments[8].corridor,
        segments[8].corridor, segments[11].corridor, segments[12].corridor
    };

    vector<Corridor*> result = segmentCorrector.getParentCorridors(segments, 0, segments.size()-1);

    ASSERT_THAT(result.size(), Eq(expected.size()));
    for (int i = 0; i < result.size(); i++) {
      EXPECT_THAT(result[i], Eq(expected[i]));
    }
  }

  for (Corridor* corridor : corridors) {
    delete corridor;
  }
}

TEST_F(SegmentCorrectorTest, removeBusStopByJoin) {
  SegmentCorrector segmentCorrector;

  Corridor* corridor = new Corridor("0");

  int FAKE_INDEX_START = 6;
  BusStop* busStops[] = {
    new BusStop(0, -1, -1),
    new BusStop(1, -1, -1),
    new BusStop(2, -1, -1),
    new BusStop(3, -1, -1),
    new BusStop(4, -1, -1),
    new BusStop(5, -1, -1), // Real one stops here
    new BusStop(6, -1, -1),
    new BusStop(7, -1, -1),
    new BusStop(8, -1, -1),
    new BusStop(9, -1, -1)
  };

  set<BusStop*> realBusStops(busStops, busStops + FAKE_INDEX_START);

  {
    // Should do nothing if no bus stop falsely claimed
    {
      Segment segment = Segment(corridor, {
          Position(busStops[0], 0),
          Position(busStops[1], 10),
          Position(busStops[2], 20),
          Position(busStops[3], 30),
          Position(busStops[4], 40)
      });

      Segment expected = Segment(corridor, {
          Position(busStops[0], 0),
          Position(busStops[1], 10),
          Position(busStops[2], 20),
          Position(busStops[3], 30),
          Position(busStops[4], 40)
      });

      segmentCorrector.removeBusStopByJoin(segment, realBusStops);

      ASSERT_THAT(segment.positions.size(), Eq(expected.positions.size()));
      for (int i = 0; i < segment.positions.size(); i++) {
        EXPECT_THAT(segment.corridor, Eq(expected.corridor));
        EXPECT_THAT(segment.positions[i].closestBusStop, Eq(expected.positions[i].closestBusStop));
      }
    }
    {
      Segment segment = Segment(corridor, {
          Position(busStops[0], 0),
          Position(busStops[1], 10),
          Position(busStops[1], 12),
          Position(busStops[2], 20),
          Position(busStops[3], 30),
          Position(busStops[3], 32),
          Position(busStops[4], 40)
      });

      Segment expected = Segment(corridor, {
          Position(busStops[0], 0),
          Position(busStops[1], 10),
          Position(busStops[1], 12),
          Position(busStops[2], 20),
          Position(busStops[3], 30),
          Position(busStops[3], 32),
          Position(busStops[4], 40)
      });

      segmentCorrector.removeBusStopByJoin(segment, realBusStops);

      ASSERT_THAT(segment.positions.size(), Eq(expected.positions.size()));
      for (int i = 0; i < segment.positions.size(); i++) {
        EXPECT_THAT(segment.corridor, Eq(expected.corridor));
        EXPECT_THAT(segment.positions[i].closestBusStop, Eq(expected.positions[i].closestBusStop));
      }
    }
  }
  {
    // Should remove bus stops falsely claimed
    Segment segment = Segment(corridor, {
        Position(busStops[FAKE_INDEX_START], 0),
        Position(busStops[4], 1),
        Position(busStops[FAKE_INDEX_START+1], 3),
        Position(busStops[FAKE_INDEX_START+2], 5),
        Position(busStops[2], 20),
        Position(busStops[4], 21),
        Position(busStops[4], 22),
        Position(busStops[3], 30),
        Position(busStops[FAKE_INDEX_START+3], 32),
        Position(busStops[3], 33),
        Position(busStops[4], 40),
        Position(busStops[4], 41),
        Position(busStops[5], 50)
    });

    Segment expected = Segment(corridor, {
        Position(busStops[4], 1),
        Position(busStops[2], 20),
        Position(busStops[4], 21),
        Position(busStops[4], 22),
        Position(busStops[3], 30),
        Position(busStops[3], 33),
        Position(busStops[4], 40),
        Position(busStops[4], 41),
        Position(busStops[5], 50)
    });

    segmentCorrector.removeBusStopByJoin(segment, realBusStops);

    ASSERT_THAT(segment.positions.size(), Eq(expected.positions.size()));
    for (int i = 0; i < segment.positions.size(); i++) {
      EXPECT_THAT(segment.corridor, Eq(expected.corridor));
      EXPECT_THAT(segment.positions[i].closestBusStop, Eq(expected.positions[i].closestBusStop));
    }
  }
}
