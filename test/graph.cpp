#include <unordered_map>

#include "gmock/gmock.h"

#include "busStop.hpp"
#include "corridor.hpp"
#include "graph.hpp"

using std::unordered_map;
using std::string;
using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;

class GraphTest : public Test {
protected:
  Graph *graph;

  virtual void SetUp() {
    vector<BusStop*> busStops = {
        new BusStop(0, -1, -1, true),
        new BusStop(1, -1, -1, false),
        new BusStop(2, -1, -1, false),
        new BusStop(3, -1, -1, false),
        new BusStop(4, -1, -1, false),
        new BusStop(5, -1, -1, true),
        new BusStop(6, -1, -1, true)
    };
    vector<Corridor*> corridors = {
      new Corridor(busStops[0], busStops[6])
    };

    // Register corridors
    for (BusStop* busStop : busStops) {
      unordered_set<Corridor*> servedCorridors = {corridors[0]};
      busStop->corridors = servedCorridors;
    }

    // Connect edges
    busStops[0]->next[corridors[0]][busStops[6]] = busStops[1];
    busStops[1]->next[corridors[0]][busStops[6]] = busStops[2];
    busStops[1]->next[corridors[0]][busStops[0]] = busStops[0];
    busStops[2]->next[corridors[0]][busStops[6]] = busStops[3];
    busStops[2]->next[corridors[0]][busStops[0]] = busStops[1];
    busStops[3]->next[corridors[0]][busStops[6]] = busStops[4];
    busStops[3]->next[corridors[0]][busStops[0]] = busStops[2];
    busStops[4]->next[corridors[0]][busStops[6]] = busStops[5];
    busStops[4]->next[corridors[0]][busStops[0]] = busStops[3];
    busStops[5]->next[corridors[0]][busStops[6]] = busStops[6];
    busStops[6]->next[corridors[0]][busStops[0]] = busStops[4];

    graph = new Graph(busStops, corridors);
  }

  virtual void TearDown() {
    delete graph;
  }
};

TEST_F(GraphTest, isInside) {
  Corridor* corridor = graph->corridors[0];

  {
    // Should return true even it is only start and end visited
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[0],
          graph->busStops[6]
      };
      EXPECT_THAT(
          graph->isInside(corridor, uniqueBusStopVisitChain, graph->busStops[0], graph->busStops[6]),
          Eq(true)
      );
    }
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[6],
          graph->busStops[0]
      };
      EXPECT_THAT(
          graph->isInside(corridor, uniqueBusStopVisitChain, graph->busStops[6], graph->busStops[0]),
          Eq(true)
      );
    }
  }

  {
    // Should return true if is inside
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[0],
          graph->busStops[1],
          graph->busStops[2],
          graph->busStops[3],
          graph->busStops[4],
          graph->busStops[5],
          graph->busStops[6]
      };
      EXPECT_THAT(
          graph->isInside(corridor, uniqueBusStopVisitChain, graph->busStops[0], graph->busStops[6]),
          Eq(true)
      );
    }
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[0],
          graph->busStops[2],
          graph->busStops[3],
          graph->busStops[5],
          graph->busStops[6]
      };
      EXPECT_THAT(
          graph->isInside(corridor, uniqueBusStopVisitChain, graph->busStops[0], graph->busStops[6]),
          Eq(true)
      );
    }
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[6],
          graph->busStops[5],
          graph->busStops[4],
          graph->busStops[3],
          graph->busStops[2],
          graph->busStops[1],
          graph->busStops[0]
      };
      EXPECT_THAT(
          graph->isInside(corridor, uniqueBusStopVisitChain, graph->busStops[6], graph->busStops[0]),
          Eq(true)
      );
    }
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[6],
          graph->busStops[3],
          graph->busStops[2],
          graph->busStops[0]
      };
      EXPECT_THAT(
          graph->isInside(corridor, uniqueBusStopVisitChain, graph->busStops[6], graph->busStops[0]),
          Eq(true)
      );
    }
  }
  {
    // Should return false if is reversed
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[0],
          graph->busStops[1],
          graph->busStops[2],
          graph->busStops[3],
          graph->busStops[4],
          graph->busStops[5],
          graph->busStops[6]
      };
      EXPECT_THAT(
          graph->isInside(corridor, uniqueBusStopVisitChain, graph->busStops[6], graph->busStops[0]),
          Eq(false)
      );
    }
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[2],
          graph->busStops[3],
          graph->busStops[5]
      };
      EXPECT_THAT(
          graph->isInside(corridor, uniqueBusStopVisitChain, graph->busStops[6], graph->busStops[0]),
          Eq(false)
      );
    }
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[6],
          graph->busStops[5],
          graph->busStops[4],
          graph->busStops[3],
          graph->busStops[2],
          graph->busStops[1],
          graph->busStops[0]
      };
      EXPECT_THAT(
          graph->isInside(corridor, uniqueBusStopVisitChain, graph->busStops[0], graph->busStops[6]),
          Eq(false)
      );
    }
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[4],
          graph->busStops[3],
          graph->busStops[2]
      };
      EXPECT_THAT(
          graph->isInside(corridor, uniqueBusStopVisitChain, graph->busStops[0], graph->busStops[6]),
          Eq(false)
      );
    }
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[3],
          graph->busStops[2],
          graph->busStops[0]
      };
      EXPECT_THAT(
          graph->isInside(corridor, uniqueBusStopVisitChain, graph->busStops[0], graph->busStops[6]),
          Eq(false)
      );
    }
  }
}

TEST_F(GraphTest, getSuitableBusStopChain) {
  Corridor* corridor = graph->corridors[0];

  vector<BusStop*> forthChain = {
      graph->busStops[0],
      graph->busStops[1],
      graph->busStops[2],
      graph->busStops[3],
      graph->busStops[4],
      graph->busStops[5],
      graph->busStops[6]
  };
  vector<BusStop*> backChain = {
      graph->busStops[6],
      graph->busStops[4],
      graph->busStops[3],
      graph->busStops[2],
      graph->busStops[1],
      graph->busStops[0]
  };

  {
    // Should work on fully matched route
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[0],
          graph->busStops[1],
          graph->busStops[2],
          graph->busStops[3],
          graph->busStops[4],
          graph->busStops[5],
          graph->busStops[6]
      };
      vector<BusStop*> result = graph->getSuitableBusStopChain(corridor, uniqueBusStopVisitChain);
      vector<BusStop*> &expected = forthChain;
      EXPECT_THAT(result.size(), Eq(expected.size()));
      for (int i = 0; i < result.size(); i++) {
        EXPECT_THAT(result[i], Eq(expected[i]));
      }
    }
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[6],
          graph->busStops[4],
          graph->busStops[3],
          graph->busStops[2],
          graph->busStops[1],
          graph->busStops[0]
      };
      vector<BusStop*> result = graph->getSuitableBusStopChain(corridor, uniqueBusStopVisitChain);
      vector<BusStop*> &expected = backChain;
      EXPECT_THAT(result.size(), Eq(expected.size()));
      for (int i = 0; i < result.size(); i++) {
        EXPECT_THAT(result[i], Eq(expected[i]));
      }
    }
  }

  {
    // Should work on partially matched route
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[2],
          graph->busStops[3],
          graph->busStops[4],
          graph->busStops[5]
      };
      vector<BusStop*> result = graph->getSuitableBusStopChain(corridor, uniqueBusStopVisitChain);
      vector<BusStop*> &expected = forthChain;
      EXPECT_THAT(result.size(), Eq(expected.size()));
      for (int i = 0; i < result.size(); i++) {
        EXPECT_THAT(result[i], Eq(expected[i]));
      }
    }
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[0],
          graph->busStops[2],
          graph->busStops[4],
          graph->busStops[6]
      };
      vector<BusStop*> result = graph->getSuitableBusStopChain(corridor, uniqueBusStopVisitChain);
      vector<BusStop*> &expected = forthChain;
      EXPECT_THAT(result.size(), Eq(expected.size()));
      for (int i = 0; i < result.size(); i++) {
        EXPECT_THAT(result[i], Eq(expected[i]));
      }
    }
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[6],
          graph->busStops[3],
          graph->busStops[1]
      };
      vector<BusStop*> result = graph->getSuitableBusStopChain(corridor, uniqueBusStopVisitChain);
      vector<BusStop*> &expected = backChain;
      EXPECT_THAT(result.size(), Eq(expected.size()));
      for (int i = 0; i < result.size(); i++) {
        EXPECT_THAT(result[i], Eq(expected[i]));
      }
    }
    {
      vector<BusStop*> uniqueBusStopVisitChain = {
          graph->busStops[6],
          graph->busStops[4],
          graph->busStops[3],
          graph->busStops[2]
      };
      vector<BusStop*> result = graph->getSuitableBusStopChain(corridor, uniqueBusStopVisitChain);
      vector<BusStop*> &expected = backChain;
      EXPECT_THAT(result.size(), Eq(expected.size()));
      for (int i = 0; i < result.size(); i++) {
        EXPECT_THAT(result[i], Eq(expected[i]));
      }
    }
  }

  {
    // Return empty vector if no suitable bus stop chain
    vector<BusStop*> uniqueBusStopVisitChain = {
        graph->busStops[6],
        graph->busStops[5],
        graph->busStops[4],
        graph->busStops[6]
    };
    vector<BusStop*> result = graph->getSuitableBusStopChain(corridor, uniqueBusStopVisitChain);
    EXPECT_THAT(result.size(), Eq(0));
  }
}

TEST_F(GraphTest, getBusStopChain) {
  // For shorthand
  Corridor* corridor = graph->corridors[0];

  // Should return selected bus stop interval
  {
    vector<BusStop*> expected = {
        graph->busStops[1],
        graph->busStops[2],
        graph->busStops[3],
        graph->busStops[4],
        graph->busStops[5]
    };
    vector<BusStop*> result = graph->getBusStopChain(graph->busStops[1], graph->busStops[5], graph->busStops[0], corridor);
    EXPECT_THAT(result.size(), Eq(expected.size()));
    for (int i = 0; i < result.size(); i++) {
      EXPECT_THAT(result[i], Eq(expected[i]));
    }
  }

  {
    vector<BusStop*> expected = {
        graph->busStops[2],
        graph->busStops[3],
        graph->busStops[4],
        graph->busStops[5]
    };
    vector<BusStop*> result = graph->getBusStopChain(graph->busStops[2], graph->busStops[5], graph->busStops[0], corridor);
    EXPECT_THAT(result.size(), Eq(expected.size()));
    for (int i = 0; i < result.size(); i++) {
      EXPECT_THAT(result[i], Eq(expected[i]));
    }
  }

  {
    vector<BusStop*> expected = {
        graph->busStops[6],
        graph->busStops[4],
        graph->busStops[3],
        graph->busStops[2],
        graph->busStops[1]
    };
    vector<BusStop*> result = graph->getBusStopChain(graph->busStops[6], graph->busStops[1], graph->busStops[6], corridor);
    EXPECT_THAT(result.size(), Eq(expected.size()));
    for (int i = 0; i < result.size(); i++) {
      EXPECT_THAT(result[i], Eq(expected[i]));
    }
  }

  {
    vector<BusStop*> expected = {
        graph->busStops[4],
        graph->busStops[3],
        graph->busStops[2],
        graph->busStops[1]
    };
    vector<BusStop*> result = graph->getBusStopChain(graph->busStops[4], graph->busStops[1], graph->busStops[6], corridor);
    EXPECT_THAT(result.size(), Eq(expected.size()));
    for (int i = 0; i < result.size(); i++) {
      EXPECT_THAT(result[i], Eq(expected[i]));
    }
  }
}