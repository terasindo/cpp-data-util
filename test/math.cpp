#include <unordered_map>

#include "gmock/gmock.h"

#include "math.hpp"

using std::unordered_map;
using std::string;
using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;

class MathTest : public Test {
protected:
  const double EPSILON = 1e-5;
  unordered_map<string, Point> points;
  virtual void SetUp() {
    points["A"] = Point(2, 3);
    points["B"] = Point(5, 7);
    points["C"] = Point(-5, 2);
    points["farA"] = Point(101, 100);
    points["farB"] = Point(151, 110);

    /**
     * These forms pentagon which looks like:
     *      B      C
     * A              top
     *~~~~~~~~~~~~~~~~~~~
     *    A     B     bottom
     */
    points["pentagonTopA"] = Point(1, 1);
    points["pentagonTopB"] = Point(3, 4);
    points["pentagonTopC"] = Point(6, 7);
    points["pentagonBottomA"] = Point(3, 1);
    points["pentagonBottomB"] = Point(6, 4);

    points["octagon0"] = Point(0, 0);
    points["octagon1"] = Point(0, 1);
    points["octagon2"] = Point(0, 2);
    points["octagon3"] = Point(1, 2);
    points["octagon4"] = Point(2, 2);
    points["octagon5"] = Point(2, 1);
    points["octagon6"] = Point(2, 0);
    points["octagon7"] = Point(1, 0);
    points["octagon8"] = Point(1, 1);
  }

  virtual void TearDown() {
    // Not needed, but I write it just to remember how to do this
  }
};


TEST_F(MathTest, pointToLineDistance) {
  EXPECT_NEAR(Math::pointToLineDistance(points["A"], points["B"], points["C"]), 2.236067977, EPSILON);
  EXPECT_NEAR(Math::pointToLineDistance(points["A"], points["C"], points["B"]), 2.236067977, EPSILON);
  EXPECT_NEAR(Math::pointToLineDistance(points["A"], points["farA"], points["farB"]), 75.700828163, EPSILON);
  EXPECT_NEAR(Math::pointToLineDistance(points["A"], points["farB"], points["farA"]), 75.700828163, EPSILON);
}

TEST_F(MathTest, pointToLineSegmentDistance) {
  EXPECT_NEAR(Math::pointToLineSegmentDistance(points["A"], points["B"], points["C"]), 2.236067977, EPSILON);
  EXPECT_NEAR(Math::pointToLineSegmentDistance(points["A"], points["C"], points["B"]), 2.236067977, EPSILON);
  EXPECT_NEAR(Math::pointToLineSegmentDistance(points["A"], points["farA"], points["farB"]), 138.60014430006919, EPSILON);
  EXPECT_NEAR(Math::pointToLineSegmentDistance(points["A"], points["farB"], points["farA"]), 138.60014430006919, EPSILON);
}

TEST_F(MathTest, isPointToLineSegmentFormedShadow) {
  {
    // Should be false if shadow is behind
    EXPECT_THAT(
      Math::isPointToLineSegmentFormedShadow(points["pentagonTopA"], points["pentagonBottomA"], points["pentagonBottomB"]),
      Eq(false)
    );
    EXPECT_THAT(
      Math::isPointToLineSegmentFormedShadow(points["pentagonTopA"], points["pentagonBottomB"], points["pentagonBottomA"]),
      Eq(false)
    );
  }
  {
    // Should be false if shadow is too long
    EXPECT_THAT(
      Math::isPointToLineSegmentFormedShadow(points["pentagonTopC"], points["pentagonBottomA"], points["pentagonBottomB"]),
      Eq(false)
    );
    EXPECT_THAT(
      Math::isPointToLineSegmentFormedShadow(points["pentagonTopC"], points["pentagonBottomB"], points["pentagonBottomA"]),
      Eq(false)
    );
  }
  {
    // Should be true if shadow is inside base
    EXPECT_THAT(
      Math::isPointToLineSegmentFormedShadow(points["pentagonTopB"], points["pentagonBottomA"], points["pentagonBottomB"]),
      Eq(true)
    );
    EXPECT_THAT(
      Math::isPointToLineSegmentFormedShadow(points["pentagonTopB"], points["pentagonBottomB"], points["pentagonBottomA"]),
      Eq(true)
    );
  }
}

TEST_F(MathTest, scalarProjection) {
  EXPECT_NEAR(
    Math::scalarProjection(points["pentagonTopA"], points["pentagonBottomA"], points["pentagonBottomB"]),
    -1.414213562, EPSILON
  );
  EXPECT_NEAR(
    Math::scalarProjection(points["pentagonTopB"], points["pentagonBottomA"], points["pentagonBottomB"]),
    2.12132034, EPSILON
  );
  EXPECT_NEAR(
    Math::scalarProjection(points["pentagonTopC"], points["pentagonBottomA"], points["pentagonBottomB"]),
    6.36396103067, EPSILON
  );
}

TEST_F(MathTest, turnAngle) {
  // Should return the smaller turn angle in degree
  EXPECT_NEAR(Math::turnAngle(points["octagon0"], points["octagon8"], points["octagon1"]), 45, EPSILON);
  EXPECT_NEAR(Math::turnAngle(points["octagon0"], points["octagon8"], points["octagon2"]), 90, EPSILON);
  EXPECT_NEAR(Math::turnAngle(points["octagon0"], points["octagon8"], points["octagon3"]), 135, EPSILON);
  EXPECT_NEAR(Math::turnAngle(points["octagon0"], points["octagon8"], points["octagon4"]), 180, EPSILON);
  EXPECT_NEAR(Math::turnAngle(points["octagon0"], points["octagon8"], points["octagon5"]), 135, EPSILON);
  EXPECT_NEAR(Math::turnAngle(points["octagon0"], points["octagon8"], points["octagon6"]), 90, EPSILON);
  EXPECT_NEAR(Math::turnAngle(points["octagon0"], points["octagon8"], points["octagon7"]), 45, EPSILON);
  EXPECT_NEAR(Math::turnAngle(points["octagon0"], points["octagon8"], points["octagon0"]), 0, EPSILON);
}

TEST_F(MathTest, polylinePolylineProjection) {
  vector<Point> polyline = {
      Point(-6.137757, 106.813952),
      Point(-6.144808, 106.815494),
      Point(-6.149206, 106.816714),
      Point(-6.1521, 106.817339),
      Point(-6.160241, 106.819186),
      Point(-6.165817, 106.820425),
      Point(-6.176258, 106.822843),
      Point(-6.182701, 106.822959),
      Point(-6.187966, 106.822964),
      Point(-6.193607, 106.823017),
      Point(-6.198365, 106.823139),
      Point(-6.205569, 106.822254),
      Point(-6.210159, 106.821209),
      Point(-6.21248, 106.820005),
      Point(-6.217046, 106.815307),
      Point(-6.221451, 106.809809),
      Point(-6.224228, 106.805792),
      Point(-6.227414, 106.801407),
      Point(-6.235033, 106.798371),
      Point(-6.24326, 106.801933)
  };
  vector<Point> basePolyline = {
      Point(-6.137757, 106.813952),
      Point(-6.144808, 106.815494),
      Point(-6.149206, 106.816714),
      Point(-6.1521, 106.817339),
      Point(-6.160241, 106.819186),
      Point(-6.165817, 106.820425),
      Point(-6.176258, 106.822843),
      Point(-6.182701, 106.822959),
      Point(-6.187966, 106.822964),
      Point(-6.193607, 106.823017),
      Point(-6.198365, 106.823139),
      Point(-6.205569, 106.822254),
      Point(-6.210159, 106.821209),
      Point(-6.21248, 106.820005),
      Point(-6.217046, 106.815307),
      Point(-6.221451, 106.809809),
      Point(-6.224228, 106.805792),
      Point(-6.227414, 106.801407),
      Point(-6.235033, 106.798371),
      Point(-6.24326, 106.801933)
  };

  vector<double> expected = {
    0,
    0.007217642620690643,
    0.011781720183573395,
    0.014742439859597528,
    0.02309033121011205,
    0.028802327095964114,
    0.03951965908671774,
    0.04596370323616285,
    0.051228705610331875,
    0.05686995458551432,
    0.06162951843107826,
    0.06888767529077693,
    0.07359512948598829,
    0.07620982966000524,
    0.08276113182063771,
    0.08980613210452938,
    0.09468957535699347,
    0.10010979869187457,
    0.10831141176492107,
    0.11727642001924114
  };

  // If algorithm hasn\'t changed, should produce this result (see the test)
  vector<double> result = Math::polylinePolylineProjection(polyline, basePolyline);

  ASSERT_THAT(result.size(), Eq(expected.size()));
  for (int i = 0; i < result.size(); i++) {
    EXPECT_NEAR(result[i], expected[i], EPSILON);
  }
}
