#include "gmock/gmock.h"

#include "point.hpp"
#include "statistic.hpp"

using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;

class StatisticTest : public Test {
protected:
  double EPSILON = 1e-5;
};

TEST_F(StatisticTest, getPdf) {
  vector<double> input = {1, 2, 3, 3, 4, 5, 5, 5, 6, 8, 9, 9};
  vector<Point> result = Statistic::getPdf(input, 12, 2, 0, 10);
  vector<Point> expected = {
      Point(0.000000, 0.020833),
      Point(0.909091, 0.062500),
      Point(1.818182, 0.104167),
      Point(2.727273, 0.125000),
      Point(3.636364, 0.145833),
      Point(4.545455, 0.166667),
      Point(5.454545, 0.104167),
      Point(6.363636, 0.041667),
      Point(7.272727, 0.083333),
      Point(8.181818, 0.104167),
      Point(9.090909, 0.041667),
      Point(10.000000, 0.000000)
  };

  ASSERT_THAT(result.size(), Eq(expected.size()));
  for (int i = 0; i < result.size(); i++) {
    EXPECT_NEAR(result[i].x, expected[i].x, EPSILON);
    EXPECT_NEAR(result[i].y, expected[i].y, EPSILON);
  }
}

TEST_F(StatisticTest, getExpectedValue) {
  vector<Point> input = {
      Point(1, 0.2),
      Point(2, 0.3),
      Point(3, 0.3),
      Point(4, 0.2),
      Point(5, 0.0)
  };

  EXPECT_NEAR(Statistic::getExpectedValue(input), 2.5, EPSILON);
}

TEST_F(StatisticTest, getXWithLeftTailArea) {
  vector<Point> input = {
      Point(1, 0.2),
      Point(2, 0.4),
      Point(3, 0.3),
      Point(4, 0.075),
      Point(5, 0.025)
  };

  EXPECT_EQ(Statistic::getXWithLeftTailArea(input, 0), 1);
  EXPECT_EQ(Statistic::getXWithLeftTailArea(input, 0.12), 1);
  EXPECT_EQ(Statistic::getXWithLeftTailArea(input, 0.19), 1);
  EXPECT_EQ(Statistic::getXWithLeftTailArea(input, 0.21), 2);
  EXPECT_EQ(Statistic::getXWithLeftTailArea(input, 0.95), 4);
  EXPECT_EQ(Statistic::getXWithLeftTailArea(input, 1), 5);
}
