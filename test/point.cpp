#include "gmock/gmock.h"

#include "point.hpp"

using ::testing::Eq;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Ge;
using ::testing::Ne;
using ::testing::Test;

class PointTest : public Test {
};

TEST_F(PointTest, Norm) {
  EXPECT_THAT(Point(3, 4).norm(), Eq(5));
}

TEST_F(PointTest, distance) {
  EXPECT_THAT(Point(1, 2).distance(Point(4, 6)), Eq(5));
  EXPECT_THAT(Point(1, 1).distance(Point(6, 13)), Eq(13));
}

TEST_F(PointTest, distanceSq) {
  EXPECT_THAT(Point(1, 2).distanceSq(Point(4, 6)), Eq(25));
  EXPECT_THAT(Point(1, 1).distanceSq(Point(6, 13)), Eq(169));
}

TEST_F(PointTest, subtraction) {
  Point p = Point(10, 5) - Point(3, 7);
  EXPECT_THAT(p.x, Eq(7));
  EXPECT_THAT(p.y, Eq(-2));
}

TEST_F(PointTest, cross) {
  EXPECT_THAT(Point(10, 5).cross(Point(3, 7)), Eq(55));
  EXPECT_THAT(Point(-2, 1).cross(Point(2, -1)), Eq(0));
}

TEST_F(PointTest, dot) {
  EXPECT_THAT(Point(10, 5).dot(Point(3, 7)), Eq(65));
  EXPECT_THAT(Point(10, 0).dot(Point(10, 0)), Eq(100));
  EXPECT_THAT(Point(10, 0).dot(Point(0, 10)), Eq(0));
  EXPECT_THAT(Point(5, 5).dot(Point(-5, 5)), Eq(0));
}
